package utilities;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import ui.smoke.testcases.careerView.TC1_LoginToCareerView;

public class SikuliActions {
	public static Screen screen;
	public static Pattern pattern;
	public static String screenPath = TC1_LoginToCareerView.dirPath + "/src/main/resources/WindowScreens/";

	public SikuliActions() {
		screen = new Screen();
	}

	public static void doubleClick(String screenName) throws FindFailed {
		String screenPath1 = screenPath + screenName;
		pattern = new Pattern(screenPath1);
		System.out.println(screenPath1);
		screen.doubleClick(screenPath1);
		screen.wait(pattern, 10);
	}
	
	public static void click(String screenName) throws FindFailed {
		String screenPath2 = screenPath  + screenName;
		pattern = new Pattern(screenPath2);
		screen.click(screenPath2);
		screen.wait(pattern, 10);
	}

	public static void setFilePath(String screenName, String path) throws FindFailed {
		String screenPath3 = screenPath  + screenName;
		pattern = new Pattern(screenPath3);
		screen.type(pattern, path);
		screen.wait(pattern, 10);
	}


}
