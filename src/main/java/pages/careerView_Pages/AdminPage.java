package pages.careerView_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminPage {
	
	@FindBy(xpath = "//h4[contains(text(), 'Company Administration')]")
	public static WebElement adminPage_header;
	
	@FindBy(xpath = "//a[contains(text(), 'Sign out')]")
	public static WebElement logout_button;
	
	public static String adminUrl = "https://careerview-stage.mercer.com/Demo2016/Admin";
	
	public AdminPage() {
		PageFactory.initElements(driver, this);
		}
	
	

	public void navigateToAdmin(String username, String password) {
		
		driver.get(adminUrl);
		LoginPage loginPage = new LoginPage();
		loginPage.loginToCareerView(username, password);
		
	}



	public void performLogout() {
		// TODO Auto-generated method stub
		clickElement(logout_button);
	}


}
