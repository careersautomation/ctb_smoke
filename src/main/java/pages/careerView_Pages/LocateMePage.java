package pages.careerView_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LocateMePage {

	
	@FindBy(xpath = "//li[contains(@class , 'myposition')]")
	public static WebElement ownRole_hightlighted;
	
	@FindBy(xpath = "//li/a[@id = 'modal-6_AD0006']")
	public static WebElement itemToCompare;
	
	public LocateMePage() {
		PageFactory.initElements(driver, this);
	}
	
	public void selectItemToCompare() {
		clickElement(itemToCompare);
	}
	
}
