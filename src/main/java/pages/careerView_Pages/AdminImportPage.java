package pages.careerView_Pages;

import static driverfactory.Driver.driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminImportPage {

	@FindBy(xpath = "//h4[@data-reactid='.9.2.1.0.0']")
	public static WebElement importPage_header; 
	
	public AdminImportPage() {
		PageFactory.initElements(driver, this);
	}
	
}
