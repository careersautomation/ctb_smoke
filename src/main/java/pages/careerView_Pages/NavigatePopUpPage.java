package pages.careerView_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NavigatePopUpPage {

	

	@FindBy(id = "modal-navigate")
	public static WebElement navigatePopUp_modal;
	
	@FindBy(xpath = "//a[@class = 'close-reveal-modal']")
	public static WebElement closeModal_button;
	
	public NavigatePopUpPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void closePopup() {
		clickElement(closeModal_button);
	}
	
}
