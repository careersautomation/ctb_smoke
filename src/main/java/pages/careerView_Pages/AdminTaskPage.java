package pages.careerView_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.clickElement;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminTaskPage {

	@FindBy(xpath = "//h5[@data-reactid='.d.2.1.0']")
	public static WebElement adminTask_header;

	@FindBy(xpath = "//input[@name='employeeId']")
	public static WebElement employeeID_input;

	@FindBy(xpath = "//input[@value='Reset']")
	public static WebElement reset_button;

	public AdminTaskPage() {
		PageFactory.initElements(driver, this);
	}

	public String performResetAction(String username) {
		// TODO Auto-generated method stub
		String alertText;
		Alert alert1 = null ;
		
		try {
			setInput(employeeID_input, username);
//			clickElement(reset_button);

			 driver.findElement(By.xpath("//input[@value='Reset']")).click();
			 
		} catch (Exception e) {
			alert1 = driver.switchTo().alert();
			
		}
		finally {
			
			 alert1.accept();
			 Alert alert2 = driver.switchTo().alert();
			 alertText = alert2.getText();
			 alert2.accept();
//			 alertText = alert1.getText();
//			 alert1.accept();
		}
		return alertText;

	}

}
