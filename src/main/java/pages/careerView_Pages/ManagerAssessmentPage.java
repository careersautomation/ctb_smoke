package pages.careerView_Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.getAttributeOfElement;

public class ManagerAssessmentPage {
	@FindBy(xpath = "//a[@data-category-id= 'assessment']")
	public static WebElement managerAssessment_link;
	
	@FindBy(xpath = "//button[contains(@class , 'editButton')]")
	public static WebElement edit_button;
	
	@FindBy(xpath = "//input[@name = 'managerName']")
	public static WebElement managerName_input;
	
	@FindBy(xpath = "//input[@name = 'managerEmail']")
	public static WebElement managerEmail_input;
	
	@FindBy(xpath = "//button[@class = 'button orange-button saveButton']")
	public static WebElement save_button;
	
	@FindBy(xpath = "//button[text()= 'Request Assessment']")
	public static WebElement requestAssessment_button;
	
	@FindBy(xpath = "//button[text()= 'Request Sent']")
	public static WebElement requestSent_button;
	
	public static String name = "Test123";
	public static String email = "abc@mercer.com";
	
	public ManagerAssessmentPage() {
		PageFactory.initElements(driver, this);
	}

	public void editManagerInfo() {
		clickElement(edit_button);
		setInput(managerName_input, name);
		setInput(managerEmail_input, email);
		clickElement(save_button);
		
	}

	public String getManageName() {
		// TODO Auto-generated method stub
		return getAttributeOfElement(managerName_input, "value");
	}
	
	public String getManageEmail() {
		// TODO Auto-generated method stub
		return getAttributeOfElement(managerEmail_input, "value");
	}
	
	public void requestAssesment() {
		clickElement(requestAssessment_button);
	}
	
}
