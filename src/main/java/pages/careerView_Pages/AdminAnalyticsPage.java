package pages.careerView_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.selEleByVisbleText;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminAnalyticsPage {
	@FindBy(id="dataSelection")
	public static WebElement rangeSelection_dropdown;
	
	
	@FindBy(xpath = "//div[@data-reactid='.3.2.1.4.0']//following-sibling::div//tbody//tr")
	public static List<WebElement> dateElement;

	public AdminAnalyticsPage() {
		PageFactory.initElements(driver, this);
	}

 public void selectFromRangeDropdown() {
		// TODO Auto-generated method stub
		selEleByVisbleText(rangeSelection_dropdown, "Last Month");
	}
}
