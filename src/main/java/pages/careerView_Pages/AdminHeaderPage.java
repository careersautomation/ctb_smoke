package pages.careerView_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminHeaderPage {
	@FindBy(xpath = "//a[contains(text(), 'Analytics')]")
	public static WebElement analytics_button;
	
	@FindBy(xpath = "//a[contains(text(), 'Import')]")
	public static WebElement import_button;
	
	@FindBy(xpath = "//a[contains(text(), 'Admin Tasks')]")
	public static WebElement adminTask_button;
	

	
	public AdminHeaderPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public void navigateViaHeader(WebElement ele) {
		clickElement(ele);
	}
	

}
