package pages.careerView_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ComparePopUpPage {

	@FindBy(xpath = "//a[@class = 'close-reveal-modal']")
	public static WebElement closeModal_button;
	
	@FindBy(xpath = "//a[contains(text(), 'Start')]")
	public static WebElement startcompare_button;
	
	@FindBy(xpath = "//a[@data-reveal-id='modal-compare-roles']")
	public static WebElement compare_subheader_button;
	
	@FindBy(id= "modal-compare-roles")
	public static WebElement roleCompare_modal;
	
	public ComparePopUpPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void closeModal() {
		clickElement(closeModal_button);
	}
	
	public void performCompare() {
		clickElement(startcompare_button);
		LocateMePage locateMe = new LocateMePage();
		clickElement(LocateMePage.itemToCompare);
		clickElement(compare_subheader_button);
	}
}
