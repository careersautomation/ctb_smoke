package pages.careerView_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyProfileSideMenu {

	@FindBy(xpath = "//a[@data-panel-id='user_manager-assessment']")
	public static WebElement managerAssessment_link;
	
	@FindBy(xpath = "//a[@class='close-reveal-modal']")
	public static WebElement closePopup_button;
	
	public MyProfileSideMenu() {
		PageFactory.initElements(driver, this);
	}
	
	public void closePopup() {
		// TODO Auto-generated method stub
		clickElement(closePopup_button);
	}
	
	public void navigateToManagerAssessmentPage() {
		clickElement(managerAssessment_link);
	}
}
