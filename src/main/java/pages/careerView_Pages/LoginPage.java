package pages.careerView_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

	@FindBy(id = "EmployeeId")
	public static WebElement username_input;
	
	@FindBy(id = "Password")
	public static WebElement password_input;
	
	@FindBy(xpath = "//input[@value = 'Log In']")
	public static WebElement submit_button;
	
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void loginToCareerView(String username , String password) {
		
		setInput(username_input, username);
		setInput(password_input, password);
		clickElement(submit_button);
		
	}
}
