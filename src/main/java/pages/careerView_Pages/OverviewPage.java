package pages.careerView_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static utilities.SikuliActions.click;
import static utilities.SikuliActions.doubleClick;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.sikuli.script.FindFailed;

import utilities.SikuliActions;



public class OverviewPage {

	@FindBy(xpath = "//a[text()='My Profile'][@class = 'blue-button modal-button-profile']")
	public static WebElement myProfile_button;

	@FindBy(xpath = "//h2/span[text()='My Profile']")
	public static WebElement myProfile_header;

	@FindBy(id = "modal-profile")
	public static WebElement profile_modal;

	@FindBy(xpath = "//div[text()='Select a Photo (JPG)']")
	public static WebElement uploadPhoto_button;

	@FindBy(xpath = "//input[@name = 'LastName']")
	public static WebElement lastName_input;

	@FindBy(xpath = "//textarea[@name = 'About']")
	public static WebElement about_input;

	@FindBy(xpath = "//button[@type = 'submit']")
	public static WebElement save_button;
	
	@FindBy(xpath = "//button[@class = 'right button orange-button saved-button']")
	public static WebElement savedInfoStyle_button;
	
	@FindBy(xpath = "//a[@data-category-id= 'basics']")
	public static WebElement overview_Link;
	
	
	public OverviewPage() {
		PageFactory.initElements(driver, this);
	}

	public void goTomyProfile() {
		// TODO Auto-generated method stub
		clickElement(myProfile_button);
	}

	public void navigateToOverviewTab() {
		clickElement(overview_Link);
	}
	
	public void uploadPhoto() throws FindFailed {
		SikuliActions sikuliAction = new SikuliActions();
		System.out.println("Sikuli called and created");
		clickElement(uploadPhoto_button);
		System.out.println("Clicked on upload button normal action");
		doubleClick("picturesTab.png");
		System.out.println("1");
//		delay(3000);
//		click("samplePicture.png");
		delay(3000);
		doubleClick("samplePicture.png");
		delay(3000);
		click("pictureToBeSelected.png");
		delay(3000);
		click("openButton.png");
		delay(3000);
		System.out.println("Picture uploaded");
		clickElement(save_button);


	}

	public void editInfo() {
		// TODO Auto-generated method stub
		setInput(lastName_input, "QA11");
		setInput(about_input, "About: Checking test11");
		clickElement(save_button);
	}

}
