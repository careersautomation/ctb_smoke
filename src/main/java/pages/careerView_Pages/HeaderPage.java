package pages.careerView_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.isElementExisting;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class HeaderPage {

	@FindBy(xpath ="//a[@data-dropdown='profile-drop']")
	public static WebElement profile_dropdown;
	
	@FindBy(xpath ="//ul[@id='profile-drop']//a[@class='show-profile']")
	public static WebElement showProfile_option;

	@FindBy(id = "profile_image")
	public static WebElement profilePhoto;
	
	@FindBy(xpath = "//span[@class = 'icon-locate-me']")
	public static WebElement locateMe_button;
	
	@FindBy(id = "nav_navigate")
	public static WebElement navigate_button;
	
	@FindBy(xpath = "//ul[@id='profile-drop']//a[@class='sign-out']")
	public static WebElement logout_button;

	@FindBy(id = "nav_compare")
	public static WebElement compare_button;
	
	@FindBy(id = "nav_all_maps")
	public static WebElement home_button;
	
	public HeaderPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void selectMyProfileOption() {
		clickElement(profile_dropdown);
		boolean isPresent = false;
		while(!isPresent) {
			 isPresent = isElementExisting(driver, showProfile_option, 5);
			 
			 if(isPresent) {
				 clickElement(showProfile_option);
			 }
		}
	}

	public void logout() {
		// TODO Auto-generated method stub
		clickElement(profile_dropdown);
		boolean isPresent = false;
		while(!isPresent) {
			 isPresent = isElementExisting(driver, showProfile_option, 5);
			 
			 if(isPresent) {
				 clickElement(logout_button);
			 }
		}
	}
	
	public void navigateViaHeader(WebElement e) {
		clickElement(e);
	}
}
