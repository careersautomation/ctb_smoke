package pages.careerView_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

	@FindBy(xpath = "//span[@class = 'indicator']/parent::a[@href = '/organization/explore/6_IT']")
	public static WebElement locateMe_menu;
	
	
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	public void navigateToLocateMe() {
		clickElement(locateMe_menu);
		HeaderPage headerPage = new HeaderPage();
		clickElement(HeaderPage.locateMe_button);
	}
}
