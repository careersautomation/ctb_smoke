package pages.Allegro;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.*;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AllegroResultsPage {

	@FindBy(xpath="//h5[contains(text(),'Results')]")
	public static WebElement ResultsPageTitle;
	
	@FindBy(xpath="(//a[contains(text(),'VIEW REPORTS')])[1]")
	public static WebElement FirstResultTile;
	
	@FindBy(xpath="(//div[@class='card-title']/h6)[1]")
	public static WebElement FirstTileTitle;
	
	public AllegroResultsPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void ResultLinkSelection() throws InterruptedException {
		scrollToElement(driver,FirstResultTile);
		waitForElementToDisplay(FirstResultTile);
		Thread.sleep(3000);
		clickElementUsingJavaScript(driver, FirstResultTile);
		waitForPageLoad();
		Thread.sleep(2000);
	}
	
	
	
}
