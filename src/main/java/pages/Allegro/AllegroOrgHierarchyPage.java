package pages.Allegro;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AllegroOrgHierarchyPage {

	@FindBy(xpath ="//td[contains(text(),'APM_Hierarchy_1')]")
	public static WebElement Hiearchy1;
	
	@FindBy(css="button[data-url='/Allegro_QA.Admin/Membership/Hierarchy/Edit']")
	public static WebElement HieEditButton;
	
	@FindBy(xpath="//div[contains(text(),' Employees')]")
	public static WebElement HieEmployeessection;
	
	@FindBy(xpath="//div[contains(text(),' Properties')]")
	public static WebElement HieProperties;
	
	@FindBy(xpath="//table[@class='table-hover table']/tbody/tr[1]/td[1]")
	public static WebElement FirstHierarchy;
	
	public AllegroOrgHierarchyPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void SelectOpenHierarchy() {
		
		waitForElementToDisplay(FirstHierarchy);
		clickElement(FirstHierarchy);
		waitForElementToDisplay(HieEditButton);
		clickElement(HieEditButton);
	}
	
	public void OpenHieEmployees() {
		waitForElementToDisplay(HieProperties);
		waitForElementToDisplay(HieEmployeessection);
		clickElement(HieEmployeessection);
		
		
	}
	
	
}
