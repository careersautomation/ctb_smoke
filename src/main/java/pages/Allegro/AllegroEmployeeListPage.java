package pages.Allegro;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.selEleByVisbleText;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AllegroEmployeeListPage {

	
	@FindBy(xpath="//*[contains(text(),'External ID')]")
	public static WebElement EmpListExternalIDText;
	
	@FindBy(css="div[id='top-navbar_menu_item__Admin']")
	public static WebElement Admin;
	
	
	
	public AllegroEmployeeListPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	
	
	
}
