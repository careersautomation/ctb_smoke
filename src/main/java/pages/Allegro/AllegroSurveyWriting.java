package pages.Allegro;

import static driverfactory.Driver.*;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Factory;

public class AllegroSurveyWriting {

	@FindBy(xpath="//ul[@class='navbar-nav']/a[contains(text(),'Surveys')]")
	public static WebElement Survey_Button;
	
	@FindBy(xpath="//a[contains(text(),' New Survey')]")
	public static WebElement NewSurvey_Button;
	
	@FindBy(xpath="//*[contains(text(),'Survey Name')]")
	public static WebElement SurveyName_Lable;
	
	@FindBy(xpath="//input[@class='form-control'][1]")
	public static WebElement SurveyName_Text;
	
	@FindBy(xpath="//select[@class='form-control'][1]")
	public static WebElement SurveyType_dropdown;
	
	//@FindBy(xpath="//input[@class='form-check-input'][1]")
	//public static WebElement Lang_checkbox;
	
	@FindBy(xpath="//button[@name='NextStep']")
	public static WebElement NextStep_Button;
	
	@FindBy(xpath="//*[contains(text(),'We Want to Hear From You')]")
	public static WebElement WeWanttoHearFromYou_lable;
	
	@FindBy(xpath="//*[contains(text(),'Survey Page')]")
	public static WebElement SurveyPage_Link;
	
	@FindBy(xpath="//a[@id=\"QuestionToggler\"]/i")
	public static WebElement Plus_button;
	
	@FindBy(xpath="//strong[text()='Performance Management']//parent::div//span[@data-languageid='1']")
	public static WebElement QuestionCard;
	
	@FindBy(css="div[class*='card instructionBox']")
	public static WebElement CardInstruction;
	
	@FindBy(xpath="//button[@name=\"NextStep\"]")
	public static WebElement NextStepQ_Button;
	
	@FindBy(xpath="//*[contains(text(),'Your Audience')]")
	public static WebElement YourAudience_lable;
	
	@FindBy(xpath="//*[@id=\"empTreeLeft\"]/div/div/div/ul/li/input")
	public static WebElement Emp_Checkbox;
	
	//@FindBy(xpath="//input[@class=\"selectEmployee\" and @data-value=2]")
	//public static WebElement Emp1_Checkbox;
	
	@FindBy(xpath="//input[@value=\"ADD SELECTED USERS\"]")
	public static WebElement AddUser_Button;
	
	@FindBy(xpath="//button[@name=\"NextStep\"]")
	public static WebElement NextStepA_Button;
	
	@FindBy(xpath="//*[contains(text(),'Scheduling / Emails')]")
	public static WebElement SchedulingEmails_lable;
	
	@FindBy(xpath="//button[@name=\"NextStep\"]")
	public static WebElement NextStepE_Button;
	
	@FindBy(xpath="//*[contains(text(),'Results based on')]")
	public static WebElement Resultsbasedon_lable;
	
	@FindBy(xpath="//select[@id=\"Dropdown_ResultsBasedOn_Type\"]")
	public static WebElement ResultBasedOn_dropdown;
	
	@FindBy(xpath="//select[@name=\"//select[@id=\"FormData.ReportBaseTypeValue\"]")
	public static WebElement ResultBasedOnValue_dropdown;
	
	@FindBy(xpath="//select[@id=\"Dropdown_DonutBasedOn_Type\"]")
	public static WebElement DonutBasedOn_dropdown;
	
	@FindBy(xpath="//select[@name=\"FormData.ReportLibraryChartBaseTypeValue\"]")
	public static WebElement DonutBasedOnValue_dropdown;
	
	@FindBy(xpath="//select[@id=\"surveyItemID\"]")
	public static WebElement surveyItemID_dropdown;
	
	@FindBy(xpath="//select[@id=\"NormSetID_Default\"]")
	public static WebElement NormSetID_Default_dropdown;
	
	@FindBy(xpath="//button[@name=\"NextStep\"]")
	public static WebElement NextStepR_Button;
	
	@FindBy(xpath="//*[contains(text(),'Survey Details')]")
	public static WebElement SurveyDetails_lable;
	
	@FindBy(xpath="//button[contains(text(),'Complete Survey Setup')]")
	public static WebElement CompleteSurveySetup_button;
	
	public AllegroSurveyWriting() {
		PageFactory.initElements(driver, this);
	}
	
	public void SelectSurvey() {
		clickElement(Survey_Button);
	}
	
	public void SelectNewSurvey() {
		clickElement(NewSurvey_Button);
	}
	
	public void DetailsPage() throws InterruptedException {
		clickElement(SurveyName_Text);
		setInput(SurveyName_Text, "Survey");
		selEleByVisbleText(SurveyType_dropdown, "Census");
		//clickElement(Lang_checkbox);
		scrollToElement(driver,NextStep_Button);
		waitForElementToDisplay(NextStep_Button);
		clickElement(NextStep_Button);
		}
	
	public void QuestionPage() throws InterruptedException {
		waitForElementToDisplay(WeWanttoHearFromYou_lable);
		clickElement(SurveyPage_Link);
		waitForElementToDisplay(Plus_button);
		clickElement(Plus_button);
		Thread.sleep(2000);
		
		

		Actions builder = new Actions(driver);

		/*Action dragAndDrop = builder.clickAndHold(QuestionCard)
		   .moveToElement(CardInstruction)
		   .release(CardInstruction)
		   .build();

		dragAndDrop.perform();*/
		
		//Element which needs to drag.    		
    	/*WebElement From=driver.findElement(By.xpath("//div[@class=\\\"card questionCard\\\"]"));	
     
    	//Element on which need to drop.		
    	WebElement To=driver.findElement(By.xpath("//div[@class=\\\"card instructionBox QuestionInstructions\\\"]"));					
     		
    	//Using Action class for drag and drop.		
    	Actions act=new Actions(driver);					

    	//Dragged and dropped.		
    	*/
		builder.dragAndDrop(QuestionCard, CardInstruction).build().perform();
    	scrollToElement(driver,NextStepQ_Button);
		waitForElementToDisplay(NextStepQ_Button);
    	clickElement(NextStepQ_Button);
	}
	
	public void AudiencePage() throws InterruptedException {
		waitForElementToDisplay(YourAudience_lable);
		clickElement(Emp_Checkbox);
		//clickElement(Emp1_Checkbox);
		scrollToElement(driver,AddUser_Button);
		clickElement(AddUser_Button);
		scrollToElement(driver,NextStepA_Button);
		waitForElementToDisplay(AddUser_Button);
		waitForElementToDisplay(NextStepA_Button);
		clickElement(NextStepA_Button);
	}
	
	public void EmailsPage() throws InterruptedException {
		scrollToElement(driver,NextStepE_Button);
		waitForElementToDisplay(NextStepE_Button);
		clickElement(NextStepE_Button);
	}
	
	public void ReportsPage() throws InterruptedException {
		waitForElementToDisplay(ResultBasedOn_dropdown);
		selEleByVisbleText(ResultBasedOn_dropdown, "Hierarchy");
		selEleByVisbleText(DonutBasedOn_dropdown, "Dimension");
		selEleByVisbleText(DonutBasedOnValue_dropdown, "Performance Management");
		selEleByVisbleText(NormSetID_Default_dropdown, "Best in Class (Top 3rd Percentile)");
		scrollToElement(driver,NextStepR_Button);
		waitForElementToDisplay(NextStepR_Button);
		clickElement(NextStepR_Button);
		
	}
	
	public void ReviewPage() throws InterruptedException {
		scrollToElement(driver,CompleteSurveySetup_button);
		waitForElementToDisplay(CompleteSurveySetup_button);
		clickElement(CompleteSurveySetup_button);
	}
	
	}

