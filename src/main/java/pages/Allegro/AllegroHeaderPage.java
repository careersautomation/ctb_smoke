package pages.Allegro;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;

public class AllegroHeaderPage {

	@FindBy(css ="button[class='btn btn-default dropdown-toggle bg-light navbutton']")
	public static WebElement Clients;
	
	@FindBy(xpath="//div[@id='top-navbar_menu_item__Admin']/button[@data-toggle]")
	public static WebElement Admin;
	
	@FindBy(xpath="//a[contains(text(),'Surveys')]")
	public static WebElement Surveys;
	
	@FindBy(xpath="//a[contains(text(),'Results')]")
	public static WebElement Results;
	
	@FindBy (css="i[title='Chandrasekar, Logesh']")
	public static WebElement LogoutIcon;
	
	@FindBy(xpath="//*[contains(text(),'VIEW MORE CLIENTS')] ")
	public static WebElement Viewmoreclients;
	
	@FindBy(xpath="//div[@class='dropdown-menu show']/div[@class='more']/a[@id='viewMoreLink']")
	public static WebElement Viewmoreclient1;
	
	@FindBy(css="a[href='/Allegro.Admin/Core/Client/Edit/510?selectedTab=Properties']")
	public static WebElement ClientConfig;
	
	@FindBy(xpath="//ul[@class='dropdown-menu show']/li[@id='top-navbar_menu_item__Admin__Employee_List']/a")
	public static WebElement EmployeeList;
	
	@FindBy(xpath="//ul[@class='dropdown-menu show']/li[@id='top-navbar_menu_item__Admin__Org_Hierarchies']/a")
	public static WebElement OrgHierarchy;
	
	@FindBy (xpath="//li[@id='top-navbar_menu_item__UserName__Logout']/a[contains(text(),'Logout')]")
	public static WebElement LogoutButton;
	
	@FindBy(xpath="(//button[@class='navbutton dropdown-toggle btn btn-default btn-block bg-light' and @type='button'])[2]")
	public static WebElement LogoutDropdown;
	
	
	public AllegroHeaderPage() {
		PageFactory.initElements(driver, this);
	}
		
		
		public void ClientsTab() throws InterruptedException {
			waitForElementToDisplay(Clients);
			clickElement(Clients);
			//waitForElementToDisplay(Viewmoreclient1);
			waitForElementToEnable(Viewmoreclients);
			Thread.sleep(3000);
			//implicitWait(driver, 3000);
			//scrollToElement(Viewmoreclients);
			clickElementUsingJavaScript(driver, Viewmoreclients);
		}
		
		public void AdminTab() {
			
			waitForElementToDisplay(Admin);
			clickElement(Admin);
			
		}
		
		
		
		public void SurveysTab() {
			
			
			waitForElementToDisplay(Surveys);
			clickElement(Surveys);
			
		}
		
		public void ResultsTab() {
			
			waitForElementToDisplay(Results);
			clickElement(Results);
			
		}
		
		public void Logout() {
			
			waitForElementToDisplay(LogoutDropdown);
			clickElement(LogoutDropdown);
			waitForElementToDisplay(LogoutButton);
			clickElement(LogoutButton);
			
		}
		
		public void EmployeeListPage() throws InterruptedException {
			waitForElementToDisplay(Admin);
			clickElementUsingJavaScript(driver,Admin);
			Thread.sleep(3000);
			waitForElementToDisplay(EmployeeList);
			clickElementUsingJavaScript(driver, EmployeeList);
			
		}
		
		public void OrgHierarchyPage() {
			
			waitForElementToDisplay(Admin);
			clickElementUsingJavaScript(driver,Admin);
			clickElementUsingJavaScript(driver, OrgHierarchy);
			
			
		}
		
}
