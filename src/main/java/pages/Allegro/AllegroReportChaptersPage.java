package pages.Allegro;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AllegroReportChaptersPage {

	@FindBy(xpath="//h3[contains(text(),'Dashboard')]")
	public static WebElement DashboardChapterText;
	
	@FindBy(css="button[id='dropdownMenu1']")
	public static WebElement ChaptersIcon;
	
	@FindBy(xpath="//ul[@class='dropdown-menu show']/li/a[contains(text(),'ItemDetail')]")
	public static WebElement ItemDetailChapter;
	
	@FindBy(xpath="//ul[@class='dropdown-menu show']/li/a[contains(text(),'AllItems')]")
	public static WebElement Allitemchapter;
	
	@FindBy(xpath="//ul[@class='dropdown-menu show']/li/a[contains(text(),'ActionPriorityGrid')]")
	public static WebElement ActionGrid;
	
	@FindBy(xpath="//ul[@class='dropdown-menu show']/li/a[contains(text(),'HeatMap')]")
	public static WebElement HeatMapChapter;
	
	
	public AllegroReportChaptersPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	
	
	
	public void ChapterSelection(WebElement Chapter) throws InterruptedException {
		
		waitForElementToDisplay(ChaptersIcon);
		clickElement(ChaptersIcon);
		waitForElementToDisplay(Chapter);
		clickElement(Chapter);
		Thread.sleep(5000);
		
		
	}
	
}
