package pages.Allegro;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.*;

import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static verify.SoftAssertions.verifyElementTextContains;

public class AllegroLoginPage {
	@FindBy(css = "button[class*='loginBtn']")
	public static WebElement loginBtn;
	@FindBy(id = "txtEmail")
	public static WebElement emailInput;
	
	@FindBy(css = "input[id='txtPassword-clone']")
	public static WebElement password;
	@FindBy(css = "input[id='txtPassword']")
	public static WebElement password1;
	
	@FindBy(css = "button[title='Enter']")
	public static WebElement enterBtn;
	
	@FindBy(id = "ContentPlaceHolder1_PassiveSignInButton")
	public static WebElement nextBtn;
	
	@FindBy(css= "input[id='ContentPlaceHolder1_TextBox1']")
	public static WebElement Emailad; 

	@FindBy(css="span[id='ContentPlaceHolder1_Label2']")
	public static WebElement SSOText;
	
	@FindBy(css="select[id='emailAddress']")
	public static WebElement SelectMercerUser;
	
	@FindBy(css="input[type='submit']")
	public static WebElement NewSiteSubmitButton;
	
	
	
	public AllegroLoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	
public void loginAllegro_SuperUser(String Email) throws InterruptedException
{
	//deleteCookies(driver);
	implicitWait(driver,3000);
	waitForElementToDisplay(SSOText);
	//verifyElementTextContains(SSOText, "Single Sign On", test);
	waitForElementToDisplay(Emailad);
	setInput(Emailad, Email);
	clickElement(nextBtn);
	
	
	//clickElement(loginBtn);
//	waitForElementToDisplay(emailInput);
//	setInput(emailInput,uname);
//	waitForElementToDisplay(password);
//	clickElement(password);
//	waitForElementToDisplay(password1);
//	setInput(password1,pwd);
//	clickElement(enterBtn);

}

public void loginAllegroNewSite() throws InterruptedException {
	
	waitForElementToDisplay(SelectMercerUser);
	Thread.sleep(4000);
	selEleByVisbleText(SelectMercerUser, "Logesh.chandrasekar@mercer.com");
	clickElement(NewSiteSubmitButton);
}

public void loginAllegro_Merceradmin(String uname , String pwd) throws InterruptedException
{
	
	waitForElementToDisplay(nextBtn);
	clickElement(nextBtn);
	waitForElementToDisplay(emailInput);
	setInput(emailInput, uname);
    clickElement(password);
	setInput(password,pwd);
	clickElement(enterBtn);
}

public void loginAllegro_Clientadmin(String uname , String pwd) throws InterruptedException
{
	
	waitForElementToDisplay(nextBtn);
	clickElement(nextBtn);
	waitForElementToDisplay(emailInput);
	setInput(emailInput, uname);
	waitForElementToDisplay(password);
    clickElement(password);
	waitForElementToDisplay(password1);
	setInput(password1,pwd);
	clickElement(enterBtn);
}

public void loginAllegro_ResultViewer(String uname , String pwd) throws InterruptedException
{
	
	waitForElementToDisplay(nextBtn);
	clickElement(nextBtn);
	waitForElementToDisplay(emailInput);
	setInput(emailInput, "");
	waitForElementToDisplay(password);
    clickElement(password);
	waitForElementToDisplay(password1);
	setInput(password1,"");
	clickElement(enterBtn);
}

}
