package pages.Allegro;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.*;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.setInput;


public class AllegroDashboardPage {
	
	@FindBy(css="a[href='Surveying/WizardDetails/Index']")
    public static WebElement Templink;
	
	@FindBy(xpath="//button[starts-with(@data-target,'#id')]")
	public static WebElement ShowFilterButton;
	
	@FindBy(css="input[id='filter_Code']")
	public static WebElement CodeTextField;
	
	@FindBy(css="input[id='filter_Name']")
	public static WebElement NameTextField;
	
	@FindBy(xpath="(//button[contains(text(),' Filter')])[1]")
	public static WebElement FilterButton;
	
	
	@FindBy(xpath="//button[contains(text(),' Edit')]")
	public static WebElement Editbutton;
	
	
	@FindBy(css="div[class='card-header show']")
	public static WebElement ClientPropertiesTab;
	
	@FindBy(xpath="//tbody[@data-total-records]")
	public static WebElement SelectSearchedClient;
	
	
	

		public AllegroDashboardPage() {
			PageFactory.initElements(driver, this);
			
		}
		
		public void SearchClients(String Client) throws InterruptedException {
			
		waitForElementToDisplay(ShowFilterButton);	
		clickElementUsingJavaScript(driver,ShowFilterButton);
		waitForElementToDisplay(CodeTextField);
		setInput(CodeTextField, Client);
		waitForElementToDisplay(FilterButton);
		clickElementUsingJavaScript(driver,FilterButton);
		waitForElementToDisplay(SelectSearchedClient);
		Thread.sleep(2000);
		clickElement(SelectSearchedClient);
		waitForElementToDisplay(Editbutton);
		clickElementUsingJavaScript(driver,Editbutton);
		waitForElementToDisplay(ClientPropertiesTab);
				
		}

		

	
		
		
}
