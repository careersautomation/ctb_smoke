package pages.Allegro;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;


public class AllegroSurveyPage {

	@FindBy(xpath="//h5[contains(text(),'Surveys')]")
	public static WebElement SurveyTitle;
	
	@FindBy(css="(//div[@class='card-title'])[1]")
	public static WebElement FirstSurveyTileName;
	
	@FindBy(xpath="(//a[contains(text(),'VIEW RESPONSE RATE')])[1]")
	public static WebElement FirstResponseRateLink;
	
	@FindBy(xpath="//div[@id='chapterContainer']/div/h5[contains(text(),'Response Rate Report')]")
	public static WebElement ResponseRateText;
	
	
	
	
	public AllegroSurveyPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public void ResponseRateReportView() throws InterruptedException {
		
		
		//waitForElementToDisplay(SurveyTitle);
	//	waitForElementToDisplay(FirstSurveyTileName);
		waitForElementToDisplay(FirstResponseRateLink);
		scrollToElement(driver,FirstResponseRateLink);
		Thread.sleep(2000);
		clickElementUsingJavaScript(driver,FirstResponseRateLink);
		waitForPageLoad();
		waitForElementToDisplay(ResponseRateText);
	}
	
	
}
