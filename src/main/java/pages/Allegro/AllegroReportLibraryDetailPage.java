package pages.Allegro;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AllegroReportLibraryDetailPage {

	@FindBy(xpath="//a[contains(text(),'View Report')]")
	public static WebElement ViewReportButton;
	
	@FindBy(css="div[class='survey-title']")
	public static WebElement ReportDetailTitle;
	
	@FindBy(xpath="//a[contains(text(),'View Report')]")
	public static WebElement ViewReportButtonDetailPage;
	
	@FindBy(xpath="(//div[@class='dropdown']/a[@class='nav-link'])[2]")
	public static WebElement FirstReportPPT;
	
	@FindBy(xpath="(//a[@class='dropdown-item reportDownload' and @data-reportformat='PPT' and contains(text(),'English')])[1]")
	public static WebElement FirstPPTReportEnglish;
	
	@FindBy(xpath="(//div[@class='dropdown']/a[@class='nav-link'])[3]")
	public static WebElement FirstReportXLS;
	
	@FindBy(xpath="(//a[@class='dropdown-item reportDownload' and @data-reportformat='XLS' and contains(text(),'English')])[1]")
	public static WebElement FirstXLSReportEnglish;
	
	@FindBy(xpath="//div[@id='LoaderSuccessMessage']//parent::a[contains(text(),'Download Report')]")
	public static WebElement DownloadReportButton;
	
	@FindBy(xpath="(//button[@type='button' and @data-dismiss='modal'])[2]")
	public static WebElement DownloadCLoseButton;
	
	public AllegroReportLibraryDetailPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void NavigateToReportDashBoard() {
		
	  waitForElementToDisplay(ViewReportButton);
	  clickElement(ViewReportButton);
	}
	
	public void ReportsDownloadSelection(WebElement Report) throws InterruptedException {
		
		waitForPageLoad();
		Thread.sleep(2000);
		waitForElementToDisplay(Report);
		clickElement(Report);
		Thread.sleep(2000);
	}
	
	public void ReportsDownloadLanguageSelection(WebElement Language) throws InterruptedException {
		waitForElementToDisplay(Language);
		Thread.sleep(2000);
		clickElementUsingJavaScript(driver, Language);
		waitForPageLoad(driver);
		Thread.sleep(15000);
		clickElementUsingJavaScript(driver, DownloadReportButton);
		Thread.sleep(5000);
		clickElementUsingJavaScript(driver, DownloadCLoseButton);
	}
}
