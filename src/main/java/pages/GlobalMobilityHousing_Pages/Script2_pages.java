package pages.GlobalMobilityHousing_Pages;
    import static driverfactory.Driver.setInput;
	import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
	import org.openqa.selenium.support.PageFactory;
	import driverfactory.Driver;
	import static driverfactory.Driver.driver;
	import static driverfactory.Driver.hoverAndClickOnElement;
	import static driverfactory.Driver.waitForElementToDisplay;

import java.io.File;
import java.util.ArrayList;

import static driverfactory.Driver.clickElementUsingJavaScript;
	import static driverfactory.Driver.hoverOverElement;
	import static driverfactory.Driver.clickElement;
	import static driverfactory.Driver.*;
	
		public class Script2_pages {
			 
	
		    @FindBy(xpath = "//select[@id='W1086_Sl']")
		    WebElement Location_Dropdown;
		    
		    @FindBy(xpath = "//select[@id='W1089_Sl']")
		    WebElement LocationUS_Dropdown;
		    
		    @FindBy(xpath = "//select[@id='W1094_Sl']")
		    WebElement SurveyDate_Dropdown;
		    
		    @FindBy(xpath = "//select[@id='W1097_Sl']")
		    WebElement Policy_Dropdown;
		    
		    @FindBy(xpath = "//span[@id='W688']/a")
		    WebElement View_Button;
		    
		    @FindBy(xpath = "//div[@id='*lK36*kW2107*x1*t1540909871116']/div")
		    WebElement ChangeCurrency_tab;
		    
		    @FindBy(xpath = "//select[@id='mstr86_select']")
		    WebElement ChangeCurrency_dropdown;
		    
		    @FindBy(xpath = "//*[contains(text(),'Sample Accommodations')]")
		    WebElement SampleAccomodation_tab;
		  
		    
		    @FindBy(xpath = "//div[@id='*lK36*kK1074*x1*t1540905816968']/div")
		    WebElement ResetAllSelector_Button;
		    
		    //@FindBy(xpath = "//div[@id='*lK36*kW2131*x1*t1540905816968']/div")
		    //WebElement MAP_tab;
		    
		    @FindBy(xpath = "//div[@id='*lK36*kW1978*x1*t1540905816968']/div")
		    WebElement GeneralInfo_tab;
		    
		    @FindBy(xpath = "//div[contains(@id , '*lK36*kW1124*x1*t1542802789926')]")
		    public static WebElement Verify_GeneralInfo_tab;
		    
		    @FindBy(xpath = "//div[@id='*lK36*kW1866*x1*t1540905816968']/div[1]")
		    WebElement Home_tab;
		    
		    @FindBy(xpath = "//div[@id='signoutid']/div/div[2]")
		    WebElement logout;
		    
		    @FindBy(xpath = "//div[@id='mstr104']/div")
		    WebElement dropdown_excel_pdf;
		    
		    @FindBy(xpath = "//*[contains(text(),'Map')]")
			WebElement MAP_tab;
		    
		    @FindBy(xpath = "//tr[@id='mstr149']/td[2]")
		    WebElement Export;
		    
		    @FindBy(xpath = "//tr[@id='mstr150']/td[2]")
		    WebElement Exportexcel;
		    
		    @FindBy(xpath = "//*[contains(text(),'Manage My Pushpins')]")
		    public static WebElement Verify_MAP_tab;
		    
		    @FindBy(xpath = "//span[@id='W888']")
		    public static WebElement Verify_Intermediate_Page;
		    
		    @FindBy(xpath = "//*[@id='mstr75']/div/table/tbody/tr/td[2]/input")
		    public static WebElement Verify_AdvancedAllowance_Page;
		  
		    @FindBy(xpath = "//span[text()='Accommodation']")
		    public static WebElement Verify_SampleAccomodation_Page;
		  
		    @FindBy(xpath = "//*[@class='r-c17_W2543 nw xtab-td '][contains(text(), '1,000')]")
		    public static WebElement Verify_Result;
		 
		  
		   public Script2_pages() {
			 PageFactory.initElements(driver, this);
		 }
		   
		    public void switch_window_left_to_right() throws InterruptedException {
		    	Actions action= new Actions(driver);
		    	action.keyDown(Keys.CONTROL).sendKeys(Keys.TAB).build().perform();   
		    /*rrayList tabs = new ArrayList (driver.getWindowHandles());
		    System.out.println(tabs.size());
		    driver.switchTo().window(tabs.get(0));*/    
						
					 }
		    

		    public void switch_window_right_to_left() throws InterruptedException {
		    	Actions action= new Actions(driver);
			    action.keyDown(Keys.CONTROL).keyDown(Keys.SHIFT).sendKeys(Keys.TAB).build().perform();      
						
					 }
		    
		   
		   public void dropdown_excel_pdf_click() throws InterruptedException {
			   JavascriptExecutor js = (JavascriptExecutor) driver;
			   js.executeScript("scroll(1024,0);", dropdown_excel_pdf);
			   waitForElementToDisplay(dropdown_excel_pdf);
			   clickElementUsingJavaScript(driver,dropdown_excel_pdf);
			   clickElementUsingJavaScript(driver,Export);
			         
					
				 }
		   
		   public void dropdown_excel_pdf_click2() throws InterruptedException {
				 waitForElementToDisplay(dropdown_excel_pdf);
				 JavascriptExecutor js = (JavascriptExecutor) driver;
				   js.executeScript("scroll(1024,0);", dropdown_excel_pdf);
				   /*waitForElementToDisplay(dropdown_excel_pdf);
				   clickElement(dropdown_excel_pdf);
				   clickElementUsingJavaScript(Exportexcel);*/
				         
					
				 }
		   
		  
			   public boolean CheckFile() // the name of the zip file which is obtained, is passed in this method
			       {
			  		 
			  		 boolean flag = false;
			  		    File dir = new File("C:/Users/STUTI-KATAREY/Downloads");
			  		    File[] dir_contents = dir.listFiles();
			  		  	    
			  		    for (int i = 0; i < dir_contents.length; i++) {
			  		        if (dir_contents[i].getName().equals("Allowance Calculation Mercer Rate All Ranges - Quick View.xlsx"))
			  		            return true;
			  		            }

			  		    return flag;
			       }
			  

			
		 
		 public void Location_Dropdown_Click() throws InterruptedException {
			 waitForElementToDisplay(Location_Dropdown);
			 selEleByVisbleText(Location_Dropdown, "Buenos Aires, Argentina");
			 Thread.sleep(1000);       
				
			 }
		 
		 public void SurveyDate_Dropdown_Click() throws InterruptedException {
			 waitForElementToDisplay(SurveyDate_Dropdown);
			 selEleByVisbleText(SurveyDate_Dropdown, "September 2018");
			 Thread.sleep(1000);
				
			 }
		 
		 public void Policy_Dropdown_Click() throws InterruptedException {
			 waitForElementToDisplay(Policy_Dropdown);
			 selEleByVisbleText(Policy_Dropdown, "Advanced Standard");
			 Thread.sleep(1000);
				
			 }
		 
		 public void View_Button_Click() throws InterruptedException {
			 waitForElementToDisplay(View_Button);
			 clickElement(View_Button);
			 Thread.sleep(1000);
				
			 }
		 
		 public void Home_tab_Click() throws InterruptedException {
			 JavascriptExecutor js = (JavascriptExecutor) driver;
			 js.executeScript("scroll(0,0);", SampleAccomodation_tab);
			 waitForElementToDisplay(Home_tab);
			 clickElement(Home_tab);
			 Thread.sleep(1000);
				
			 }
		 
		 public void SampleAccomodation_tab_Click() throws InterruptedException {
			 JavascriptExecutor js = (JavascriptExecutor) driver;
			 js.executeScript("scroll(1024,0);", SampleAccomodation_tab);
			 clickElement(SampleAccomodation_tab);
			 
			 }
		 
		 public void Location_Dropdown_Click2() throws InterruptedException {
			 waitForElementToDisplay(LocationUS_Dropdown);
			 selEleByVisbleText(LocationUS_Dropdown, "United States");
			 Thread.sleep(1000);
				
			 }
		 
		 public void SurveyDate_Dropdown_Click2() throws InterruptedException {
			 selEleByVisbleText(SurveyDate_Dropdown, "September 2018");
			 Thread.sleep(1000);
				
			 }
		 
		 
		 public void Policy_Dropdown_Click2() throws InterruptedException {
			 
			 waitForElementToDisplay(Policy_Dropdown);
			 selEleByVisbleText(Policy_Dropdown, "Advanced Allowance All Ranges");
			 Thread.sleep(1000);
				
			 }
		 
		 public void Currency_tab_Click() throws InterruptedException {
			 clickElement(ResetAllSelector_Button);
			 Thread.sleep(1000);
				
		 }
		 
		 public void ChangeCurrency_tab_Click() throws InterruptedException {
			 clickElement(ChangeCurrency_tab);
			 //Thread.sleep(1000);
				
		 }
		 
		 public void ChangeCurrency_dropdown_Click() throws InterruptedException {
			 waitForElementToDisplay(ChangeCurrency_dropdown);
			 selEleByVisbleText(ChangeCurrency_dropdown, "Euro EUR");
			 Thread.sleep(10000);
			 //Thread.sleep(1000);
				
		 }
		 public void MAP_tab_Click() throws InterruptedException {
			 //JavascriptExecutor js = (JavascriptExecutor) driver;
			 //js.executeScript("scroll(1024,0);", dropdown_excel_pdf);
			 waitForElementToDisplay(MAP_tab);
			 clickElement(MAP_tab);
			 waitForElementToDisplay(Verify_MAP_tab);
			 
			 //Thread.sleep(1000);
				
			 }
		 
		 
		
			 public static boolean isFileDownloaded(String downloadPath, String fileName) {
			 		  File dir = new File(downloadPath);
			 		  File[] dirContents = dir.listFiles();

			 		  for (int i = 0; i < dirContents.length; i++) {
			 		      if (dirContents[i].getName().equals(fileName)) {
			 		          // File has been found, it can now be deleted:
			 		          dirContents[i].delete();
			 		          return true;
			 		      }
			 		          }
			 		      return false;
			 		  } 

		 
		 
		 
		 
		 
		 
		 
		
		 
		 
			 
		 
		 public void logout_Click() throws InterruptedException {
			 clickElement(logout);
			 Thread.sleep(1000);
			 
			 
			 }
		 
		}
