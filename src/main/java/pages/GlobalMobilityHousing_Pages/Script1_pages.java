package pages.GlobalMobilityHousing_Pages;

import static driverfactory.Driver.setInput;
import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import driverfactory.Driver;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.hoverAndClickOnElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;

public class Script1_pages {

	@FindBy(xpath = "//div[@id='projects_ProjectsStyle']/table/tbody/tr[4]/td[1]/div/a")
	public static WebElement Mercer_Mobility_folder;

	@FindBy(xpath = "//input[@id='Uid']")
	WebElement UserIdTextBox;

	@FindBy(xpath = "//input[@class='mstrButton']")
	public static WebElement SubmitButton;

	@FindBy(xpath = "//*[@id=\"dktpSectionView\"]/div[2]/table/tbody/tr[1]/td[1]/table/tbody/tr/td[2]/div[1]/a")
	public static WebElement Shared_Reports;

	@FindBy(xpath = "//table[@id='FolderList']/tbody/tr/td[2]/span/a")
	public static WebElement Housing_Folder;

	@FindBy(xpath = "//table[@id='FolderList']/tbody/tr[4]/td[2]/span/a")
	public static WebElement Housing_application_landingPage;

	@FindBy(xpath = "//select[@id='W1086_Sl']")
	WebElement Location_Dropdown;

	@FindBy(xpath = "//select[@id='W1094_Sl']")
	WebElement SurveyDate_Dropdown;

	@FindBy(xpath = "//select[@id='W1097_Sl']")
	WebElement Policy_Dropdown;

	@FindBy(xpath = "//span[@id='W688']/a")
	WebElement View_Button;

	@FindBy(xpath = "//input[@id='mstr840']")
	WebElement Apartment_checkbox;

	@FindBy(xpath = "//*[@class='r-c32_K7250 nw xtab-td '][contains(text(),'12,000')]")
	public static WebElement Verify_Apartment_UnCheck;

	@FindBy(xpath = "//input[@id='mstr840']")
	public static WebElement Verify_Mercer_Standard_policy_page;

	@FindBy(xpath = "//*[@id=\"mstr109\"]/table/tbody/tr[5]/td[1]")
	public static WebElement Verify_Apartment_uncheck_result;

	@FindBy(xpath = "//div[text()='Reset All Selectors']")
	WebElement ResetAllSelector_Button;

	@FindBy(css = "table[class='mstrmojo-DocLayout-HBox-HorizContainerTable']")
	WebElement tabs;

	@FindBy(xpath = "//*[contains(text(),'Map')]")
	WebElement MAP_tab;

	@FindBy(xpath = "//*[contains(text(),'Manage My Pushpins')]")
	public static WebElement Verify_MAP_tab;

	@FindBy(xpath = "//*[contains(text(),'General Info')]")
	WebElement GeneralInfo_tab;

	@FindBy(xpath = "//div[text()='General Information']")
	public static WebElement Verify_GeneralInfo_tab;
	// div[contains(@id , '*lK36*kW1124*x1*t1542802789926')]
	@FindBy(xpath = "//*[contains(text(),'Home')]")
	WebElement Home_tab;

	@FindBy(xpath = "//span[@id='W688']/a")
	public static WebElement Verify_HOME_tab;

	@FindBy(xpath = "//*[contains(text(),' Sign Out')]")
	WebElement logout;

	@FindBy(xpath = "//div[@id='divLoggedOutConfirm']/div[2]/div/div[1]")
	public static WebElement Verify_logout;

	public Script1_pages() {
		PageFactory.initElements(driver, this);
	}

	public void Mercer_Mobility_folder_Click() throws InterruptedException {
		waitForElementToDisplay(Mercer_Mobility_folder);
		clickElement(Mercer_Mobility_folder);
		Thread.sleep(1000);

	}

	public void login(String username) {
		// waitForElementToDisplay(UserIdTextBox);
		setInput(UserIdTextBox, username);
		// waitForElementToDisplay(SubmitButton);
		clickElement(SubmitButton);
	}

	public void Shared_Reports_click() throws InterruptedException {
		clickElement(Shared_Reports);
		Thread.sleep(1000);

	}

	public void Housing_Folder_Click() throws InterruptedException {
		// waitForElementToDisplay(Housing_Folder);
		clickElement(Housing_Folder);
		Thread.sleep(1000);

	}

	public void Housing_application_landingPage_Click() throws InterruptedException {
		// waitForElementToDisplay(Housing_Folder);
		clickElement(Housing_application_landingPage);
		Thread.sleep(1000);

	}

	public void Location_Dropdown_Click() throws InterruptedException {
		waitForElementToDisplay(Location_Dropdown);
		selEleByVisbleText(Location_Dropdown, "New York City, United States");
		Thread.sleep(1000);

	}

	public void SurveyDate_Dropdown_Click() throws InterruptedException {
		selEleByVisbleText(SurveyDate_Dropdown, "September 2018");
		Thread.sleep(1000);

	}

	public void Policy_Dropdown_Click() throws InterruptedException {
		selEleByVisbleText(Policy_Dropdown, "Advanced Standard");
		Thread.sleep(1000);

	}

	public void View_Button_Click() throws InterruptedException {

		clickElement(View_Button);
		Thread.sleep(10000);

	}

	public void Apartment_checkbox_Click() throws InterruptedException {
		clickElement(Apartment_checkbox);
		Thread.sleep(10000);

	}

	public void Apartment_refresh() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		try {
			wait.until(ExpectedConditions.stalenessOf(Verify_Apartment_uncheck_result));
		} catch (TimeoutException e) {
			System.out.println("Page is not refreshed in 30 seconds");
		}
	}


	public void MAP_tab_Click() throws InterruptedException {
		clickElement(MAP_tab);
		waitForElementToDisplay(Verify_MAP_tab);

	}

	public void GeneralInfo_tab_Click() throws InterruptedException {
		waitForElementToDisplay(GeneralInfo_tab);
		clickElement(GeneralInfo_tab);
		Thread.sleep(10000);

	}

	public void Home_tab_Click() throws InterruptedException {
		waitForElementToDisplay(Home_tab);
		clickElement(Home_tab);
		Thread.sleep(1000);

	}

	public void logout_Click() throws InterruptedException {
		clickElement(logout);
		Thread.sleep(1000);

	}

	public void VerifyElementTitle() throws InterruptedException {
		String actualTitle = driver.getTitle();
		String expectedTitle = "";
		assertEquals(expectedTitle, actualTitle);
		Thread.sleep(1000);

	}

}