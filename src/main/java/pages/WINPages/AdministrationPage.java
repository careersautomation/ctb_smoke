package pages.WINPages;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdministrationPage {
	@FindBy(xpath="//button[@class='medium action reset-mydata-button']")
	public WebElement resetData_Button;
	
	@FindBy(xpath="//button[@class='medium action mjl-button']")
	public WebElement mercerJobLibrary_button;
	
	@FindBy(xpath="//button[@class='medium action add-mercer-button']")
	public WebElement addMercerData;
	
	@FindBy(xpath="//div[@class='popup has-title size-to-fit']//div[@class='title']")
	public WebElement addMercerData_popup;
	
	@FindBy(xpath="//div[@class='popup has-title size-to-fit']//button[@class='medium ok']")
	public WebElement ok_button;
	
	@FindBy(xpath="//button[@class='medium action job-arch-button']")
	public WebElement manageJobAchitecture_Tab;
	
	@FindBy(xpath="//a[text()='Job Architecture']")
	public static WebElement JobAchitecture_Tab;
	
	@FindBy(xpath="//button[@class='medium action import-data-button']")
	public static WebElement Import_Center;
	
	@FindBy(xpath="//h1[text()='Import Center']")
	public static WebElement Import_Center_page ;
	
	public AdministrationPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void JobArchitecture() throws InterruptedException {
		waitForElementToDisplay(manageJobAchitecture_Tab);
		clickElement(manageJobAchitecture_Tab);
		waitForPageLoad(driver);
	}
	
	public void accessAdminTab(WebElement ele) {
		waitForElementToClickable(ele);
		clickElementUsingJavaScript(driver,ele);
	}
	public void ImportCenter() throws InterruptedException {
		waitForElementToDisplay(Import_Center);
		clickElement(Import_Center);
		waitForPageLoad(driver);
	}
}
