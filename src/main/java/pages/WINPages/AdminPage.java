package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminPage {
	
	@FindBy(xpath="//div[@id='page-header']//li[@class='has-dropdown']//i[@class='m-icon-menu']//parent::a")
	public static WebElement Tools;
	
	@FindBy(xpath="//a[contains(text()[normalize-space()],'Administration')]")
	public static WebElement Administration;
	
	@FindBy(xpath="//h1[contains(text(),'Mercer WIN - System Administration')]")
	public static WebElement admintitle;
	
	Thinking thinking =new Thinking();
	
	public AdminPage()
	{
		PageFactory.initElements(driver, this);
		
	}

	public void Adminbutton() throws IOException, InterruptedException
	{
		waitForElementToDisplay(Tools);
		clickElement(Tools);
		waitForElementToDisplay(Administration);
		clickElement(Administration);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
	}

}
