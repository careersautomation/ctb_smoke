package pages.WINPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.*;

import java.io.IOException;

public class Global_PrintFunctionality {
	
	@FindBy(xpath="//button[@class='tab medium icon chart-tab-button']")
	public static WebElement chart_Tab;
	
	@FindBy(xpath="//button[@class='tab medium icon chart-tab-button selected']")
	public static WebElement selectedchart_Tab;
	
	@FindBy(xpath="//div[@class='edit-box select-box chart-name-selector chart-selector form-element']//a")
	public static WebElement selectChartName_Dropdown;
	
	@FindBy(xpath="//a[contains(text(),'Pay Range By Component')]")
	public static WebElement selectChart;

	@FindBy(xpath="//button[@class='ok medium chart-refresh-button form-element']")
	public static WebElement refreshChart_Button;
	
	@FindBy(xpath="//button[@title='Print']")
	public static WebElement print_Button;
	
	@FindBy(xpath="//button[@class='apply-button action-button medium next ok']")
	public static WebElement continue_Button;

	@FindBy(xpath="//button[@class='tab medium icon table-tab-button']")
	public static WebElement table_Chart;
	
	@FindBy(xpath="//button[@class='tab medium icon table-tab-button selected']")
	public static WebElement selectedtable_Chart;
	
	@FindBy(xpath="//div[@class='list-item data-node category']//a[contains(text(),'Grid')]")
	public static WebElement Grid_Tab;
	
	Thinking thinking=new Thinking();
	public Global_PrintFunctionality() {
		PageFactory.initElements(driver, this);
	}
	
	public void chartPrint() throws IOException, InterruptedException {
	
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToClickable(chart_Tab);
		 clickElementUsingJavaScript(driver,chart_Tab);
		 System.out.println("chart selected");
		 thinking.waitForInvisibilityOfThinkingSpinner();
		 thinking.waitForInvisibilityOfThinkingSpinner();
		 waitForElementToClickable(print_Button);
		 clickElementUsingJavaScript(driver,print_Button);
		 thinking.waitForInvisibilityOfThinkingSpinner();
		 thinking.waitForInvisibilityOfThinkingSpinner();
		 waitForElementToClickable(continue_Button);
		 clickElement(continue_Button);
		 thinking.waitForInvisibilityOfThinkingSpinner();
		 thinking.waitForInvisibilityOfThinkingSpinner();
	} 
		 
	
	public void GridPrint() throws IOException, InterruptedException {
		thinking.waitForInvisibilityOfThinkingSpinner();
		 thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToDisplay(table_Chart);
		clickElementUsingJavaScript(driver,table_Chart);
		thinking.waitForInvisibilityOfThinkingSpinner();
		 thinking.waitForInvisibilityOfThinkingSpinner();
		System.out.println("table selected");
		clickElement(print_Button);
		thinking.waitForInvisibilityOfThinkingSpinner();
		 thinking.waitForInvisibilityOfThinkingSpinner();
		clickElement(Grid_Tab);
		thinking.waitForInvisibilityOfThinkingSpinner();
		 thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToClickable(continue_Button);
		clickElement(continue_Button);
		thinking.waitForInvisibilityOfThinkingSpinner();
		 thinking.waitForInvisibilityOfThinkingSpinner();
		
	}
	
}
