package pages.WINPages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.*;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ageDataPage {

	@FindBy(xpath="//button[@data-purpose='age-data']")
	public static WebElement ageData_Tab;

	@FindBy(xpath="//input[@data-purpose='age-market-data']")
	public static WebElement ageMarketData_Checkbox;
	
	@FindBy(xpath="//input[@data-purpose='age-survey-participation-data']")
	public static WebElement ageMyData_Checkbox;
	
	@FindBy(xpath="//button[@data-purpose='target-date']")
	public static WebElement date_Tab;
	
	@FindBy(xpath="//button[@data-pika-day='7']")
	public static WebElement pickDate;
	
	@FindBy(xpath="//input[@data-purpose='first-year-percentage' and @data-index='0']")
	public static WebElement firstPublicationYear_Tab;
	
	@FindBy(xpath="//input[@data-purpose='following-years-percentage' and @data-index='0']")
	public static WebElement firstNextYear_Tab;
	
	@FindBy(xpath="//input[@data-purpose='first-year-percentage' and @data-index='1']")
	public static WebElement secondPublicationYear_Tab;
	
	@FindBy(xpath="//input[@data-purpose='following-years-percentage' and @data-index='1']")
	public static WebElement secondNextYear_Tab;
	
	@FindBy(xpath="//button[@data-purpose='calculate']")
	public static WebElement calculate_Button;
	
	@FindBy(xpath="//button[@data-purpose='apply']")
	public static WebElement apply_Button;
	
	@FindBy(xpath="//button[@class='report-control-button age-data-task btn-on']//following::div[1]")
	public static WebElement ageON_resultpage;
	
	public ageDataPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void clickageData() {

		waitForElementToDisplay(ageData_Tab);
		clickElementUsingJavaScript(driver,ageData_Tab);
	}
	
	public void ageData() {
		waitForElementToClickable(ageMarketData_Checkbox);
		clickElement(ageMarketData_Checkbox);
		clickElement(ageMyData_Checkbox);
		clickElement(date_Tab);
		clickElement(pickDate);
		setInput(firstPublicationYear_Tab , "23");
		setInput(firstNextYear_Tab , "35");
		setInput(secondPublicationYear_Tab , "23");
		setInput(secondNextYear_Tab , "35");
		clickElement(secondPublicationYear_Tab);
		clickElement(calculate_Button);
		waitForElementToClickable(apply_Button);
		clickElementUsingJavaScript(driver,apply_Button);
		
	}
	
	
}



