package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getElementWithTitle;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;
import static driverfactory.Driver.waitForPageLoad;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyJobs_Resultspage {
	
	@FindBy(xpath="//input[@data-record='0']")
    public static WebElement selectresult1;
	
	@FindBy(xpath="//input[@data-record='1']")
    public static WebElement selectresult2;
	
	@FindBy(xpath="//input[@data-record='2']")
    public static WebElement selectresult3;
	
	@FindBy(xpath="//input[@data-record='3']")
    public static WebElement selectresult4;
	    
	@FindBy(xpath="//button[@class='large ok next continue-button']")
	public static WebElement Continue_to_reults;
	
	@FindBy(xpath = "//button[@class='image benchmarking-icon']")
    public static WebElement benchmarkicon ;
    
    @FindBy(xpath="//button[@class='large save-as-button']//b[text()='Save As']")
    public static WebElement saveas;
    
    @FindBy(xpath="//div[@class='group edit-box new-result-name form-element']//input[@type='text']")
    public static WebElement saveasinput;
    
    @FindBy(xpath="//button[@class='action medium ok']")
    public static WebElement Save;
    
    @FindBy(xpath="//span[text()='My Saved Results']//parent::a")
    public static WebElement savedresultset;
    
    @FindBy(xpath="//div[@class='list-item  data']//following::a[1]")
    public static WebElement savedresultsetselect;
    
    @FindBy(xpath="//div[@class='popup market-pricing-popup has-title size-to-page size-specific']//button[@class='action close-button medium']")
    public static WebElement benchmarkclose;
    
    @FindBy(xpath="//*[@id=\\\"organization\\\"]/div/div/select")
    public static WebElement joborg;
    
    @FindBy(xpath="//b[text()='Save and Close']")
    public static WebElement addjobclose;
    
    @FindBy(xpath="//button[text()='Add New Job']")
    public static WebElement addjob;
    
    @FindBy(xpath="//option[text()='Test org 1 (1)']")
    public static WebElement joborgselect;
    
    @FindBy(xpath="//b[text()='Continue']")
	public WebElement Continue;
    
    @FindBy(xpath="//a[text()='My Jobs']")
	public static WebElement pagetitle;
    
    @FindBy(xpath="//div[@id='page-title']//h1")
   	public static WebElement title;
    
    @FindBy(xpath="//button[@class='action medium ms-market-data-button ok']")
	public static WebElement benchmarkdetailsload;
    
    public static String resultsetname="1";
	
    Thinking thinking =new Thinking();
    My_jobs job=new My_jobs();
	 public MyJobs_Resultspage(){
			PageFactory.initElements(driver, this);
		}
	
public void myjobs_results_page() throws InterruptedException, IOException {
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
	waitForElementToEnable(selectresult1);
	clickElement(selectresult1);
	waitForElementToEnable(selectresult2);
	clickElement(selectresult2);
	scrollToElement(driver,selectresult3);
	waitForElementToEnable(selectresult3);
	clickElement(selectresult3);
	scrollToElement(driver,selectresult4);
	waitForElementToEnable(selectresult4);
	clickElement(selectresult4);
	waitForElementToEnable(Continue_to_reults);
	clickElement(Continue_to_reults);
	waitForPageLoad();
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
	waitForElementToEnable(job.jobdetails);
	clickElement(job.jobdetails);
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
	//delay(5000);
	waitForElementToEnable(job.jobdetailsload);
	waitForElementToEnable(job.jobdetailsclose);
	
}
public void jobdetailsclose() throws IOException, InterruptedException {
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
	clickElement(job.jobdetailsclose);
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
}
public void benchmarkicon() throws IOException, InterruptedException {
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
	waitForElementToEnable(benchmarkicon);
	clickElement(benchmarkicon);
	//delay(3000);
	thinking.waitForInvisibilityOfThinkingSpinner();
	waitForElementToEnable(benchmarkdetailsload);
	waitForElementToEnable(benchmarkclose);
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
}
public void benchmarkclose() throws IOException, InterruptedException {
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
	clickElement(benchmarkclose);
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
}
public void save() throws IOException, InterruptedException {
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
	waitForElementToDisplay(saveas);
	clickElement(saveas);
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
	waitForElementToDisplay(saveasinput);
	setInput(saveasinput,resultsetname);
	waitForElementToDisplay(Save);
	clickElement(Save);
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
	waitForElementToDisplay(Continue);
	clickElement(Continue);
	//delay(2000);
	waitForPageLoad();
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
	waitForElementToEnable(job.jobdetails);
}
public void resultset() throws IOException, InterruptedException {
	waitForElementToEnable(savedresultset);
	clickElement(savedresultset);
	WebElement resultsetaccess=getElementWithTitle("a",resultsetname);
	clickElement(resultsetaccess);
	delay(2000);
	waitForPageLoad();
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
	waitForElementToEnable(job.jobdetails);
}
}
