package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import java.io.IOException;

import static driverfactory.Driver.waitForElementToEnable;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Select_Organization {
	
		
	@FindBy(xpath="//*[contains(text(),'WIN SM Test [SM210]')]")
	public static WebElement orgselect;
	
	@FindBy(xpath="//b[text()='Continue']")
	public static WebElement orgselected;
	
	@FindBy(xpath ="//b[contains(text(), 'Sign Out')]")
	public static WebElement signOut_link;
	
	@FindBy(xpath="//div[@id='breadcrumb']//a[text()='Home']")
	public static WebElement Homepage;

	@FindBy(xpath="//button[@class='medium ok search icon']")
	public static WebElement Dashboard;
	
	Thinking thinking =new Thinking();
	
public Select_Organization() {
	PageFactory.initElements(driver, this);
}

  public void select_organization() throws IOException, InterruptedException {
     waitForElementToDisplay(orgselect);
     clickElement(orgselect);
     waitForElementToDisplay(orgselected);
     
     /*WebElement element = driver.findElement(By.xpath("//button[@id='doneButton']"));
	  JavascriptExecutor executor = (JavascriptExecutor)driver;
	  executor.executeScript("arguments[0].click();", element);
     */
     clickElement(orgselected);
     waitForPageLoad();
     //delay(3000);
     
     //thinking.waitForInvisibilityOfThinkingSpinner();
     //thinking.waitForInvisibilityOfThinkingSpinner();
     delay(3000);
     waitForElementToEnable(Dashboard);
   } 
  
}