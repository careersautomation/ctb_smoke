package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;
import static driverfactory.Driver.selEleByVisbleText;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class My_Jobs_Add_New_Job {
	
	@FindBy(xpath = "//input[@data-property='JobTitle']")
    public static WebElement jobtitle;
    
    @FindBy(xpath="//*[@id=\\\"search-criteria\\\"]/div[10]/div[1]/div[4]/button/div/b")
    public static WebElement jobsearch;
    
    @FindBy(xpath="//b[text()='Manage Jobs']//ancestor::button")
    public static WebElement managejob;
    
    @FindBy(xpath="/html/body/div[23]/div[10]/div/div[1]/div/div[1]/div[2]/div/div[3]/input")
    public static WebElement jobcode;
    
    @FindBy(xpath="//div[@data-property='JobCodeExt']//input[@class='display ellipsis']")
    public static WebElement jobext;
    
    @FindBy(xpath="//select[@data-property='CountryCode']")
    public static WebElement jobcountry;
    
    @FindBy(xpath="//option[text()='Albania (AL)']")
    public static WebElement jobcountryselect;
    
    @FindBy(xpath="//span[@class='combobox-title']")
    public static WebElement jobexttwisty;
    
    @FindBy(xpath=" //div[@data-property='JobCodeExt']//div[@class='body']")
    public static WebElement jobexttwistyexpand;
    
    @FindBy(xpath="//select[@data-property='OrganizationId']")
    public static WebElement joborg;
    
	@FindBy(xpath="//b[text()='Save']")
	public static WebElement Save;
	
	@FindBy(xpath="//button[@class='action close-button medium ok']//following::b")
	public static WebElement Close;
    
    @FindBy(xpath="//div[contains(@class,'popup flyout manage-jobs-flyout')]//button[text()='Add New Job']")
    public static WebElement addjob;
    
    @FindBy(xpath="//select[@data-property='OrganizationId']//following::option[1]")
    public static WebElement joborgselect;
	
    @FindBy(xpath="//div[@class='popup bench-job-details-popup full-page has-title size-to-page size-specific']//div[@data-form-item-property='JobTitle']//child::div")
    public static WebElement jobnameverify;
    
    @FindBy(xpath = "//b[text()='Market Comparisons']")
    public static WebElement jobdetailsload;
    
    @FindBy(xpath="//*[@class='body']//*[text()='Search']")
    public static WebElement myjobssearch;
    
    @FindBy(xpath="//a[text()='1']")
    public static WebElement jobextselect;
  
    
    public static String JobExt="1";
	
    Random rg = new Random();	
	int randomInt = rg.nextInt(100000);	
	String numberAsString = Integer.toString(randomInt);	
	public String Jobtitle="Job"+numberAsString;
	
    Thinking thinking = new Thinking();
public My_Jobs_Add_New_Job() {
	PageFactory.initElements(driver, this);
}

public void add_job() throws IOException, InterruptedException {
	
	waitForElementToDisplay(myjobssearch);
	clickElement(myjobssearch);
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
	waitForElementToDisplay(managejob);
	clickElement(managejob);
	delay(1000);
	List<WebElement> elements= driver.findElements(By.xpath("//button[text()='Add New Job']"));
	for(WebElement element: elements){
		if(element.isDisplayed()){
			element.click();
		}
	}
	//delay(5000);
	waitForElementToDisplay(jobtitle);
	setInput(jobtitle,Jobtitle);
	//waitForElementToDisplay(jobcode);
	//setInput(jobcode,"1004");
	waitForElementToDisplay(jobexttwistyexpand);
	clickElement(jobexttwistyexpand);
	waitForElementToDisplay(jobext);
	//setInput(jobext,JobExt);
	waitForElementToDisplay(jobextselect);
	clickElement(jobextselect);
	//waitForElementToDisplay(jobexttwisty);
	//clickElement(jobexttwisty);
	waitForElementToDisplay(jobcountry);
	clickElement(jobcountry);
	waitForElementToDisplay(jobcountryselect);
	clickElement(jobcountryselect);
	waitForElementToDisplay(joborg);
	clickElement(joborg);
	waitForElementToDisplay(joborgselect);
	clickElement(joborgselect);
	//waitForElementToDisplay(joborg);
	//clickElement(joborg);
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
	waitForElementToDisplay(Save);
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
	delay(3000);
	clickElement(Save);
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
	delay(2000);
	waitForElementToDisplay(Close);
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
}
public void add_job_close() throws IOException, InterruptedException {
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
	clickElement(Close);
	thinking.waitForInvisibilityOfThinkingSpinner();
	thinking.waitForInvisibilityOfThinkingSpinner();
}

}
