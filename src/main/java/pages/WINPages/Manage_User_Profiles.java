package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.InitTests;

public class Manage_User_Profiles {
	
	@FindBy(xpath="//div/b[contains(., 'Manage User Profiles')]")
	public static WebElement ManageUserProfiles;
	
	@FindBy(xpath="//div[@class='data-grid display-below portal']//div[@class='scroll-size']//tr[1]//child::a[1]")
	public static WebElement link;
	
	@FindBy(xpath="//button[@class='medium ok save-button']")
	public static WebElement Done;
	
	@FindBy(xpath="//div[@id='breadcrumb']//a[contains(text()[normalize-space()],'Administration')]")
	public static WebElement Administration;
	
	@FindBy(xpath= "//button[@class='medium action eval-appr-button']")
	public static WebElement ManageEvaluationApprovals;
	
	@FindBy(xpath="//button[@class='medium action profile-button' and @data-purpose='users-report']")
	public static WebElement UserReports;
	
	@FindBy(xpath="//a[text()='User Profile Administration']")
	public static WebElement ManageUserProfile;
	
	Thinking thinking=new Thinking();
	
	public Manage_User_Profiles()
	{
		PageFactory.initElements(driver, this);
		
	}

	public void Profiles() throws IOException, InterruptedException
	{
		waitForElementToDisplay(ManageUserProfiles);
		clickElement(ManageUserProfiles);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
	}
	public void Profilelink() throws IOException, InterruptedException {
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToDisplay(link);
		clickElement(link);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToDisplay(Done);
		clickElement(Done);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToDisplay(Administration);
		clickElement(Administration);
		
	}

}
