package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

	@FindBy(xpath="//h1[text()='My Homepage']")
	public static WebElement homepage;

	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	public void HomePage()
	{
		waitForElementToDisplay(homepage);
	}	
}

