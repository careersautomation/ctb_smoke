package pages.WINPages;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProgressCenter {
	
	@FindBy(xpath="//button[@class='icon image plus']")
	public  WebElement Progress_Center;
	
	@FindBy(xpath="//button[@data-purpose='show-progress-center']")
	public  WebElement JobprogressCenter_button;
	
	@FindBy(xpath="//button[@class='image plus']")
	public  WebElement GlobalprogressCenter_button;
	
	@FindBy(xpath="//div[@class='progress']")
	public WebElement GlobalProgress_close;
	
	 @FindBy(xpath="//button[@class='icon image plus']")
	public WebElement DiagnosticProgressCenter_button;
	
	@FindBy(xpath="//div[@class='data-grid display-below portal fixed-width']//table[@class='data-table']//tbody//tr[@class='odd'][1]//td[@class='col0 root']//span[text()='Complete']")
	public  static WebElement Jobcomplete_status;
	
	@FindBy(xpath="//div[@class='data-grid display-below portal fixed-width']//table[@class='data-table']//tbody//tr[@class='odd'][1]//td[@class='col1 root']//a[@class='report-name']")
	public static  WebElement Jobreport_name;
	
	@FindBy(xpath="//div[@class='alert-grid data-grid display-below portal fixed-width']//table[@class='data-table']//tbody//tr[@class='odd'][1]//td[@class='col0 root']//span[text()='Complete']")
	public  static WebElement Globalcomplete_status;
	
	@FindBy(xpath="//div[@class='alert-grid data-grid display-below portal fixed-width']//table[@class='data-table']//tbody//tr[@class='odd'][1]//td[@class='col1 root']//a[@class='report-name']")
	public static  WebElement Globalreport_name;
	
	@FindBy(xpath="//div[@class='data-grid display-below portal fixed-width']//div[@class='table-portal' and @style]")
	public static WebElement MJlreport_Table;
	
	@FindBy(xpath="//div[@class='report-queue-alert-box popup container' and @style='display: block; z-index: 1002;']")
	public static WebElement globalreport_Table;
	
	@FindBy(xpath="//div[@class='popup report-queue-alert-box has-title size-specific']//table[@class='data-table']//tr[@class='odd'][1]//td[@class='col0 root'][1]//span[text()='Complete']")
	public  static WebElement complete_status;
	
	@FindBy(xpath="//div[@class='popup report-queue-alert-box has-title size-specific']//table[@class='data-table']//tr[@class='odd'][1]//td[@class='col0 root'][1]//following::a[1]")
	public static  WebElement report_name;
	
	@FindBy(xpath="//div[contains(@class,'report-queue-alert-box')]")
	public static  WebElement reportTable;
	
	@FindBy(xpath="//div[@id='grid0']//table[contains(@class,'data-table')]")
	public static  WebElement progtable;
	
	
	Thinking thinking=new Thinking();
	public  ProgressCenter() {
		PageFactory.initElements(driver, this);
	}
	
	public void waitReportGeneration() {
		delay(30000); 
	}
	public void waitReportGenerate() {
		delay(10000);
	}
	public void waitdiagnosticReportGeneration() {
		delay(35000); 
	}
	
	public void waitforReportGeneration() {
		delay(35000); 
	}
	
	 public void  progress(WebElement element ) {
		 delay(8000);
		 //driver.switchTo().frame(driver.findElement(By.xpath("//iframe[contains(@class,'overlay clear')]")));
		waitForElementToClickable(element);
		clickElement(element);
		delay(2000);
	 }
	 public void  closeprogress() {
		// delay(8000);
		 //driver.switchTo().frame(driver.findElement(By.xpath("//iframe[contains(@class,'overlay clear')]")));
		 driver.findElement(By.xpath("//iframe[contains(@class,'overlay clear')]")).click();
		// driver.switchTo().defaultContent();
		//waitForElementToClickable(element);
		//clickElement(element);
		//delay(2000);
	 }
	 
	 public void generateReport() throws IOException, InterruptedException {
		 for(int i=1;i<8;i++)
		 {
			if(driver.findElements(By.xpath("//div[@class='popup report-queue-alert-box has-title size-specific']//table[@class='data-table']//tr[@class='odd'][1]//td[@class='col0 root'][1]//span[text()='Complete']")).size()==0)
			{
				closeprogress();
				delay(5000);
				clickElement(Progress_Center);
				thinking.waitForInvisibilityOfThinkingSpinner();
				waitForElementToDisplay(progtable);
				waitForElementToDisplay(driver.findElement(By.xpath("//span[text()='Status']")));
				delay(2000);
			}
			else
			{
				break;
			}
		 
	 }
	 }
	
}
