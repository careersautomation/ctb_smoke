package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.waitForElementToClickable;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Sign_out {
	
	@FindBy(xpath="//button//b[text()='Sign Out']")
    public static WebElement signout;
	
	@FindBy(xpath="//button[@class='small logout']")
    public WebElement signout_1;
	
	@FindBy(xpath="//a[@data-purpose='sign-out']")
	public WebElement signout_2;
	
	public Sign_out(){
		PageFactory.initElements(driver, this);
	}
	public void signout() 
	{
		clickElement(signout);
	}
	public void signout(WebElement ele) 
	{
		waitForElementToClickable(ele);
		clickElement(ele);
	}

}
