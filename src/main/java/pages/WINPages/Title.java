package pages.WINPages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Title {
	@FindBy(xpath="//div[@class='title']//h1")
	public  WebElement title_tab;
	
	@FindBy(xpath="//div[@class='titlebar']//h2")
	public WebElement MMD_title;
	
	@FindBy(xpath="//div[@class='modal-header']//h3")
	public WebElement MmdPopUp_Title;
	
	
	
	public Title() {
		PageFactory.initElements(driver, this);
	}
	
}
