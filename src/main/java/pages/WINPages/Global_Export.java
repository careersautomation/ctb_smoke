package pages.WINPages;

import static driverfactory.Driver.*;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Global_Export {
	@FindBy(xpath="//button[@title='Export']")
	public static WebElement export_Button;
	
	@FindBy(xpath="//div[@class='popup-content']//input[@maxlength='200']")
	public static WebElement fileName;
	
	@FindBy(xpath="//button[@class='ok medium']")
	public static WebElement done_Button;
	
	@FindBy(xpath="//*[contains(text(),'Close']")
	public static WebElement close_button;
	
	Thinking thinking=new Thinking();
	public Global_Export() {
		PageFactory.initElements(driver, this);
	}
	
	public void sendGlobalexport() throws IOException, InterruptedException {
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToClickable(export_Button);
		clickElement(export_Button);	
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
	}
	
	public  void GlobalReportfileOptions(String filename) throws IOException, InterruptedException
	{
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToClickable(fileName);
		setInput(fileName , filename);
		clickElement(done_Button);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
	}
	
	public void closePopup() throws IOException, InterruptedException {
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToClickable(close_button);
		clickElement(close_button);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
	}
}
