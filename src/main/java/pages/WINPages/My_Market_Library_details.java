package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class My_Market_Library_details {
	
	@FindBy(xpath="//div[@class='group edit-box keyword-search-criteria']//following::input")
	public static  WebElement keywordentry;
	
	@FindBy(xpath="//button[@class='icon medium ok search-button icon submit']")
	public static  WebElement search;
	
	@FindBy(xpath="//div[@id='grid2']//following::a[1]")
	public static  WebElement recorddetailspopup;
	
	@FindBy(xpath="//div[@id='grid5']//table[@class='data-table']")
	public static  WebElement recorddetailspopupload;
	
	@FindBy(xpath="//div[@id='grid2']//following::a[2]")
	public static  WebElement sourcedetailspopup;
	
	@FindBy(xpath="//div[@class='popup mml-source-detail has-title size-to-page size-specific']//span[@title='Record Identification']")
	public static  WebElement sourcedetailspopupload;

	@FindBy(xpath="//div[@class='popup mml-record-detail has-title size-to-page size-specific']//b[text()='Close']")
	public static  WebElement popupclose;
	
	@FindBy(xpath="//div[@class='popup mml-source-detail has-title size-to-page size-specific']//b[text()='Close']")
	public static  WebElement mmlpopupclose;
	
	@FindBy(xpath="//b[text()='Geography']")
	public static  WebElement Geography;
	
	@FindBy(xpath="//label[text()='All Asia Countries/Regions']//parent::div/input")
	public static  WebElement Allasia;
	
	@FindBy(xpath="//div[contains(@class,'popup search-criteria-popup anchored-popup size-to-fit')]//button[contains(@class,'action-button apply-button medium ok')]")
	public static  WebElement Apply;
	
	Thinking thinking=new Thinking();
	
	public My_Market_Library_details(){
		PageFactory.initElements(driver, this);
	}
	
	public void marketdetails() throws IOException, InterruptedException
	{
		waitForElementToDisplay(keywordentry);
		setInput(keywordentry,"account");	
		waitForElementToDisplay(Geography);
		clickElement(Geography);
		waitForElementToDisplay(Allasia);
		clickElement(Allasia);
		waitForElementToDisplay(Apply);
		clickElement(Apply);
		waitForElementToDisplay(search);
		clickElement(search);
		waitForElementToEnable(recorddetailspopup);
		clickElement(recorddetailspopup);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToEnable(recorddetailspopupload);
		waitForElementToEnable(popupclose);
		//delay(4000);
	}
	public void recorddetailsclose()
	{
			
			clickElement(popupclose);
			waitForElementToEnable(sourcedetailspopup);
	}
	public void sourcedetils() throws IOException, InterruptedException
	{
			
			clickElement(sourcedetailspopup);
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
			waitForElementToDisplay(sourcedetailspopupload);
			waitForElementToEnable(mmlpopupclose);
			//delay(4000);
	}
	public void sourcedetilsclose()
	{
			
			clickElement(mmlpopupclose);	
	}
		
}
