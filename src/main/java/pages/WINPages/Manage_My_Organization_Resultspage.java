package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getElementWithTitle;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;
import static driverfactory.Driver.waitForPageLoad;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Manage_My_Organization_Resultspage {
	
	@FindBy(xpath="//input[@data-record='-1']")
	public static WebElement selectall;
	
	@FindBy(xpath="//button[@class='large ok next continue-button']")
	public static WebElement Continue_to_results;
	
	@FindBy(xpath="//a[@data-record='0']")
	public static WebElement Organizationpopup;
	
	@FindBy(xpath="//button[@class='action close-button medium ok']")
	public static WebElement Close;
	
	@FindBy(xpath="//button[@class='icon row-edit-icon row-edit-button']")
	public static WebElement rowedit;
	
	@FindBy(xpath="//a[contains(text(),'My Organizations Results')]")
	public static WebElement Resultspage;
	
	@FindBy(xpath="//button[@class='large save-as-button']//b[text()='Save As']")
    public static WebElement saveas;
    
    @FindBy(xpath="//div[@class='group edit-box new-result-name form-element']//input[@type='text']")
    public static WebElement saveasinput;
    
    @FindBy(xpath="//button[@class='action medium ok']")
    public static WebElement Save;
    
    @FindBy(xpath="//span[text()='My Saved Results']//parent::a")
    public static WebElement savedresultset;
    
    @FindBy(xpath="//div[@class='list-item  data']//following::a[1]")
    public static WebElement savedresultsetselect;
    
    @FindBy(xpath="//a[text()='My Organizations Results']")
   	public static WebElement pagetitle;
    
    @FindBy(xpath="//div[@id='page-title']//h1")
   	public static WebElement title;
    
    @FindBy(xpath="//b[text()='Continue']")
	public WebElement Continue;
    
    public static String resultsetname="1";
    
    Thinking thinking=new Thinking();
	 public Manage_My_Organization_Resultspage(){
			PageFactory.initElements(driver, this);
		}
	 
	 public void Organization_results_page() throws InterruptedException, IOException {
		 thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
		 waitForElementToEnable(selectall);
			clickElement(selectall);
			waitForElementToEnable(Continue_to_results);
			clickElement(Continue_to_results);
			waitForPageLoad();
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
			delay(2000);
			waitForElementToEnable(Organizationpopup);
			clickElement(Organizationpopup);
			//delay(4000);
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
			waitForElementToEnable(Close);
			
		}
		public void detailsclose() throws IOException, InterruptedException {
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
			clickElement(Close);
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
		}
		public void rowedit() throws IOException, InterruptedException {
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
			clickElement(rowedit);
			waitForPageLoad();
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
		}
		public void Resultspage() throws IOException, InterruptedException {
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
			clickElement(Resultspage);
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
		}
		public void save() throws IOException, InterruptedException {
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
			waitForElementToDisplay(saveas);
			clickElement(saveas);
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
			waitForElementToDisplay(saveasinput);
			setInput(saveasinput,resultsetname);
			waitForElementToDisplay(Save);
			clickElement(Save);
			waitForElementToDisplay(Continue);
			clickElement(Continue);
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
			//delay(3000);
		}
		public void resultset() throws IOException, InterruptedException {
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
			waitForElementToEnable(savedresultset);
			clickElement(savedresultset);
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
			WebElement resultsetaccess=getElementWithTitle("a",resultsetname);
			clickElement(resultsetaccess);
			//delay(2000);
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
			waitForPageLoad();
		}

}
