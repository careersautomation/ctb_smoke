package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Signout {

	@FindBy(xpath="//b[text()='Sign Out']")
	public static WebElement Signout;
	

	public Signout()
	{
		PageFactory.initElements(driver, this);
		
	}

	public void Signout()
	{
		delay(3000);
		waitForElementToDisplay(Signout);
		clickElement(Signout);
}
}
