package pages.WINPages;
import utilities.InitTests;

import static driverfactory.Driver.driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import java.io.IOException;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.setInput;

public class Login {
	@FindBy(id="ctl00_MainSectionContent_Email")
	public static WebElement email;
	
	@FindBy(id="ctl00_MainSectionContent_Password")
	public static WebElement password;

	@FindBy(id="ctl00_MainSectionContent_ButtonSignin")
	public static WebElement signin;	
	
	@FindBy(id="ContentPlaceHolder1_TextBox1")
	public WebElement emailmsso;
	
	@FindBy(id="okta-signin-username")
	public WebElement emailmssologin;
	
	@FindBy(id="okta-signin-password")
	public WebElement passwordmssologin;
	
	@FindBy(id="okta-signin-submit")
	public WebElement signinmsso;
	
	Thinking thinking=new Thinking();
	public Login() {
		PageFactory.initElements(driver, this);
	}
	public void login(String Username,String Password) throws IOException, InterruptedException
	{
		if(driver.findElements(By.id("ctl00_MainSectionContent_Email")).size()!=0)
		{
		setInput(email, Username);
		setInput(password, Password);
		clickElement(signin);
		waitForPageLoad();
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		}
		else
		{
			setInput(emailmsso,"@mercer.com");
			clickElement(driver.findElement(By.id("ContentPlaceHolder1_PassiveSignInButton")));
			delay(5000);
			if(driver.findElements(By.id("okta-signin-username")).size()!=0)
			{
			setInput(emailmssologin, Username);
			setInput(passwordmssologin, Password);
			clickElement(signinmsso);
			}
		}
	}
}
