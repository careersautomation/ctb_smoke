package pages.WINPages;

import static driverfactory.Driver.driver;

import java.io.IOException;

import static driverfactory.Driver.*;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class editViewPage {

	@FindBy(xpath="//button[@data-id='view-edit-control']")
	public static WebElement edit_Dropdown;

	@FindBy(xpath="//button[@data-purpose='view-edit']")
	public static WebElement edit_link;
	
	@FindBy(xpath="//button[@data-id='Base Salary']")
	public static WebElement baseSalary_link;
	
	@FindBy(id="view_option_detail_57")
	public static WebElement Select_BaseSalary;
	
	@FindBy(xpath="//a[@data-id='stats']")
	public static WebElement select_Statistic;
	
	@FindBy(xpath="//button[@data-id='Count Statistics']")
	public static WebElement select_countStatistic;
	
	@FindBy(id="view_option_detail_3")
	public static WebElement Select_DistinctStatistic;
	
	@FindBy(xpath="//button[@data-purpose='save']")
	public static WebElement Save_Button;
	
	@FindBy(id="view-name-input")
	public static WebElement viewName_Tab;
	
	@FindBy(xpath="//button[@class='button-win-action' and @data-purpose='save']")
	public static WebElement viewSave_Button;
	
	@FindBy(xpath="//button[@class='apply-button' and @data-purpose='continue']")
	public static WebElement viewContinue_Button;
	
	@FindBy(css="button[data-purpose='done']")
	public static WebElement Done_Button;
	
	@FindBy(xpath="//span[@class='current-view result']")
	public static WebElement currentViewResult;
	
	@FindBy(xpath="//div[@class='summary data-grid display-below portal']//div[@class='table-portal' and @style]")
	public static WebElement result_page;
	
	Thinking thinking =new Thinking();
	public editViewPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void clickEdit() throws IOException, InterruptedException 
	{
		thinking.waitForInvisibilityOfSpinner();
		waitForPageLoad();
		waitForElementToClickable(edit_Dropdown);
		clickElementUsingJavaScript(driver,edit_Dropdown);
		waitForElementToClickable(edit_link);
		clickElement(edit_link);
		thinking.waitForInvisibilityOfSpinner();
	}
	
	public void select_mercerMarketData() {
		clickElementUsingJavaScript(driver,baseSalary_link);
		clickElement(Select_BaseSalary);
	}
	
	public void select_Statistic() {
		clickElement(select_Statistic);
		clickElement(select_countStatistic);
		clickElement(Select_DistinctStatistic);
	}
	
	public void select_Save() throws IOException, InterruptedException {
		clickElement(Save_Button);
		setInput(viewName_Tab , "Job_2" );
		clickElement(viewSave_Button);
		clickElement(viewContinue_Button);
		waitForPageLoad();
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(Done_Button);
		waitForElementToClickable(Done_Button);
		clickElementUsingJavaScript(driver,Done_Button);
		thinking.waitForInvisibilityOfSpinner();
		
	}
}




