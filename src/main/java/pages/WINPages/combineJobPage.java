package pages.WINPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.*;

import java.io.IOException;



public class combineJobPage {
	
	@FindBy(xpath="//button[@data-purpose='combine-jobs']")
	public static WebElement combineJobs_Tab;
	
	@FindBy(xpath="//input[@data-record='0']")
	public static WebElement selectJob_1;
	
	@FindBy(xpath="//input[@data-record='1']")
	public static WebElement selectJob_2;
	
	@FindBy(xpath="//input[@data-record='2']")
	public static WebElement selectJob_3;
	
	@FindBy(xpath="//button[@data-purpose='combine-jobs']")
	public static WebElement combineButton;
	
	@FindBy(id="combine-job-title")
	public static WebElement jobTitle_Tab;
	
	@FindBy(id="combine-job-code")
	public static WebElement jobCode_Tab;
	
	@FindBy(xpath="//button[@data-purpose='combine-apply']")
	public static WebElement applyButton;
	
	@FindBy(xpath="//button[@data-purpose='save-combined-jobs']")
	public static WebElement DoneButton;
	
	@FindBy(xpath="//span[@title='combine1']")
	public static WebElement combine1_ResultPage;
	Thinking thinking=new Thinking();
	public combineJobPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void clickcombineJob() throws IOException, InterruptedException {
		waitForElementToDisplay(editViewPage.result_page);
		waitForElementToClickable(combineJobs_Tab);
		clickElementUsingJavaScript(driver,combineJobs_Tab);
		thinking.waitForInvisibilityOfSpinner();
	}
	
	public void combinejob() throws IOException, InterruptedException {
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToClickable(selectJob_1);
		clickElement(selectJob_1);
		clickElement(selectJob_2);
		clickElement(selectJob_3);
		clickElement(combineButton);
		thinking.waitForInvisibilityOfSpinner();
	}
	
	public void jobDetail() throws IOException, InterruptedException {
		setInput(jobTitle_Tab , "combine1");
		setInput(jobCode_Tab , "123");
		clickElement(applyButton);
		thinking.waitForInvisibilityOfSpinner();
		clickElement(DoneButton);
		thinking.waitForInvisibilityOfSpinner();
	}
	
	
}
