package pages.WINPages;

import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;
import static driverfactory.Driver.waitForPageLoad;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import driverfactory.Driver;

import static driverfactory.Driver.delay;

public class Navigate_to_Home {
	
	@FindBy(xpath="//a[text()='Home']")
    public static WebElement home;
	
	 public Navigate_to_Home(){
			PageFactory.initElements(driver, this);
		}
	
	    public void homepage() throws InterruptedException
		{
	    	delay(3000);
			waitForElementToDisplay(home);
			clickElement(home);			    
			delay(3000);
			waitForPageLoad();
		}

}
