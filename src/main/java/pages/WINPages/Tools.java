package pages.WINPages;
import static driverfactory.Driver.driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

import utilities.InitTests;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.*;

public class Tools {
	@FindBy(xpath="//*[@class='m-icon-menu']")
	public static WebElement Tool_link;
	
	@FindBy(xpath="//*[@class='m-icon-envelope']")
	public static WebElement ContactMercer;
	
	@FindBy(xpath="//*[@class='m-icon-alert']")
	public static WebElement About;
	
	@FindBy(xpath="//*[@class='m-icon-gear-large']")
	public static WebElement Administration;
	
	@FindBy(xpath="//*[@class='m-icon-user']")
	public static WebElement My_Profile;
	
	@FindBy(xpath="//span[contains(text(),'About')]")
	public static WebElement About_Page;
	
	@FindBy(xpath="//button[@class='apply-button medium ok']")
	public static WebElement Close_About_Page;
	
	@FindBy(xpath="//*[contains(text(),'Mercer WIN - System Administration')]")
	public static WebElement Administration_Page;
	
	@FindBy(xpath="//*[@id=\"page-title\"]/div/h1")
	public static WebElement User_Profile_Page;

	@FindBy(xpath="//a[text()='Home']")
	public static WebElement Home_Page;	
	
	@FindBy(xpath="//a[text()='Administration']")
	public static WebElement Administration_Title;
	
	@FindBy(xpath="//a[text()='Manage Evaluation Approvals']")
	public static WebElement Manage_Evaluation_Approvals_Title;
	
	@FindBy(xpath="//a[text()='User Profile']")
	public static WebElement User_Profile_Title;

	public Tools() {
		PageFactory.initElements(driver, this);
	}

	public void ContactMercer() throws InterruptedException{
		waitForElementToDisplay(Tool_link);
		clickElement(Tool_link);
		waitForElementToDisplay(ContactMercer);
		clickElement(ContactMercer);	
	}

	public void About() throws InterruptedException{
		waitForElementToDisplay(Tool_link);
		clickElement(Tool_link);
		waitForElementToDisplay(About);
		clickElement(About);
		delay(5000);
	}
	
	public void Administration() throws InterruptedException{
		waitForElementToDisplay(Tool_link);
		clickElement(Tool_link);
		waitForElementToDisplay(Administration);
		clickElement(Administration);
		delay(5000);	
	}
	
	public void My_Profile() throws InterruptedException{
		waitForElementToDisplay(Tool_link);
		clickElement(Tool_link);
		waitForElementToDisplay(My_Profile);
		clickElement(My_Profile);
		delay(5000);
	}
	
	public void About_Page() throws InterruptedException{
		waitForElementToDisplay(Close_About_Page);
		clickElement(Close_About_Page);
	}	
		
	public void Home() throws InterruptedException{
			waitForElementToDisplay(Home_Page);
			clickElement(Home_Page);
	}
}
