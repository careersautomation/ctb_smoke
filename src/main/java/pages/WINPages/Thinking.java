package pages.WINPages;

import static driverfactory.Driver.driver;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Thinking {

	@FindBy(xpath="//div[@class='big-win-common-thinking']")
    public static By thinking;
	
	public static WebDriverWait wait;
	
	public Thinking(){
		PageFactory.initElements(driver, this);
	}
	
	public void waitForInvisibilityOfSpinner() throws IOException, InterruptedException {
		
		if (driver.findElements(By.xpath("//div[@class='thinking']")).size()!=0) 
		{
		wait = new WebDriverWait(driver,150);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='thinking']")));
		}
	}
	
	public void waitForInvisibilityOfThinkingSpinner() throws IOException, InterruptedException {
		
		if (driver.findElements(By.xpath("//div[@class='bg-win-common-thinking']")).size()!=0) 
		{
		wait = new WebDriverWait(driver,100);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='bg-win-common-thinking']")));
		}
	}
public void waitForInvisibilityOfThinkingSpinner(int time) throws IOException, InterruptedException {
		
		if (driver.findElements(By.xpath("//div[@class='bg-win-common-thinking']")).size()!=0) 
		{
		wait = new WebDriverWait(driver,time);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='bg-win-common-thinking']")));
		}
	}
	public void thinking()
	{
		if(((WebElement) thinking)!=null)
		{
		new WebDriverWait(driver,10).until(ExpectedConditions.invisibilityOfElementLocated(thinking));
		}
		else
		{
		System.out.println("Next Step");
		}
	}
}
