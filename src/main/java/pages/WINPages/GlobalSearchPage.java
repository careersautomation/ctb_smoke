package pages.WINPages;

import pages.WINPages.Thinking;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.*;

import java.io.IOException;

public class GlobalSearchPage {
	
	@FindBy(xpath="//button[@class='medium open-popup open-option-button']")
	public static WebElement mv_Tab;
	
	@FindBy(xpath="//div[@class='sub-level-group selected']//div[@class='sub-level-items']//following::input[1]")
	public static WebElement select_mv;
	
	@FindBy(xpath="//div[@class='option-popup GeographySurvey popup container']//div[@class='controls']//button[@class='apply-button action-button medium ok']")
	public static WebElement apply_Button;
	
	@FindBy(xpath="//input[@name='Keyword']")
	public static WebElement KeywordSearch;
	
	@FindBy(xpath="//button[@class='medium ok icon submit']")
	public static WebElement search_button;
	
	@FindBy(xpath="//td[@class='selector']//input[@type='checkbox']")
	public static WebElement select_allJob;
	
	@FindBy(xpath="//button[@class='large ok next continue-button']")
	public static WebElement continue_button;
	
	@FindBy(xpath="//h1[@class='results-page']")
	public static WebElement MMDResultPage;
	
	@FindBy(xpath="(//div[@class='data-grid']//div[@class='body']//tbody//tr[@class='odd'])[1]")
	public static WebElement result;
	
	@FindBy(css="button[title='Table']")
	public static WebElement table;
	
	Thinking thinking=new Thinking();
	
	public GlobalSearchPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void GlobalJobView() throws IOException, InterruptedException {
		
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToDisplay(mv_Tab);
		clickElementUsingJavaScript(driver,mv_Tab);
		System.out.println("Geography/market view selected");
		thinking.waitForInvisibilityOfThinkingSpinner();
		clickElement(select_mv);
		clickElementUsingJavaScript(driver,apply_Button);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		setInput(KeywordSearch , "account");
		clickElement(search_button);
		clickElement(search_button);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToClickable(select_allJob);
		clickElementUsingJavaScript(driver,select_allJob);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToClickable(continue_button);
		clickElement(continue_button);	
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		if(driver.findElements(By.cssSelector("button[title='Table']")).size()!=0)
		{
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
			delay(10000);
			clickElement(table);
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
		}
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
	}	
		
}
