package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class My_Employees {
	
	@FindBy(id="feature-menu-button3")
	public static WebElement myempl;
	
	@FindBy(xpath="//div[@id='breadcrumb']//a[text()='My Employees']")
	public static WebElement Myemployees;

	Thinking thinking=new Thinking();
	
	public My_Employees(){
		PageFactory.initElements(driver, this);
	}
	public void Myemployees() throws IOException, InterruptedException
	{
		waitForPageLoad();
		waitForElementToDisplay(myempl);
		clickElement(myempl);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForPageLoad();
	}
	
	public  void Myemployees1() throws InterruptedException, IOException
	{
		waitForPageLoad();
		waitForElementToDisplay(myempl);
		clickElement(myempl);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForPageLoad();
	}
}
