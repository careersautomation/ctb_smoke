package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import java.io.IOException;
import java.util.Random;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Job_Architecture_Family_Tab {
	
	@FindBy(xpath="//button[@class='medium action job-arch-button']")
	public WebElement jobarchitecture;
	
	@FindBy(xpath="//div[@class='column page-column-header customer']//button[@class='btn btn-new-item']")
	public WebElement newbutton;
	
	@FindBy(xpath="//div[@class='column page-column-header mercer']//button[@class='btn btn-copy-all-items']")
	public WebElement copyall;
	
	@FindBy(xpath="//select[@data-purpose='sort']")
	public WebElement sort;
	
	@FindBy(xpath="//div[@class='row tree-row show-row  level1 customer-is-not-present mercer-is-present collapsed unmodified']//div[@class='column tree-node mercer']//button[@class='btn btn-show-detail']")
	public WebElement details;
	
	@FindBy(xpath="//button[@class='btn btn-close-detail']")
	public WebElement detailsclose;
	
	@FindBy(xpath="//button[text()='Family']")
	public WebElement FamilyTab;
	
	@FindBy(xpath="//a[text()='Retail' and @data-source='Mjl']//parent::div/button")
	public WebElement familyexpand;
	
	@FindBy(xpath="//a[text()='Retail Store Management' and @data-source='Mjl']//parent::div/button")
	public WebElement subfamilyexpand;
	
	@FindBy(xpath="//a[text()='Retail' and @data-source]//parent::div/span/button[@title='Copy All']")
	public WebElement copyallFamily;
	
	@FindBy(xpath="//button[@class='btn btn-save']")
	public WebElement save;
	
	@FindBy(xpath="//select[@data-purpose='sort']//option[1]")
	public WebElement sortorder1;
	
	@FindBy(xpath="//select[@data-purpose='sort']//option[2]")
	public WebElement sortorder2;
	
	@FindBy(xpath="//select[@data-purpose='sort']//option[3]")
	public WebElement sortorder3;
	
	@FindBy(xpath="//select[@data-purpose='sort']//option[4]")
	public WebElement sortorder4;
	
	@FindBy(xpath="//select[@data-purpose='select']")
	public WebElement select;
	
	@FindBy(xpath="//select[@data-purpose='select']//option[1]")
	public WebElement selectorder1;
	
	@FindBy(xpath="//select[@data-purpose='select']//option[2]")
	public WebElement selectorder2;
	
	@FindBy(xpath="//button[@class='action close-button medium ok']//following::b")
	public WebElement addempclose;
	
	@FindBy(xpath="//div[@class='popup bench-employee-details-popup full-page has-title size-to-page size-specific']//div[@data-form-item-property='Name']//div[@class='form-value read-only span3']")
	public WebElement employeename;
	
	@FindBy(xpath="//div[@id='breadcrumb']//a[contains(text()[normalize-space()],'Administration')]")
	public static WebElement Administration;
	
	Thinking thinking =new Thinking();
	public Job_Architecture_Family_Tab() {
		PageFactory.initElements(driver, this);
	}
	
	public void JobArchitecturefamily() throws InterruptedException, IOException
	{
		waitForElementToDisplay(Administration);
		clickElement(Administration);
		waitForPageLoad();
		clickElement(jobarchitecture);
		waitForPageLoad();
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(sort);
	}
	public void sortverify() throws IOException, InterruptedException {
		waitForElementToDisplay(sort);
		clickElement(sort);
		thinking.waitForInvisibilityOfSpinner();
	}
	public void selectverify() throws IOException, InterruptedException {
		waitForElementToDisplay(sortorder2);
		clickElement(sortorder2);
		waitForElementToDisplay(sort);
		clickElement(sort);
		waitForElementToDisplay(sortorder1);
		clickElement(sortorder1);
		waitForElementToDisplay(select);
		clickElement(select);
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
	}
	public void expand() throws IOException, InterruptedException {
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(selectorder2);
		clickElement(selectorder2);
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(select);
        clickElement(select);
        thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
        waitForElementToDisplay(selectorder1);
		clickElement(selectorder1);
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(details);
		clickElement(details);
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(detailsclose);
		clickElement(detailsclose);
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(familyexpand);
		clickElement(familyexpand);
		waitForElementToDisplay(subfamilyexpand);
		clickElement(subfamilyexpand);
		waitForElementToDisplay(copyallFamily);
		clickElement(copyallFamily);
		waitForElementToDisplay(save);
		clickElement(save);
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
	}

}
