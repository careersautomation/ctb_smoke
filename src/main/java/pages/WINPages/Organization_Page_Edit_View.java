package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;
import static driverfactory.Driver.waitForPageLoad;

import java.io.IOException;
import java.util.Random;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Organization_Page_Edit_View {
	
	@FindBy(xpath="//b[text()='Edit']//ancestor::button")
	public WebElement editbutton;
	
	@FindBy(xpath="//a[@title='General Information']")
	public WebElement GeneralInformation;
    
	@FindBy(xpath="//a[@title='General Information']//following::input[4]")
	public WebElement column1;
	
	@FindBy(xpath="//a[@title='General Information']//following::input[5]")
	public WebElement column2;
	
	@FindBy(xpath="//b[text()='Done']//ancestor::button")
	public WebElement Done;
	
	@FindBy(xpath="//b[text()='Save As']//ancestor::button")
	public WebElement saveas;
	
	@FindBy(xpath="//div[@class='group edit-box edit-box group view-name edit-view-save-as-control-textbox']//following::input")
	public WebElement editviewname;

	@FindBy(xpath="//button[@class='action medium ok']")
	public WebElement save;
	
	@FindBy(xpath="//b[text()='Continue']")
	public WebElement Continue;
	
	@FindBy(xpath="//div[@class='edit-view selections column']//h2[@class='title']")
	public WebElement editselections;
	
	@FindBy(xpath="//a[text()='Edit View']")
	public WebElement editviewpage;
	
	@FindBy(xpath="//a[@data-record='0' and @data-column='0']")
	public static WebElement Organizationdetails;
	
	Random rg = new Random();	
	int randomInt = rg.nextInt(100000);	
	String numberAsString = Integer.toString(randomInt);	
	public String viewname="View"+numberAsString;
	
	Thinking thinking=new Thinking();
	public Organization_Page_Edit_View(){
		PageFactory.initElements(driver, this);
	}
	
	public void editviewbutton() throws IOException, InterruptedException
	{	
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToEnable(editbutton);
		clickElement(editbutton);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForPageLoad();
	}
	public void editviewselections() throws IOException, InterruptedException {
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		clickElement(GeneralInformation);			   	
		clickElement(column1);
		clickElement(column2);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		
	}
	public void edit_view_save() throws IOException, InterruptedException
	{
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToDisplay(saveas);
		clickElement(saveas);
		waitForElementToDisplay(editviewname);
		setInput(editviewname,viewname);
		waitForElementToDisplay(save);
		clickElement(save);
		waitForElementToDisplay(Done);
		clickElement(Done);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForPageLoad();
		waitForElementToEnable(Organizationdetails);
		//delay(6000);
	}

}
