package pages.WINPages;

import static driverfactory.Driver.*;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class combinePCpage {

	@FindBy(xpath="//button[@data-stat-type='Actual']")
	public static WebElement Actual_Tab;
	
	@FindBy(xpath="//button[@data-purpose='combine-pcs']")
	public static WebElement combinePC_Tab;
	
	@FindBy(xpath="//div[@data-pc='41']//input[@class='combine-pc-toggle']")
	public static WebElement selectPC_41;
	
	@FindBy(xpath="//div[@data-pc='44']//input[@class='combine-pc-toggle']")
	public static WebElement selectPC_44;
	
	@FindBy(xpath="//div[@data-pc='59']//input[@class='combine-pc-toggle']")
	public static WebElement selectPC_59;
	
	@FindBy(xpath="//div[@data-pc='61']//input[@class='combine-pc-toggle']")
	public static WebElement selectPC_61;
	
	@FindBy(xpath="//button[@data-purpose='combine']")
	public static WebElement combine_button;
	
	@FindBy(xpath="//button[@data-purpose='done-page']")
	public static WebElement save_Button;
	
	@FindBy(xpath="//div[@class='summary data-grid display-below portal']//span[@title='41 - 44 (combined)']")
	public  WebElement combine1_result;

	@FindBy(xpath="//div[@class='summary data-grid display-below portal']//span[@title='59 - 61 (combined)']")
	public  WebElement combine2_result;
	
	@FindBy(xpath="//div[@class='split-grid']//div[@class='summary data-grid display-below portal']")
	public static WebElement result_table;
	
	Thinking thinking=new Thinking();
	
	public combinePCpage() {
		PageFactory.initElements(driver, this);
	}
	
	public void pcCombine() throws IOException, InterruptedException {
		waitForPageLoad();
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToClickable(Actual_Tab);
		clickElementUsingJavaScript(driver,Actual_Tab);
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToClickable(combinePC_Tab);
		clickElementUsingJavaScript(driver,combinePC_Tab);
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
		clickElementUsingJavaScript(driver,selectPC_41);
		clickElementUsingJavaScript(driver,selectPC_44);
		clickElementUsingJavaScript(driver,combine_button);
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToClickable(selectPC_59);
		clickElementUsingJavaScript(driver,selectPC_59);
		clickElementUsingJavaScript(driver,selectPC_61);
		clickElementUsingJavaScript(driver,combine_button);
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
		clickElementUsingJavaScript(driver,save_Button);
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
		
	}
}
