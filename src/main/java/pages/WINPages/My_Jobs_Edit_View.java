package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;

import java.io.IOException;
import java.util.Random;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class My_Jobs_Edit_View {
	
	@FindBy(xpath="//b[text()='Edit']//ancestor::button")
	public WebElement editbutton;
	
	@FindBy(xpath="//a[@title='Market Variances']")
	public WebElement Marketvariances;
    
	@FindBy(xpath="//a[@title='Market Variances']//following::input[1]")
	public WebElement column1;
	
	@FindBy(xpath="//a[@title='Market Variances']//following::input[2]")
	public WebElement column2;
	
	@FindBy(xpath="//button[@class='large ok next done-button']")
	public WebElement Done;
	
	@FindBy(xpath="//button[@class='large save-as-button']")
	public WebElement saveas;
	
	@FindBy(xpath="//div[@class='group edit-box edit-box group view-name edit-view-save-as-control-textbox']//following::input")
	public WebElement editviewname;

	@FindBy(xpath="//button[@class='action medium ok']")
	public WebElement save;
	
	@FindBy(xpath="//b[text()='Continue']")
	public WebElement Continue;
	
	@FindBy(xpath="//div[@class='edit-view selections column']//h2[@class='title']")
	public WebElement editselections;
	
	@FindBy(xpath="//a[text()='Edit View']")
	public WebElement editviewpage;
	
	@FindBy(xpath="//b[text()='Manage Jobs']//ancestor::button")
	public WebElement managejobs;
	
	 @FindBy(xpath="//a[@data-record='0']")
	    public static WebElement jobdetails;
	
	My_jobs job=new My_jobs();
	Thinking thinking= new Thinking();
	Random rg = new Random();	
	int randomInt = rg.nextInt(100000);	
	String numberAsString = Integer.toString(randomInt);	
	public String viewname="View"+numberAsString;
	
	public My_Jobs_Edit_View(){
		PageFactory.initElements(driver, this);
	}
	
	public void editviewbutton() throws IOException, InterruptedException
	{	
		waitForElementToEnable(editbutton);
		clickElement(editbutton);
		waitForPageLoad();
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
	}
	public void editviewselections() {
		clickElement(Marketvariances);			   	
		clickElement(column1);
		clickElement(column2);
		
	}
	public void edit_view_save() throws IOException, InterruptedException
	{
		waitForElementToDisplay(saveas);
		clickElement(saveas);
		waitForElementToDisplay(editviewname);
		setInput(editviewname,viewname);
		waitForElementToDisplay(save);
		clickElement(save);
		waitForElementToDisplay(Done);
		clickElement(Done);
		waitForPageLoad();
		delay(2000);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		//waitForElementToEnable(jobdetails);
		waitForElementToEnable(managejobs);
		//delay(6000);
	}

}
