package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;
import static driverfactory.Driver.waitForPageLoad;

import java.io.IOException;

import static driverfactory.Driver.delay;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.InitTests;

public class Manage_IPE_Settings {
	
	@FindBy(xpath="//button[@class='medium action ipe-button']")
	public static WebElement IPEButton;

	@FindBy(xpath="//b[text()='Configurations']")
	public static WebElement Configtab;		
	
	@FindBy(xpath="//button[@class='action close-button medium ok']")
	public static WebElement Close;
	
	Thinking thinking=new Thinking();
	public Manage_IPE_Settings() {
		PageFactory.initElements(driver, this);
	}
	public void ipe() throws InterruptedException, IOException
	{	thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToDisplay(IPEButton);
		clickElement(IPEButton);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToEnable(Configtab);
	}
	public void ipepopup() throws IOException, InterruptedException {
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		clickElement(Configtab);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		waitForElementToDisplay(Close);
		thinking.waitForInvisibilityOfThinkingSpinner();
		thinking.waitForInvisibilityOfThinkingSpinner();
		clickElement(Close);
	}

}
