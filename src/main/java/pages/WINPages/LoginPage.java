package pages.WINPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;

public class LoginPage {

	@FindBy(id="ctl00_MainSectionContent_Email")
	public static WebElement Email;
 
	@FindBy(id="ctl00_MainSectionContent_Password")
	public static WebElement passwordInput;
	
	@FindBy(id="ctl00_MainSectionContent_ButtonSignin")
	public static WebElement signin_Button;
	
	@FindBy(id="ContentPlaceHolder1_TextBox1")
	public WebElement emailmsso;
	
	WebDriver driver;
	
	public LoginPage() {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void login(String username,String password)
	{
		if(driver.findElements(By.id("ctl00_MainSectionContent_Email")).size()!=0)
		{
		setInput(Email,username);
		setInput(passwordInput,password);
		waitForElementToDisplay(signin_Button);
		clickElement(signin_Button);
		waitForPageLoad();
		}
		else
		{
			setInput(emailmsso,"@mercer.com");
			clickElement(driver.findElement(By.id("ContentPlaceHolder1_PassiveSignInButton")));
			delay(5000);
		}
	}	
}

