package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForPageLoad;

import java.io.IOException;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class My_jobs_search_page {
	
	 @FindBy(id="feature-menu-button2")
	 public static WebElement Myjobs;
	 
	 @FindBy(xpath="//a[text()='My Jobs']")
	 public static WebElement Myjobssearchpage;
	 
	 public My_jobs_search_page(){
			PageFactory.initElements(driver, this);
		}
	 Thinking thinking =new Thinking();
	 public void jobssearchpage() throws InterruptedException, IOException
		{
		 	thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
			waitForElementToDisplay(Myjobs);
			clickElement(Myjobs);
			waitForPageLoad();
			thinking.waitForInvisibilityOfThinkingSpinner();
			thinking.waitForInvisibilityOfThinkingSpinner();
			
		}

}
