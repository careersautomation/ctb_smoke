package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Job_Architecture_Career_Stream_Tab {
	
	@FindBy(xpath="//button[@data-action='goToCareer']")
	public WebElement Careerstreamtab;
	
	@FindBy(xpath="//div[@class='column page-column-header customer']//button[@class='btn btn-new-item']")
	public WebElement newbutton;
	
	@FindBy(xpath="//div[@class='column page-column-header mercer']//button[@class='btn btn-copy-all-items']")
	public WebElement copyall;
	
	@FindBy(xpath="//select[@data-purpose='sort']")
	public WebElement sort;
	
	@FindBy(xpath="//button[text()='Career Stream']")
	public WebElement CareerStreamtab;
	
	@FindBy(xpath="//a[text()='EXECUTIVE']//parent::div/button")
	public WebElement expand;
	
	@FindBy(xpath="//a[text()='EXECUTIVE']//parent::div/span/button[@data-action='copy-item']")
	public WebElement copyallFamily;
	
	@FindBy(xpath="//a[text()='EXECUTIVE']//parent::div//button[@title='Details']")
	public WebElement details;
	
	@FindBy(xpath="//button[@class='btn btn-close-detail']")
	public WebElement detailsclose;
	
	@FindBy(xpath="//button[@class='btn btn-save']")
	public WebElement save;
	
	@FindBy(xpath="//button[text()='Reset']")
	public WebElement reset;
	
	@FindBy(xpath="//select[@data-purpose='sort']//option[1]")
	public WebElement sortorder1;
	
	@FindBy(xpath="//select[@data-purpose='sort']//option[2]")
	public WebElement sortorder2;
	
	@FindBy(xpath="//select[@data-purpose='sort']//option[3]")
	public WebElement sortorder3;
	
	@FindBy(xpath="//select[@data-purpose='sort']//option[4]")
	public WebElement sortorder4;
	
	@FindBy(xpath="//select[@data-purpose='select']")
	public WebElement select;
	
	@FindBy(xpath="//select[@data-purpose='select']//option[1]")
	public WebElement selectorder1;
	
	@FindBy(xpath="//select[@data-purpose='select']//option[2]")
	public WebElement selectorder2;
	
	@FindBy(xpath="//button[@class='action close-button medium ok']//following::b")
	public WebElement addempclose;
	
	@FindBy(xpath="//div[@class='popup bench-employee-details-popup full-page has-title size-to-page size-specific']//div[@data-form-item-property='Name']//div[@class='form-value read-only span3']")
	public WebElement employeename;
	
	Thinking thinking =new Thinking();
	public Job_Architecture_Career_Stream_Tab() {
		PageFactory.initElements(driver, this);
	}
	
	public void JobArchitecturecareer() throws InterruptedException, IOException
	{
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
		clickElement(Careerstreamtab);
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
	}
	
	public void sortverify() throws IOException, InterruptedException {
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
		waitForPageLoad();
		clickElement(sort);
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
	}
	public void selectverify() throws IOException, InterruptedException {
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(sortorder2);
		clickElement(sortorder2);
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(sort);
		clickElement(sort);
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(sortorder1);
		clickElement(sortorder1);
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(select);
		clickElement(select);
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
		
	}
	public void expand() throws IOException, InterruptedException {
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(selectorder2);
		thinking.waitForInvisibilityOfSpinner();
		clickElement(selectorder2);
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(select);
        clickElement(select);
        thinking.waitForInvisibilityOfSpinner();
        waitForElementToDisplay(selectorder1);
		clickElement(selectorder1);
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(details);
		clickElement(details);
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(detailsclose);
		clickElement(detailsclose);
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(expand);
		clickElement(expand);
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(copyallFamily);
		clickElement(copyallFamily);
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(reset);
		clickElement(reset);
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
	}


}
