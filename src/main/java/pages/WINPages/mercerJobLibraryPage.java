package pages.WINPages;

import static driverfactory.Driver.*;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class mercerJobLibraryPage {
	
	@FindBy(xpath="//button[@class='dropdown-target select-view-button']")
	public static WebElement view_Dropdown;
	
	@FindBy(xpath="//button[@dataview-id='0c242c38-28d5-446c-9b51-5726e8731d4a']")
	public static WebElement select_JobView;
	
	@FindBy(xpath="//button[@dataview-id='8c20a9e8-9020-406e-a2ef-360893e8b017']")
	public static WebElement select_PCView;
	
	@FindBy(xpath="//button[@data-id='mv' and @class='search-filter-button ']")
	public  static WebElement marketViewFilter;
	
	@FindBy(xpath="//li[@data-id='EU']")
	public static WebElement click_EMEA;
	
	@FindBy(id="EU.BE.15130219-ebc9-e511-86d5-002481adb66c")
	public static WebElement select_parentMv;
	
	@FindBy(id="EU.BE.b58a0282-ecc9-e511-86d5-002481adb66c")
	public static WebElement select_childMv;
	
	@FindBy(id="EU.BE.d44cec69-07cc-e511-86d5-002481adb66c")
	public static WebElement select_mvMJC1 ;
	
	@FindBy(id="EU.DK.f559409a-00cc-e511-86d5-002481adb66c")
	public static WebElement select_mvDETRS;
	
	@FindBy(xpath="//div[@data-purpose='action-buttons']//button[text()='Apply']")
	public static WebElement mvApply_Button;
	
	@FindBy(xpath="//button[@data-purpose='search-submit']")
	public static WebElement search_Button;
	
	@FindBy(xpath="//div[@class='data-grid display-below portal']//input[@data-record='-1']")
	public static WebElement select_allJob;
	
	@FindBy(xpath="//button[@data-id='searchContinueButton']")
	public static WebElement continue_Button;
	
	@FindBy(xpath="//span[@class='current-view result']")
	public static WebElement currentViewResult;
	
	@FindBy(xpath="//div[@class='report-model-title']")
	public static WebElement result_Page;
	
	@FindBy(xpath="//button[@data-purpose='show-option-detail' and @data-id='f']")
	public static WebElement familyFlyout;
	
	@FindBy(xpath="//button[@data-purpose='show-option-detail' and @data-id='ji']")
	public static WebElement jobindustry;
	
	@FindBy(xpath="//button[@data-purpose='show-option-detail' and @data-id='cl']")
	public static WebElement careerlevel;

	@FindBy(xpath="//li[@data-id='RET']//input")
	public static WebElement select_HumanResources;
	
	@FindBy(xpath="//label[text()=' Cross Industry']//parent::li/input")
	public static WebElement crossindustry;
	
	@FindBy(xpath="//label[text()='All EXECUTIVE Career Levels']//parent::li/input")
	public static WebElement checkallexecutive;
	
	@FindBy(xpath="(//div[@class='data-grid display-below portal']//div[@class='body']//tbody//tr[@class='odd'])[1]")
	public static WebElement mjl_tableresult;
	
	@FindBy(xpath="//div[@class='data-grid mjl-results-grid display-below portal']//div[@class='table-portal' and @style]")
	public  WebElement admin_tableresult;
	
	@FindBy(xpath="//a[text()='EMEA']")
	public  WebElement selectemea;
	
	@FindBy(xpath="//label[contains(text(),'All EMEA Countries/Regions')]//ancestor::li/input")
	public  WebElement selectallemea;
	
	
	Thinking thinking=new Thinking();
	public mercerJobLibraryPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public void jobView() throws InterruptedException, IOException {
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToClickable(view_Dropdown);
		clickElement(view_Dropdown);
		thinking.waitForInvisibilityOfSpinner();
		scrollToElement(driver,select_JobView);
		clickElement(select_JobView);
		thinking.waitForInvisibilityOfSpinner();
		mvFilter(select_parentMv , select_childMv);
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToClickable(mjl_tableresult);
		waitForElementToClickable(select_allJob);
		clickElementUsingJavaScript(driver,select_allJob);
		thinking.waitForInvisibilityOfSpinner();
		System.out.println("Jobs selected");
		waitForElementToClickable(continue_Button);
		clickElement(continue_Button);		
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
	}	
		
	public void mvFilter (WebElement ele1 , WebElement ele2 ) throws IOException, InterruptedException {
		waitForElementToClickable(marketViewFilter);
		clickElementUsingJavaScript(driver,marketViewFilter);
		System.out.println("market view selected");
		clickElement(click_EMEA);
		clickElement(ele1);
		clickElement(ele2);
		thinking.waitForInvisibilityOfSpinner();
		clickElement(mvApply_Button);
		thinking.waitForInvisibilityOfSpinner();
		clickElement(search_Button);
		thinking.waitForInvisibilityOfSpinner();
	}
		
	
	public void navigateJobView() throws InterruptedException, IOException {
		waitForPageLoad();
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToClickable(view_Dropdown);
		clickElement(view_Dropdown);
		thinking.waitForInvisibilityOfSpinner();
		scrollToElement(driver,select_JobView);
		clickElement(select_JobView);
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToClickable(search_Button);
		clickElementUsingJavaScript(driver,search_Button);
		//Thinking.loading()
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(mjl_tableresult);
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToClickable(select_allJob);
		clickElementUsingJavaScript(driver,select_allJob);
		waitForElementToClickable(continue_Button);
		clickElement(continue_Button);
		thinking.waitForInvisibilityOfSpinner();
	}
	
	public void pcView() throws IOException, InterruptedException {
		System.out.println("mercer Job Library-PC View");
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToClickable(view_Dropdown);
		clickElement(view_Dropdown);
		thinking.waitForInvisibilityOfSpinner();
		clickElement(select_PCView);
		/*waitForElementToClickable(marketViewFilter);
		clickElementUsingJavaScript(driver,marketViewFilter);
		System.out.println("market view selected");
		thinking.waitForInvisibilityOfSpinner();
		clickElement(click_EMEA);
		clickElement(select_mvDETRS);
		clickElement(ele2);
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
		clickElement(mvApply_Button);*/
		thinking.waitForInvisibilityOfSpinner();
		clickElementUsingJavaScript(driver,marketViewFilter);
		thinking.waitForInvisibilityOfSpinner();
		clickElement(selectemea);
		clickElement(selectallemea);
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
		waitForElementToDisplay(driver.findElement(By.xpath("//button[text()='Apply']")));
		if(driver.findElement(By.xpath("//button[text()='Apply']")).isDisplayed())
		{
			waitForElementToDisplay(driver.findElement(By.xpath("//button[text()='Apply']")));
			clickElement(driver.findElement(By.xpath("//button[text()='Apply']")));
		}
		thinking.waitForInvisibilityOfSpinner();
		thinking.waitForInvisibilityOfSpinner();
		clickElement(search_Button);
		thinking.waitForInvisibilityOfSpinner();
		//mvFilter( select_mvDETRS);
		waitForElementToClickable(continue_Button);
		clickElementUsingJavaScript(driver,continue_Button);
		thinking.waitForInvisibilityOfSpinner();
	}
	
	
	public void admin_mjl() throws IOException, InterruptedException {
		thinking.waitForInvisibilityOfSpinner();
		
		/*waitForElementToClickable(jobindustry);
		clickElementUsingJavaScript(driver,jobindustry);
		waitForElementToClickable(crossindustry);
		clickElementUsingJavaScript(driver,crossindustry);
		clickElement(mvApply_Button);*/
		
		waitForElementToClickable(familyFlyout);
		clickElementUsingJavaScript(driver,familyFlyout);
		waitForElementToClickable(select_HumanResources);
		clickElementUsingJavaScript(driver,select_HumanResources);
		clickElement(mvApply_Button);
		
		
		waitForElementToClickable(careerlevel);
		clickElementUsingJavaScript(driver,careerlevel);
		waitForElementToClickable(checkallexecutive);
		clickElementUsingJavaScript(driver,checkallexecutive);
		clickElement(mvApply_Button);
		
		
		thinking.waitForInvisibilityOfSpinner();
		clickElement(search_Button);
		thinking.waitForInvisibilityOfSpinner();
		if(driver.findElements(By.xpath("//input[@data-record='-1' and @disabled='disabled']")).size()!=0)
		{
				System.out.print("");
		}
		else
		{
				driver.findElement(By.xpath("//input[@data-record='-1']")).click();
		}
		waitForElementToClickable(continue_Button);
		clickElement(continue_Button);
		thinking.waitForInvisibilityOfSpinner();
	}
}
