package pages.pit;

import java.util.Set;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import driverfactory.Driver;

public class PIT_InputPage extends Driver
{
	public PIT_InputPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//label[contains(text(),'Optional Subtitle for Results Page')]")
	public static WebElement optionalTitleLabel;
	
	@FindBy(id="AnnualSalary")
	public static WebElement salaryInput;
	
	@FindBy(id="inputId_BonusPayment")
	public static WebElement bonusInput;
	
	@FindBy(xpath="//*[@id='Clean_DivFirst']/div[2]/div[1]/div[1]/select")
	public static WebElement bonusTypeDropdown;
	
	@FindBy(id="btnSubmit")
	public static WebElement viewResultsBtn;
	
	@FindBy(xpath="//*[@id='Clean_DivFirst']/div[7]/div[1]/select")
	public static WebElement marritalStatusDropdown;
	
	@FindBy(id="ddlGivenSalaryIs")
	public static WebElement givenSalaryTypeDropdown;
	
	
	//For GROSS TO NET 
	
	@FindBy(xpath="//*[@id='Clean_DivSecond']/div[1]/div[2]/div[5]/select")
	public static WebElement TaxableAllowanceTypeDropdown;
	
	@FindBy(id="inputId_TAFirst")
	public static WebElement TaxableAllowanceValueInput;
	
	@FindBy(xpath="//*[@id='Clean_DivSecond']/div[1]/div[8]/div[3]/select")
	public static WebElement TDCTypeDropdown;
	
	@FindBy(id="inputId_TDCFirst")
	public static WebElement TDCValueInput;
	
	//Additional Compensation to include in Gross Up section--NET TO GROSS
	@FindBy(id="IsIncludedRow1")
	public static WebElement additionalComp1CB;
	
	@FindBy(id="NameRow1")
	public static WebElement AdditionalCompLabel1INPUT;
	
	@FindBy(id="AmtValueRow1")
	public static WebElement AdditionalCompValue1INPUT;
	
	
	public PIT_ResultsPage accessResultPage() {
		try {
		clickElement(viewResultsBtn);
		
		delay(3000);
		//PIT results page will be opened in next tab
		String parentWindow =driver.getWindowHandle();
		  Set<String> allWindows =driver.getWindowHandles();
		  for(String pitResultsWindow:allWindows){
			  
			  if(!parentWindow.equals(pitResultsWindow))
				  driver.switchTo().window(pitResultsWindow);
			  
			  }
		  delay(4000);
		  waitForPageLoad();
	} catch (Exception e) {
		e.printStackTrace();
	}
		
		return new PIT_ResultsPage();
	}
	
	public void setInputPageValues(String salary){
		
	}
	
	
}
