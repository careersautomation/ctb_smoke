package pages.pit;

import java.io.File;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import driverfactory.Driver;

public class PIT_ResultsPage extends Driver
{
	
	public PIT_ResultsPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//div[@class='col-xs-12 guts']//h2")
	public static WebElement headerLabel;
	
	@FindBy(xpath="//*[contains(text(),'Net Income')]/parent::*//td[2]")
	public static WebElement netIncome;
	
	@FindBy(xpath= "//*[text()='Total Personal Income Tax']//parent::tr//td[2]")
	public static WebElement totalIncomeTax;
	
	@FindBy(xpath= "//*[contains(text(),'Family Allowance')]/parent::*//td[@colspan=2]")
	public static WebElement familyAllowance;
	
	@FindBy(xpath= "//*[contains(text(),'Social Security Contributions')]/parent::*//td[@colspan=2]")
	public static WebElement socialSecurity;
	
	@FindBy(id="ExportPDF")
	public static WebElement exportPDFRadioBtn;
	
	@FindBy(id="ExportExcel")
	public static WebElement exportEXCELRadioBtn;
	
	@FindBy(id="Export")
	public static WebElement exportBtn;
	
	// for net to gross
	@FindBy(xpath="//*[text()='Gross Income']//parent::tr//td[2]")
	public static WebElement grossIncome;
	

	public String getValue(String label) {
		String actualValue=null;
		switch(label) {
		case "NetIncome":
			waitForElementToDisplay(netIncome);
			actualValue=netIncome.getText().toString().trim().replaceAll(",", "");
		    break;
		case "GrossIncome":
			delay(3000);
			waitForElementToDisplay(grossIncome);
			actualValue=grossIncome.getText().toString().trim().replaceAll(",", "");
		    break;   
		case "TotalPersonalIncomeTax":
			waitForElementToDisplay(totalIncomeTax);
			actualValue=totalIncomeTax.getText().toString().trim().replaceAll(",", "");
		    break; 
		case "SocialSecurity":
			waitForElementToDisplay(socialSecurity);
			actualValue=socialSecurity.getText().toString().trim().replaceAll(",", "");
		    break; 
		case "FamilyAllowance":
			waitForElementToDisplay(familyAllowance);
			actualValue=familyAllowance.getText().toString().trim().replaceAll(",", "");
		    break; 
		}
		
		
		return actualValue;
	}
	
	public static boolean isFileDownloaded(String downloadPath, String fileName) {
		  File dir = new File(downloadPath);
		  File[] dirContents = dir.listFiles();

		  for (int i = 0; i < dirContents.length; i++) {
			  
		      if (dirContents[i].getName().contains(fileName)) {
		          // File has been found, it can now be deleted:
		    	  System.out.println("File found "+dirContents[i].getName());
		    	System.out.println("Downloaded File name--- >> "+dirContents[i].getName());
		    	  dirContents[i].delete();
		          return true;
		      }
		          }
		      return false;
		  }	
}
