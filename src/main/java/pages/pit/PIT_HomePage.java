package pages.pit;

import java.util.Set;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import driverfactory.Driver;

public class PIT_HomePage extends Driver

{
	public PIT_HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath="//li[@id='emailId']//a")
	public static WebElement bannerEmail;
	
	@FindBy(xpath="//*[@id='emailId']/ul/li[1]/div[2]")
	public static WebElement clientName;
	
	@FindBy(xpath="(//ul[@class='dropdown-menu']//li/div[@class='col-sm-10'])[2]")
	public static WebElement AcountCode;
	
  //Personal Tax Calculator section
	@FindBy(id="TaxLocation")
	public static WebElement countryDropdown;
	
	@FindBy(id="submitbtnId")
	public static WebElement beginCaclulationBtn;
	
//Personal Tax Report section 
	@FindBy(id="TaxReportLocation")
	public static WebElement taxReportLocationDropdown;
	
	@FindBy(id="Year")
	public static WebElement taxYearDropdown;
	
	@FindBy(id="submitbtnReportId")
	public static WebElement runYourReportBtn;
	
	
	
	public PIT_InputPage accessInputPage(String countryName) {
		delay(8000);
		waitForPageLoad();
		waitForElementToDisplay(countryDropdown);
		selEleByVisbleText(countryDropdown, countryName);
		clickElement(beginCaclulationBtn);
		String parentWindow =driver.getWindowHandle();
		  Set<String> allWindows =driver.getWindowHandles();
		  for(String inputWindow:allWindows){
			  System.out.println("Printing window handle on HOME PAGE >>>>"+inputWindow);
			if(!parentWindow.equals(inputWindow))
				 driver.switchTo().window(inputWindow);
			
			  delay(5000);
			  waitForPageLoad();
			  }
		
		
		
		return new PIT_InputPage();
	}
	
	
	public static String getAccountCode(){
		delay(1000);
		bannerEmail.click();
		delay(1000);
		String account=AcountCode.getText();
		return account;
	
	}
}
