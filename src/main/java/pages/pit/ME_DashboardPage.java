package pages.pit;

import java.util.Set;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import driverfactory.Driver;
import verify.SoftAssertions;

public class ME_DashboardPage extends Driver
{

	public  ME_DashboardPage()
	 {
		PageFactory.initElements(driver, this);
	}

// OR for ME landing page 
	
	@FindBy(xpath="//*[@id='dnn_accountContextSelectionControl_pnlAccountSelect']/div[1]/div/a")
	public static WebElement SignedINAsAccountCode;
	
	@FindBy(xpath="//a[@class='sapphire ng-binding'][contains(text(),'Personal Income Tax')]")
	public static WebElement PITLink;
	
	
	
	
	public PIT_HomePage accessPIT(){
		try {
			waitForPageLoad();
			delay(6000);
			waitForElementToDisplay(PITLink);
		//	SoftAssertions.verifyElementTextContains(PITLink, "Personal Income Tax");
			clickElement(PITLink);
			delay(3000);
			waitForPageLoad();
			//PIT will be opened in next tab
			String parentWindow =driver.getWindowHandle();
			  Set<String> allWindows =driver.getWindowHandles();
			  for(String pitWindow:allWindows){
				  if(!parentWindow.equals(pitWindow))
					  driver.switchTo().window(pitWindow);
				  delay(5000);
				  }
			
			  waitForPageLoad();
			  
		} catch (Exception e) {
			e.printStackTrace();
		}
		  return new PIT_HomePage();
	}
	


}
