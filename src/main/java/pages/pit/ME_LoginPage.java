package pages.pit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import driverfactory.Driver;
import utilities.InitTests;

/**
 * @author Pavan
 *
 */
public class ME_LoginPage extends Driver
{
	public ME_LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//span[text()='My Account ']")
	public static WebElement myAccountLink;
	
	@FindBy(xpath="//span[text()='Login'][1]")
	public static WebElement loginBtn;

	@FindBy(id="ctl00_MainSectionContent_Email")
	public static WebElement emailIdInput;

	@FindBy(id="ctl00_MainSectionContent_Password")
	public static WebElement passwordInput;

	@FindBy(xpath="//*[@id='ctl00_MainSectionContent_ButtonSignin']")
	public static WebElement signInBtn;
	
	/**
	 * @MethodName: doLogin 
	 * @purpose: To login in to Mobility exchange. 
	 * @author: pavan
	 **/
	public ME_ClientAccountSelectionPage doLogin(){
		try {
			/*waitForElementToDisplay(myAccountLink);
			myAccountLink.click();*/
			clickElement(myAccountLink);
			waitForElementToDisplay(loginBtn);
			loginBtn.click();
			waitForElementToDisplay(emailIdInput);
			emailIdInput.clear();
			setInput(emailIdInput, InitTests.USERNAME);
			passwordInput.clear();
			setInput(passwordInput, InitTests.PASSWORD);
			clickElement(signInBtn);
			
		} catch (Exception e) {
			System.out.println("----- doLogin()"+e.getMessage());
		
		}		
		return new ME_ClientAccountSelectionPage();
	}
	
}

