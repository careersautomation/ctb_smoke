package pages.pit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import driverfactory.Driver;

public class ME_ClientAccountSelectionPage extends Driver
{
	
	public ME_ClientAccountSelectionPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//i[@class='logo sprite sprite-logo-nav']")
	public static  WebElement mercerLogo;
	
	@FindBy(xpath="//*[@class='btn btn-primary pull-right ng-scope']")
	public static  WebElement continueBtn;
	
	public ME_DashboardPage selectClientAccount(String clientName,String accountCode){
		//customized xpaths
		try {
			String clientNameXpath="//a[@class='list-group-item ng-binding ng-scope'][contains(text(),'"+clientName+"')]";
			String accountCodeXpath="//div[@class='col-xs-12 list-group scroll-list ng-scope']//a[contains(text(),'"+accountCode+"')]";
			waitForPageLoad();
			Thread.sleep(3000);
			
		driver.findElement(By.xpath(clientNameXpath)).click();
			Thread.sleep(5000);
	driver.findElement(By.xpath(accountCodeXpath)).click();
			continueBtn.click();
		} catch (Exception e) {
			System.out.println("----- selectClientAccount()"+e.getMessage());
		}
		return new ME_DashboardPage();
		
	}
	
	
}
