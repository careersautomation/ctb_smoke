package pages.BelongNBA_Pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ReferencePage {

	@FindBy(xpath ="//h2[contains(text(),'References')]")
	public static WebElement Referenceheading;
	
	
	public ReferencePage() {
		PageFactory.initElements(driver, this);
	}

	
}
