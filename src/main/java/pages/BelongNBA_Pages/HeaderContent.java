package pages.BelongNBA_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HeaderContent {
	
	@FindBy(id = "searchbtn")
	public static  WebElement Search_Button;
	
	@FindBy(xpath = "(//input[@type='text'])[2]")
	public static  WebElement searchTextbox;
	

	@FindBy(xpath ="(//a[contains(text(),'Contacts')])[1]")
	public static WebElement Contactslink;

	
	@FindBy(xpath ="(//a[contains(text(),'References')])[1]")
	public static WebElement Referencelink;

	
	@FindBy(xpath ="//a[@class='menubtn']")
	public static WebElement Menubutton;
	
	@FindBy(xpath ="(//a[contains(text(),'Health')])[1]")
	public static WebElement Healthlink;
	
	@FindBy(xpath ="(//a[contains(text(),'Financial')])[1]")
	public static WebElement Financiallink;
	
	
	@FindBy(xpath ="(//a[contains(text(),'Income Protection')])[1]")
	public static WebElement Incomelink;
	
	@FindBy(xpath ="(//a[contains(text(),'Work/Life')])[1]")
	public static WebElement Worklink;
	
	@FindBy(xpath = "//a[contains(text(),'Log Out')]")
	public static WebElement logoutbtn;
	
	
	
	public HeaderContent() {
		PageFactory.initElements(driver, this);
	}
	 
	 public void searchAKeyword(String keyword)
	 {
	 	setInput(searchTextbox, keyword);
	 	searchTextbox.sendKeys(Keys.ENTER);
	 
	 }
	 
	
 public void navigateViaFlyout(WebElement e) {
	 clickElement(Menubutton);
	 clickElement(e);
 }
 
 public void clickHeaderLink(WebElement headerLink) {
	 clickElement(headerLink);
 }

 
 public void logout() throws InterruptedException
 {
  clickElement(logoutbtn);

 }
 
 
}


