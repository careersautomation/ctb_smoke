package pages.BelongNBA_Pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WorkAndLifePage {


	@FindBy(xpath ="//span[contains(text(),'Work/Life')]")
	public static WebElement Worktext;
	
	public WorkAndLifePage() {
		PageFactory.initElements(driver, this);
	}

	
}
