package pages.BelongNBA_Pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FinancePage {
	
	@FindBy(xpath ="//span[contains(text(),'Financial')]")
	public static WebElement Financialtext;

	
	public FinancePage() {
		PageFactory.initElements(driver, this);
	}

	
}
