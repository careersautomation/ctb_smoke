package pages.BelongNBA_Pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HealthPage {

	@FindBy(xpath ="//span[contains(text(),'Health')]")
	public static WebElement Healthtext;
	
	
	public HealthPage() {
		PageFactory.initElements(driver, this);
	}

	
}
