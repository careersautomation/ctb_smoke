package pages.BelongNBA_Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.driver;

import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
public class Loginpage {

		 @FindBy(id = "txtUserName")
		public static WebElement Usernametextbox;
		
		 @FindBy(id = "txtPassword")
		public static WebElement Passwordtextbox;
		 
		 @FindBy(xpath = "//input[contains(@id,'btnLogin')]")
		 public static WebElement Submit;
		 
		
		
		 @FindBy(xpath ="//span[contains(text(),'Please Login Below')]")
			public static  WebElement LoginBelow;
		
		
		 public Loginpage() {
				PageFactory.initElements(driver, this);
			}
		
		 public void login(String username, String password) throws InterruptedException
		 {
//			 waitForElementToDisplay(LoginBelow);
			 System.out.println("entering username");
			 setInput(Usernametextbox,username);
			 setInput(Passwordtextbox,password);
		 	 clickElement(Submit);
		 	
		 }
}
	


	


