package pages.BelongNBA_Pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class IncomePage {

	@FindBy(xpath ="//span[contains(text(),'Income Protection')]")
	public static WebElement Incometext;
	
	
	
	public IncomePage() {
		PageFactory.initElements(driver, this);
	}

	
}
