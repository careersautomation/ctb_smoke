package pages.BelongNBA_Pages;
import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
public class Homepage {
	
	 @FindBy(xpath ="//h2[contains(text(),'Welcome to Your NBA Benefits')]")
		public static WebElement NBAHeader;

	 
	public Homepage() {
		PageFactory.initElements(driver, this);
	}

}
