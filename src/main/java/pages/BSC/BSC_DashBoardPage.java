package pages.BSC;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class BSC_DashBoardPage {

	@FindBy(xpath="(//a[contains(text(),'Balance Sheet Calculator (QA)')])[4]")
	public static WebElement BalanceSheetCalculatorQA;
	
	
	@FindBy(xpath="//h4[contains(text(),'International Compensation Calculators')]")
	public static WebElement InternationalCompensationCalculatorText;
	
	@FindBy(xpath="//a[contains(text(),'11325 Multi-National Pay with Americans Abroad - Catherine Bruning ')]")
	public static WebElement link11325;
	
	@FindBy(css="a[href='https://qa-mobilityportal.mercer.com/Account-Client-selection']")
	public static WebElement SwitchClient;
	
	public BSC_DashBoardPage() {
		PageFactory.initElements(driver, this);
	}
	public void BalanceSheetSelection() throws InterruptedException {
		
		scrollToElement(driver,InternationalCompensationCalculatorText);
		waitForElementToDisplay(InternationalCompensationCalculatorText);
		clickElement(BalanceSheetCalculatorQA);
		
	}
	
	public void SwitchClient() {
		
		waitForElementToDisplay(SwitchClient);
		clickElement(SwitchClient);
	}
	
}
