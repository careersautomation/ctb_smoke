package pages.BSC;

import org.openqa.selenium.WebElement;
import static driverfactory.Driver.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.thoughtworks.selenium.webdriven.commands.Click;

public class BSC_AccountSelectionPage {

	
	@FindBy(linkText="MERCER")
	public static WebElement GHRM_Mercer;
	
	@FindBy(linkText="97216906 GHRM Clients -")
	public static WebElement GHRMClient;
	
	@FindBy(xpath="//button[contains(text(),'Continue')]")
	public static WebElement Continue;
	
	@FindBy(linkText="Mercer (New York)")
	public static WebElement ICS_MercerNY;
	
	@FindBy(xpath="//a[contains(text(),'9999 Multi-National Pay with Americans Abroad - Catherine Bruning')]")
	public static WebElement Account9999;
	
	@FindBy(linkText="11325 Multi-National Pay with Americans Abroad - Catherine Bruning")
	public static WebElement Account11325;
	
	@FindBy(xpath="//h3[contains(text(),'Select a customer:')]")
	public static WebElement SelectACustomerText;
	
	public BSC_AccountSelectionPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public void GHRMAccount() throws InterruptedException {

		Thread.sleep(3000);
		clickElement(GHRM_Mercer);
		clickElement(Continue);
	}
	
	public void ICSAccount() throws InterruptedException {
		
		Thread.sleep(3000);
		clickElement(ICS_MercerNY);
		clickElement(Account11325);
		clickElement(Continue);
	}
	
}
