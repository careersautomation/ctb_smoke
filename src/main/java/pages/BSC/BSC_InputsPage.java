package pages.BSC;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.*;


public class BSC_InputsPage {

	@FindBy(css="input[id='salary']")
	public static WebElement AnnualBasepayAmountField;
	
	@FindBy(css="select[id='MaritalHome']")
	public static WebElement MaritalStatusAtHome;
	
	@FindBy(css="select[id='MaritalHost']")
	public static WebElement MaritalStatusAtHost;
	
	@FindBy(css="a[id='iconExpandCollapseCOL']")
	public static WebElement CostOfLivingIcon;
	
	@FindBy(css="select[id='COLSurveyDate']")
	public static WebElement SurveyDate;
	
	@FindBy(css="a[id='iconExpandCollapseHousing']")
	public static WebElement HostLocationHousingIcon;
	
	@FindBy(css="select[id='HousingData']")
	public static WebElement HousingDataValue;
	
	@FindBy(css="select[id='HousingSurveyDates']")
	public static WebElement HousingSurveyDates;
	
	@FindBy(css="a[href='#TaxHostGrossUpContent']")
	public static WebElement TaxHostGrossUpIcon;
	
	@FindBy(xpath="(//div[@class='col-sm-5 custom-style-inputs']/input[@type='checkbox'])[3]")
	public static WebElement TaxHostGrossUpCheckBox;
	
	@FindBy(css="button[id='btnContinue']")
	public static WebElement ContinueButton;
	
public BSC_InputsPage() {
		
		PageFactory.initElements(driver, this);
}
	
	public void GHRM_InputPageselection() throws InterruptedException {
		
		waitForElementToDisplay(AnnualBasepayAmountField);
		setInput(AnnualBasepayAmountField,"150000");
		scrollToElement(driver,MaritalStatusAtHome);
		selEleByVisbleText(MaritalStatusAtHome, "Married with 2 children");
		selEleByVisbleText(MaritalStatusAtHost, "Married with 2 children");
		clickElement(CostOfLivingIcon);
		selEleByVisbleText(SurveyDate, "March 2018");
		scrollToElement(driver,HostLocationHousingIcon);
		clickElement(HostLocationHousingIcon);
		selEleByVisbleText(HousingDataValue, "Host location housing costs");
		selEleByVisbleText(HousingSurveyDates, "March 2018");
		scrollToElement(driver,TaxHostGrossUpIcon);
		clickElement(TaxHostGrossUpIcon);
		clickElement(TaxHostGrossUpCheckBox);
		scrollToElement(driver,ContinueButton);
		clickElement(ContinueButton);
		
		
	}
	
	public void ICS_InputPageselection() throws InterruptedException {
		
		waitForElementToDisplay(AnnualBasepayAmountField);
		setInput(AnnualBasepayAmountField,"1500000");
		scrollToElement(driver,MaritalStatusAtHome);
		selEleByVisbleText(MaritalStatusAtHome, "Married with 1 child");
		selEleByVisbleText(MaritalStatusAtHost, "Married with 1 child");
		scrollToElement(driver,HostLocationHousingIcon);
		Thread.sleep(2000);
		clickElement(HostLocationHousingIcon);
		selEleByVisbleText(HousingDataValue, "Expatriate accommodation costs by income level and family size");
		selEleByVisbleText(HousingSurveyDates, "March 2018");
		scrollToElement(driver,TaxHostGrossUpIcon);
		clickElement(TaxHostGrossUpIcon);
		clickElement(TaxHostGrossUpCheckBox);
		Thread.sleep(2000);
		scrollToElement(driver,ContinueButton);
		Thread.sleep(5000);
		clickElement(ContinueButton);
		
		
	}
	
	
	
}
