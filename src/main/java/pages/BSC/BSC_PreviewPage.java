package pages.BSC;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.*;
public class BSC_PreviewPage {
	@FindBy(css="button[id='btnContinue']")
	public static WebElement ContinueButton;
	
	
public BSC_PreviewPage() {
		
		PageFactory.initElements(driver, this);
}
	public void previewContinue() throws InterruptedException {
		
		waitForPageLoad();
		waitForElementToDisplay(ContinueButton);
		scrollToElement(driver,ContinueButton);
		Thread.sleep(2000);
		clickElement(ContinueButton);
	}
}
