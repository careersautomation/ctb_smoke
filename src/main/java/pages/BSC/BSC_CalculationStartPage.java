package pages.BSC;

import static driverfactory.Driver.driver;

import java.util.List;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BSC_CalculationStartPage {

	@FindBy(xpath="//h1[contains(text(),'Balance Sheet Calculator')]")
	public static WebElement BSCTitleText;
	
	@FindBy(css="a[href='/BalanceSheet/v1/Home/SignOut']")
	public static WebElement ReturnToMobilityExchangeLink;
	
	@FindBy(css="select[id='HomeLocation']")
	List<WebElement> HomeLocationOption;
		
	@FindBy(css="select[id='HomeLocation']")
	public static WebElement HomeDropDownLink;
	
	@FindBy(xpath="//select[contains(@data-bind,'HomeLocationList1')]")
	public static WebElement HomeDropDownList1;
	
	@FindBy(css="select[id='HostLocation']")
	List<WebElement> HostLocationOption;
	
	@FindBy(css="select[id='HostLocation']")
	public static WebElement HostDropDownLink;
	
	@FindBy(xpath="//select[contains(@data-bind,'HostLocationList1')]")
	public static WebElement HostDropDownList1;
	
	@FindBy(css="button[id='submitbtnId']")
	public static WebElement RunANewCalculationButton;
	
	@FindBy(css="select[id='IndexType']")
	public static WebElement ICS_IndexType;
	
	@FindBy(xpath="//h3[contains(text(),'Start a Calculation')]")
	public static WebElement StartCalculationText;
	
	@FindBy(css="input[id='rdoHomeLoc']")
	public static WebElement HomeLocationRadioButton;
	
	@FindBy(css="input[id='rdoHostLoc']")
	public static WebElement HostLocationRadioButton;
	

	
	
public BSC_CalculationStartPage() {
		
		PageFactory.initElements(driver, this);
}

public void GHRM_StartCalculation(String country, String country1) throws InterruptedException {

	clickElement(HomeDropDownLink);
	Thread.sleep(4000);
	selEleByVisbleText(HomeDropDownLink, country);
	Thread.sleep(4000);
	selEleByVisbleText(HostDropDownLink, country1);
	clickElement(RunANewCalculationButton);
		
	
}

public void ICS_StartCalculation(String country2, String country3) throws InterruptedException {

	selEleByVisbleText(ICS_IndexType, "Standard");
	clickElement(HostLocationRadioButton);
	Thread.sleep(3000);
	clickElement(HostDropDownList1);
	selEleByVisbleText(HostDropDownList1, country2);
	selEleByVisbleText(HomeDropDownList1, country3);
	Thread.sleep(2000);
	clickElement(RunANewCalculationButton);
		
	
}


public void selectHomeLocation(String country) {
	// TODO Auto-generated method stub
	for(WebElement e: HomeLocationOption)
	{
		if(e.getText().contains(country))
		{
			e.click();
			break;
		}
	}
}

public void selectHostLocation(String country1) {
	// TODO Auto-generated method stub
	for(WebElement e: HostLocationOption)
	{
		if(e.getText().contains(country1))
		{
			e.click();
			break;
		}
	}
	
	
}
	
}
