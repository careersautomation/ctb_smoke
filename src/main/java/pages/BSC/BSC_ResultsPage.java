package pages.BSC;

import static driverfactory.Driver.driver;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.List;

import static driverfactory.Driver.*;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.thoughtworks.selenium.webdriven.commands.GetText;

public class BSC_ResultsPage {

	@FindBy(xpath="//h2[contains(text(),'Calculation Summary')]")
	public static WebElement CalculationSummaryText;
	
	@FindBy(css="a[href='/BalanceSheet/v1/Home/SignOut']")
	public static WebElement ReturnToMobilityExchangeLink;
	
	@FindBy(xpath="//label[contains(@data-bind,'TotalHomeCountryCompensationHome')]")
	public static WebElement TotalHomeCountryCompensationValue;
	
	@FindBy(xpath="//label[contains(@data-bind,'HostEstimatedPersonalIncomeTaxHome')]")
	public static WebElement HostEstimatedPersonalIncomeTaxHomeValue;
	
	@FindBy(xpath="//label[contains(@data-bind,'EstimatedHomeSocialSecurityHome')]")
	public static WebElement EstimatedHomeSocialSecurityHomeValue;
	
	@FindBy(xpath="(//label[@class='control-label ThousandSeparator light'])[13]")
	public static WebElement ICS_CountryTax;
	
	@FindBy(xpath="(//label[@class='control-label ThousandSeparator light'])[15]")
	public static WebElement ICS_SocialSecurity;
	
	@FindBy(xpath="(//label[@class='control-label ThousandSeparator bold'])[15]")
	public static WebElement ICS_TotNetCompensation;
	
	@FindBy(xpath="//button[contains(text(),'Export')]")
	public static WebElement ExportButton;
	
	@FindBy(xpath="//a[contains(text(),'EXCEL')]")
	public static WebElement ExcelDownload;
	
	@FindBy(xpath="//a[contains(text(),'PDF')]")
	public static WebElement PDFDownload;
	
	
	
public BSC_ResultsPage() {
		
		PageFactory.initElements(driver, this);
}

public void ReturnMobility() {
	
	waitForElementToDisplay(ReturnToMobilityExchangeLink);
	clickElement(ReturnToMobilityExchangeLink);
	
	
}
	public void comparevalues() {
	
		
		
		try{
			System.out.println(TotalHomeCountryCompensationValue.getText());
			
			   Integer i = Integer.parseInt(TotalHomeCountryCompensationValue.getText());
			   System.out.println(i);
			}catch(NumberFormatException ex){ // handle your exception
				System.out.println("not a number");
			}
		
	}
	
	
	public Boolean checkValues(String s) {
		
		String rep=s.replaceAll("[(),]","");
		System.out.println(rep);
      // the String to int conversion happens here
      int i = Integer.parseInt(rep.trim());
 
      // print out the value after the conversion
      
      if(i>0) {
    	  System.out.println("Number is greater than 0");
    	  return true;
      }
      
       return false;
    
	}
	
	public void ExcelDownload() {
		
		clickElement(ExportButton);
		clickElement(ExcelDownload);
	}
	 
public void PDFDownload() {
		
		clickElement(ExportButton);
		clickElement(PDFDownload);
	}

public void GetExcelNames() throws EncryptedDocumentException, InvalidFormatException, IOException  {
File myFile = getLatestFilefromDir(); 
Workbook wb = WorkbookFactory.create(myFile);

List<String> sheetNames = new ArrayList<String>();

for (int i=0; i<wb.getNumberOfSheets(); i++) {
	System.out.println(wb.getSheetName(i));
    sheetNames.add( wb.getSheetName(i) );
}

//System.out.println(sheetNames);

}

public File getLatestFilefromDir(){
	//String dirPath
    File dir = new File("C:/Users/logesh-Chandrasekar/Downloads");
    File[] files = dir.listFiles();
    if (files == null || files.length == 0) {
        return null;
    }

    File lastModifiedFile = files[0];
    for (int i = 1; i < files.length; i++) {
       if (lastModifiedFile.lastModified() < files[i].lastModified()) {
           lastModifiedFile = files[i];
       }
    }
    System.out.println(lastModifiedFile);
    return lastModifiedFile;
   
}




/*public void VerifyExcelDownloaded() {
	
	String downloadPath="C:/Users/logesh-Chandrasekar/Downloads";
	 File getLatestFile = getLatestFilefromDir(downloadPath);
	    String fileName = getLatestFile.getName();
	    System.out.println(fileName);
	  //  Assert.assertTrue(fileName.equals("mailmerge.xls"), "Downloaded file name is not matching with expected file name");
}*/

public void waitUntilFileToDownload(String folderLocation) throws InterruptedException {
    File directory = new File(folderLocation);
    boolean downloadinFilePresence = false;
    File[] filesList =null;
    LOOP:   
        while(true) {
            filesList =  directory.listFiles();
            for (File file : filesList) {
                downloadinFilePresence = file.getName().contains("Balance Sheet Calculation_");
            }
            if(downloadinFilePresence) {
                for(;downloadinFilePresence;) {
                    Thread.sleep(5000);
                    continue LOOP;
                }
            }else {
                break;
            }
        }
}

	
}
