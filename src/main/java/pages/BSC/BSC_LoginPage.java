package pages.BSC;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import static driverfactory.Driver.*;

public class BSC_LoginPage {


	@FindBy(xpath="(//a[@class='btn action my-account-lnk dropdown-toggle'])[1]")
	public static WebElement MyAccount;
	
	@FindBy(xpath="(//a[contains(@class,'btn btn-primary btn-block')])[1]")
	public static WebElement LoginButton;
	

	@FindBy(css="div[class='panel panel-l']")
	public static WebElement SigninText;
	
	@FindBy(css="input[id='ctl00_MainSectionContent_Email']")
	public static WebElement EmailInputField;
	
	@FindBy(css="input[id='ctl00_MainSectionContent_Password']")
	public static WebElement PasswordInputField;
	
	@FindBy(css="button[id='ctl00_MainSectionContent_ButtonSignin']")
	public static WebElement SigninButton;
	
	@FindBy(css="button[id='ctl00_MainSectionContent_ButtonSubmit']")
	public static WebElement MFAConfirmButton;
	
	@FindBy(xpath="(//span[contains(text(),'Logout')])[1]")
	public static WebElement LogoutLink;
	
	@FindBy(xpath="//h1[contains(text(),'Contact Us for Help')]")
	public static WebElement ContactHelpText;
	
	public BSC_LoginPage() {
		PageFactory.initElements(driver, this);
		
	}
	
	public void BSC_Login() throws InterruptedException {
		implicitWait(driver, 3000);
		waitForElementToDisplay(MyAccount);
		clickElement(MyAccount);
		clickElement(LoginButton);
		
	} 
	

	public void Signin(String email , String pwd) throws InterruptedException {
		
		waitForElementToDisplay(SigninText);
		setInput(EmailInputField, email);
		setInput(PasswordInputField, pwd);
		clickElement(SigninButton);
		
if (ContactHelpText.getText().contentEquals("Contact Us for Help")) {
			
			navigateBack(driver);
			setInput(PasswordInputField,"Localizer@123");
			clickElement(SigninButton);
			}
	
		
		
	
	}
	
	

	public void BSC_logout() throws InterruptedException {
		Thread.sleep(3000);
		clickElement(MyAccount);
		clickElement(LogoutLink);
	}
	
	
	
	
	
	
	
	
}
