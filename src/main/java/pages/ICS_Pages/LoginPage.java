package pages.ICS_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.InitTests;

public class LoginPage{
	
	
	@FindBy(id = "ctl00_MainSectionContent_Email")
	public static WebElement usernametextbox;
	
	@FindBy(id = "ctl00_MainSectionContent_Password")
	public static WebElement passwordtextbox;
	
	@FindBy(id = "ctl00_MainSectionContent_ButtonSignin")
	public static WebElement submitButton;
	
	
	
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	
	public void signIn(String username,String password) throws InterruptedException
	{
		
		 setInput(usernametextbox,username);
		 setInput(passwordtextbox,password);
		 clickElement(submitButton);
	}
}