package pages.ICS_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.waitForElementToDisplay;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HeaderPage {
	
	@FindBy(xpath = "//a[@title='Compensation Tables']")
	public static WebElement compensationTableLink;
	
	@FindBy(xpath = "//a[text()='Add Table']")
	public static WebElement addtableLink;
	
	@FindBy(xpath = "(//a[text()='Home'])[1]")
	public static WebElement homelink;
	
	
	
	public HeaderPage() {
		PageFactory.initElements(driver, this);
	}
	
	 public void clickHeaderLink(WebElement headerLink) throws InterruptedException {
			
		 Thread.sleep(8000);
		 clickElement(headerLink);
	 }

	
	public void clickCompensationLink() throws InterruptedException {
		waitForElementToDisplay(compensationTableLink);
		hoverOverElement(driver,compensationTableLink);
		 clickElement(addtableLink);
		 
	 }
}
