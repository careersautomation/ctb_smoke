package pages.ICS_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import driverfactory.Driver;

public class CompensationTablePage {
	
	@FindBy(xpath = "(//input[@type='radio'])[2]")
	public static WebElement oneTimeOrder;
	
	@FindBy(xpath = "//*[@id='hostLocation']")
	public static WebElement hostLocation;
	
	@FindBy(xpath = "//label[contains(text(),'Host Location')]")
	public static WebElement hostLocationLabel;
	

	@FindBy(xpath = "//label[contains(text(),'Report Type')]")
	public static WebElement reportTypeLabel;
	
	@FindBy(xpath="//*[@id='reportType']")
	public static WebElement reportType;
	
	@FindBy(xpath = "(//input[@type='button'])[1]")
	public static WebElement confirmButton;
	
	@FindBy(xpath = "//button[contains(text(),'Purchase')]")
	public static WebElement purchasebtn;
	
	@FindBy(xpath = "//a[contains(text(),'View Table')]")
	public static WebElement viewtable;
	
	@FindBy(xpath = "//button[text()='Go to Current Tables']")
	public static WebElement gotocurrentbtn;
	
	@FindBy(id = "u_nav_logout")
	public static WebElement logoutButton;
	
	@FindBy(xpath = "//h2[text()='Login']")
	public static WebElement loginText;

	
	
	public CompensationTablePage() {
		PageFactory.initElements(driver, this);
	}
	
	public void selectRadioBtn() {
		clickElement(oneTimeOrder);
		
	}

	
	public void selectConfirm() {
		clickElement(confirmButton);
		clickElement(purchasebtn);
		//clickElement(viewtable);
	}
	public void selectGotoCurrentBtn() {
		clickElement(gotocurrentbtn);
		clickElement(logoutButton);
		
	}
	
	
}
