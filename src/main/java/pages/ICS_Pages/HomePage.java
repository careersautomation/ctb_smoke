package pages.ICS_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
//import org.sikuli.script.FindFailed;
//import org.sikuli.script.Pattern;
//import org.sikuli.script.Screen;

public class HomePage {

	@FindBy(xpath = "//a[@href=\"/icsweb/MyAccountShow.do\"]")
	public static WebElement welcomeimatest;
	
	@FindBy(xpath = "//div[contains(text(),'Knowledge Center')]")
	public static WebElement knowledgeCenter;
	
	@FindBy(xpath = "(//ul[@class='icon-list-16x16'])[3]")
	public static List<WebElement> knowledgeCenter_pdf;
	
	@FindBy(xpath="//a[contains(text(),'Balance Sheet Booklet')]")
	public static WebElement balancesheetpdf;
	
	
	
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
//	public void switchwindow()
//	{
//		
//		String winHandleBefore = driver.getWindowHandle();
//		driver.switchTo().window(winHandleBefore);
//		
//	}
//	
	public List<WebElement> getpdfCount() {
		return (knowledgeCenter_pdf);
	}
	
	
	public void testTabs() {
	  
	    String oldTab = driver.getWindowHandle();
	    ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
	   // newTab.remove(oldTab);
	    driver.switchTo().window(newTab.get(1));
	     
	}

//	public void selectpdflink() throws InterruptedException, FindFailed
//	{
//		
//		clickElement(balancesheetpdf);
//		Screen screen = new Screen();
//		Pattern clearlist = new Pattern("C:\\Users\\ananya-palled\\Desktop\\clearlist.png");
//		Pattern open = new Pattern("C:\\Users\\ananya-palled\\Desktop\\openbutton.png");
//		screen.doubleClick(clearlist);
//		screen.wait(open, 10);
//		screen.doubleClick(open);
//		Pattern closebtn = new Pattern("C:\\Users\\ananya-palled\\Desktop\\closebtn.png");
//		screen.doubleClick(closebtn);
//		
//	}
	
	
}
