package pages.ICS_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ClientSelectionPage {
	@FindBy(xpath = "(//a[@class='list-group-item ng-binding ng-scope'])[2]")
	public static WebElement mercersupport;
	
	@FindBy(xpath = "//button[@class='btn btn-primary pull-right ng-scope']")
	public static WebElement continubtn;
	
	
	public ClientSelectionPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void selectCustomer() {
		clickElement(mercersupport);
		clickElement(continubtn);
		
	}
	
	
	
}
