package pages.ICS_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.selEleByVisbleText;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WelcomePage {
//	@FindBy(xpath = "(//a[contains(text(),'Asia')])[1]")
//	public static WebElement location;
//	
	@FindBy(xpath = "//div[@class='welcome hidden-xs']")
	public static WebElement welcomeiMercer;
	
	@FindBy(xpath = "(//span[text()='My Account '])[1]")
	public static WebElement myAccount;
	
	@FindBy(xpath = "(//span[@class='hidden-md'])[1]")
	public static WebElement loginbtn;
	
	@FindBy(xpath = "//h1[contains(text(),'Sign In')]")
	public static WebElement signinHeader;
	
	public WelcomePage() {
		PageFactory.initElements(driver, this);
	}
	
	
	
	public void  myAccountLink( ) throws InterruptedException
	 {
		clickElement(myAccount);
		clickElement(loginbtn);
			
}
}


