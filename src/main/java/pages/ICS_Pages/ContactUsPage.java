package pages.ICS_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class ContactUsPage  {
	@FindBy(id = "ctl00_MainSectionContent_Email")
	public static WebElement usernametextbox1;
	
	@FindBy(id = "ctl00_MainSectionContent_Password")
	public static WebElement passwordtextbox1;
	
	@FindBy(id = "ctl00_MainSectionContent_ButtonSignin")
	public static WebElement submitButton1;
	
	
	public ContactUsPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void contactUs(String user,String pwd) throws InterruptedException
	{
		if (driver.getPageSource().contains("Contact Us for Help"))
		{
			driver.navigate().back();
//			LoginPage loginpage =new LoginPage();
//			loginpage.signIn(USERNAME,PASSWORD);
			 setInput(usernametextbox1,user);
			 setInput(passwordtextbox1,pwd);
			 clickElement(submitButton1);
			
		}
		else
		{
			System.out.println("not present");
		}
		
	}
}
