package pages.ICS_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MytoolsPage {
	@FindBy(xpath = "(//a[text()='One-Time Tables'])[4]")
	public static WebElement onetimetable;

public MytoolsPage() {
	PageFactory.initElements(driver, this);
}

public void selectTimeTable() {
	clickElement(onetimetable);
}
	
}
