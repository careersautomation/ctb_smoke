package pages.WINRegression;

import static driverfactory.Driver.*;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Edit_View {

	@FindBy(xpath = "//button[@class='edit-view-button']")
	public static WebElement Edit;

	@FindBy(xpath = "//button[@data-purpose='view-edit']")
	public static WebElement EditView;

	@FindBy(xpath = "//label[text()='Data Effective Date']")
	public static WebElement dateeffectivedate;

	@FindBy(xpath = "//label[text()='Country']")
	public static WebElement Country;

	@FindBy(xpath = "//button[text()='Save As']")
	public static WebElement Saveas;

	@FindBy(xpath = "//button[@data-purpose='done']")
	public static WebElement Done;

	@FindBy(xpath = "//input[@id='view-name-input']")
	public static WebElement Viewname;

	@FindBy(xpath = "//div[@data-purpose='save-dialog']//button[text()='Save']")
	public static WebElement save;

	@FindBy(xpath = "//button[@data-id='searchContinueButton']")
	public static WebElement Continue;

	@FindBy(xpath = "//button[@data-purpose='view-select']//i[@class='fa fa-caret-down']")
	public static WebElement View_dropdown;
	
	@FindBy(xpath = "//a[text()='Edit View']")
	public static WebElement Edit_View_Page;
	
	@FindBy(xpath = "//span[text()='Data Effective Date']")
	public static WebElement Data_Effective_Date;
	
	@FindBy(xpath = "//span[text()='Country']")
	public static WebElement country;
	
	@FindBy(xpath = "//button[@data-purpose='search-submit']")
	public static WebElement Search;

	Random rg = new Random();
	int randomInt = rg.nextInt(100000);
	String numberAsString = Integer.toString(randomInt);
	public String viewname = "View" + numberAsString;

	public Edit_View() {
		PageFactory.initElements(driver, this);
	}

	public void editview() {
		waitForElementToDisplay(Edit);
		clickElement(Edit);
		waitForElementToDisplay(EditView);
		clickElement(EditView);
		waitForPageLoad();
		delay(5000);
	}

	public void editview1() {
		waitForElementToDisplay(dateeffectivedate);
		clickElement(dateeffectivedate);
		waitForElementToDisplay(Country);
		clickElement(Country);
		waitForElementToDisplay(Saveas);
		clickElement(Saveas);
		waitForElementToDisplay(Viewname);
		setInput(Viewname, viewname);
		waitForElementToDisplay(save);
		clickElement(save);
		delay(3000);
		waitForElementToDisplay(Done);
		clickElement(Done);
		waitForPageLoad();
		delay(3000);
		waitForElementToDisplay(Search);
		clickElement(Search);
	}
}
