package pages.PAT_pages;

import static driverfactory.Driver.*;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class PAT_Loginpage {

	@FindBy(id="ctl00_MainSectionContent_Email")
	public WebElement email;

	@FindBy(id="ctl00_MainSectionContent_Password")
	public WebElement password;

	//a[@class = 'btn action my-account-lnk dropdown-toggle']
	@FindBy(xpath="//span[text()='My Account ']")
	public WebElement myAccounts;

	@FindBy(xpath="//a[text()='Platform Administration']")
	public WebElement PlatformAdminLink;

	@FindBy(id="ctl00_MainSectionContent_ButtonSignin")
	public WebElement signIn;

	@FindBy(xpath= "//a[contains(text(),'MERCER')]")
	public WebElement Mercerlink;

	@FindBy(xpath="//a[contains(text(),'97216906 GHRM Clients -')]")
	public WebElement Accountlink;

	@FindBy(xpath="//div[@id='dnn_ctr505_ClientAccountSelection_pnlMain']//div[2]/button")
	public WebElement continueButton;

	@FindBy(xpath = "//h3[contains(text(),'Platform Administration')]")
	public  WebElement platAdmin;

	@FindBy(xpath = "//h1")
	public WebElement header;

	//UserManagement xpath
	@FindBy(id="divUserManagement")
	public static WebElement UserMgmt;

	@FindBy(xpath="//a[contains(text(),' Compensation Localizer ')]")
	public WebElement CompLocalizer;

	@FindBy(xpath="//a[contains(text(),'All Other Applications')]")
	public WebElement allOtherApps;

	@FindAll(@FindBy(how = How.XPATH, using = "//div[3]//div[1]/ul/li[position()>0 and position()<8]/a"))    
	public List<WebElement> allElements;

	//UsertypeMgmt
	@FindBy(id="divUserTypeManagement")
	public WebElement UserTypeMgmt;

	@FindBy(xpath="//h4[contains(text(),'User Type Management')]")
	public WebElement UserTypeHeader;

	//RoleMgmt
	@FindBy(id="divRoleManagement")
	public WebElement RoleMgmt;

	@FindBy(xpath="//h4[contains(text(),'Role Management')]")
	public WebElement RoleMgmtHeader;

	//Release Manager
	@FindBy(id="divReleaseManager")
	public WebElement RelManager;

	@FindBy(xpath="//h4[contains(text(),'Release  Manager')]")
	public WebElement RelManagerHeader;

	@FindBy(xpath="//a[contains(text(),'Tax Data Management')]")
	public WebElement TaxDataMgmt;

	@FindBy(xpath="//a[contains(text(),'Tax PDF Management')]")
	public WebElement TaxPDFMgmt;

	@FindBy(xpath="//a[contains(text(),'Tax Data - Bulk Management')]")
	public WebElement TaxDataBulkMgmt;

	//Content Managament
	@FindBy(id="divContentManager")
	public WebElement ContMgmt;

	@FindBy(xpath="//h4[contains(text(),'Content Management')]")
	public WebElement ContMgmtHeader;

	//Application Statistics
	@FindBy(id="divApplicationStatistics")
	public WebElement AppStats;

	@FindBy(xpath="//h4[contains(text(),'Application Statistics')]")
	public WebElement AppStatsHeader;

	@FindBy(xpath="//a[contains(text(),'Compensation Localizer Calculation Report')]")
	public WebElement CLCReport;

	//License Agreement Management

	@FindBy(id="divLicenseManager")
	public WebElement LAM;

	@FindBy(xpath="//h4[contains(text(),'License  Agreement Management')]")
	public WebElement LAMHeader;

	@FindBy(xpath="//a[contains(text(),'Compensation Localizer Reset')]")
	public WebElement CLReset;

	//Spendable Income

	@FindBy(id="divSpendableIncome")
	public WebElement SpendIncome;

	@FindBy(xpath="//h4[contains(text(),'Spendable Income - Data Management')]")
	public WebElement SpendIncomeHeader;

	@FindBy(xpath="//a[@href='/PlatformAdministration/v1/Home/SignOut']")
	public WebElement signOutLink;

	@FindBy(xpath="//h1[contains(text(),'My Tools & Data')]")
	public WebElement dashboardHeader;

	@FindBy(xpath="//div[@id='dnn_ctr490_view_pnlMain']/div/div/tabs/div/div/ul/li[1]/a")
	public WebElement myToolsTab;

	@FindBy(xpath="//div[@id='border']/div[1]/label")
	public WebElement CLCReportLabel;

	//Calendar

	@FindBy(id="dpStartDate")
	public WebElement dateField;

	@FindBy(xpath="//div[@id='border']/div[3]/div[1]/img")
	public WebElement calIcon;

	@FindBy(xpath="//div[@id='ui-datepicker-div']/div/div/select[1]")
	public WebElement monthDropdown;

	@FindBy(xpath="//div[@id='ui-datepicker-div']/div/div/select[2]")
	public WebElement yearDropdown;

	@FindBy(tagName = "td") 
	public List<WebElement> cols;

	@FindBy(tagName = "tr") 
	public List<WebElement> rows;

	@FindBy(id= "btnExport")
	public WebElement exportBtn;
	
	public PAT_Loginpage() {
		PageFactory.initElements(driver, this);
	}


	public void login() {

		waitForElementToDisplay(email);
		email.sendKeys("compensation.localizer@mercer.com");
		waitForElementToDisplay(password);
		password.sendKeys("Localizer@123");
		clickElement(signIn);
		if (driver.getPageSource().contains("Contact Us for Help")) {
			driver.navigate().back();
			waitForElementToDisplay(password);
			password.sendKeys("Localizer@123");
			clickElement(signIn);
		}
		else {
			System.out.println("Reference Error Not Found");
		}

	}

	public void clickCustomerAndAccount() {
		clickElement(Mercerlink);
		waitForPageLoad();
		clickElement(Accountlink);
		clickElement(continueButton);
		waitForPageLoad();

	}
	public void clickPlatformAdmin() {
		waitForElementToDisplay(myAccounts);
		clickElement(myAccounts);
		waitForElementToDisplay(PlatformAdminLink);
		clickElement(PlatformAdminLink);
		//Add switch to different browser code here
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
		waitForElementToDisplay(platAdmin);
	}

	public void clickUserMgmtButton() {
		clickElement(UserMgmt);
		waitForPageLoad();
		waitForElementToDisplay(CompLocalizer);
	}

	public void clickUserTypeMgmt() {
		clickElement(UserTypeMgmt);
		waitForPageLoad();
		waitForElementToDisplay(UserTypeHeader);
	}

	public void clickRoleMgmt() {
		clickElement(RoleMgmt);
		waitForPageLoad();
		waitForElementToDisplay(RoleMgmtHeader);
	}

	public void clickRelManager() {
		clickElement(RelManager);
		waitForPageLoad();
		waitForElementToDisplay(RelManagerHeader);
	}

	public void clickContMgmt() {
		clickElement(ContMgmt);
		waitForPageLoad();
	}

	public void clickAppStats() {
		clickElement(AppStats);
		waitForPageLoad();
	}

	public void clickLAM() {
		clickElement(LAM);
		waitForPageLoad();
		waitForElementToDisplay(LAMHeader);
	}

	public void clickSpendIncome() {
		clickElement(SpendIncome);
		waitForPageLoad();
	}

	public void clicksignOut() {
		clickElement(signOutLink);
		waitForElementToDisplay(dashboardHeader);
	}

	public void clickCLCReport() {
		waitForElementToDisplay(CLCReport);
		clickElement(CLCReport);
	}

	public void selectDateByJS() {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MMM/yyyy");  
		LocalDateTime now = LocalDateTime.now();  
		System.out.println(dtf.format(now)); 
		LocalDate value = now.minusMonths(1).toLocalDate();
		System.out.println(dtf.format(value));
		String dateValue = dtf.format(value);
		String[] date = dateValue.split("/", 5);
		System.out.print(" " +date[0]);
		selEleByVisbleText(monthDropdown, date[1]);
		selEleByVisbleText(yearDropdown, date[2]);

		for (WebElement cell: cols){

			//Select 10th Date
			if (cell.getText().equals(date[0])){

				cell.findElement(By.linkText(date[0])).click();

				break;

			}

		}
	}
	
	public void clickExport() throws InterruptedException {
		clickElement(exportBtn);
		Thread.sleep(5000);
	}
	
	
	
	 public boolean CheckFile() // the name of the zip file which is obtained, is passed in this method
     {
		 
		 boolean flag = false;
		    File dir = new File("C:/Users/vidya-dandinashivara/Downloads");
		    File[] dir_contents = dir.listFiles();
		  	    
		    for (int i = 0; i < dir_contents.length; i++) {
		        if (dir_contents[i].getName().equals("CompLocReportResults.xlsx"))
		            return true;
		            }

		    return flag;
     }
}
