package ui.smoke.testcases.PAT;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.verifyElementTextContains;

import org.testng.annotations.Test;
import org.testng.xml.LaunchSuite.ExistingSuite;

import com.thoughtworks.selenium.webdriven.commands.IsElementPresent;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.PAT_pages.PAT_Loginpage;
import utilities.InitTests;
import verify.SoftAssertions;
import static verify.SoftAssertions.assertTrue;

public class PATLogin extends InitTests {

	@Test(priority = 1, enabled = true)
	public void verifyLogin() throws Exception {
		try {
			test = reports.createTest("PATLogin");
			test.assignCategory("smoke");
			initWebDriver("https://qa-mobilityportal.mercer.com/Dashboard/MyTools","CHROME", "", "", "local", test, "");
			PAT_Loginpage loginpage = new PAT_Loginpage();
		
			loginpage.login();
			loginpage.clickCustomerAndAccount();
			loginpage.clickPlatformAdmin();
			verifyElementTextContains(loginpage.platAdmin, "Platform Administration",test);
			verifyElementTextContains(loginpage.header, "Test Talent Impact",test);

			loginpage.clickUserMgmtButton();
			verifyElementTextContains(loginpage.CompLocalizer, "Compensation Localizer",test);
			verifyElementTextContains(loginpage.allOtherApps, "All Other Applications",test);
			
			loginpage.clickUserTypeMgmt();
			verifyElementTextContains(loginpage.UserTypeHeader, "User Type Management",test);
			
			loginpage.clickRoleMgmt();
			verifyElementTextContains(loginpage.RoleMgmtHeader, "Role Management",test);
			
			System.out.println(loginpage.allElements.size());
			
			loginpage.clickRelManager();
			verifyElementTextContains(loginpage.RelManagerHeader, " Release  Manager " ,test);
			verifyElementTextContains(loginpage.TaxDataMgmt, "Tax Data Management",test);
			verifyElementTextContains(loginpage.TaxPDFMgmt, "Tax PDF Management",test);
			verifyElementTextContains(loginpage.TaxDataBulkMgmt, "Tax Data - Bulk Management",test);
			
			loginpage.clickContMgmt();
			verifyElementTextContains(loginpage.ContMgmtHeader, "Content Management",test);
			
			loginpage.clickAppStats();
			verifyElementTextContains(loginpage.AppStatsHeader, " Application Statistics ",test);
			verifyElementTextContains(loginpage.CLCReport, " Compensation Localizer Calculation Report ",test);
			
			loginpage.clickLAM();
			verifyElementTextContains(loginpage.LAMHeader, " License  Agreement Management ",test);
			verifyElementTextContains(loginpage.CLReset, " Compensation Localizer Reset ",test);
			
			loginpage.clickSpendIncome();
			verifyElementTextContains(loginpage.SpendIncomeHeader, " Spendable Income - Data Management ",test);
			
			loginpage.clicksignOut();
			verifyElementTextContains(loginpage.dashboardHeader, "My Tools & Data",test);
			assertTrue(Driver.isElementExisting(driver,loginpage.myToolsTab, 10), "Element is present.",test);
		
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("testSearchJob()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("testSearchJob()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} 
		finally
		{
			reports.flush();
			driver.close();
			killBrowserExe(BROWSER_TYPE);

		}
	}
	
	@Test(enabled = false ,priority = 2)
	public void verifyCLCReport() throws Exception {
		try {
			test = reports.createTest("verifyCLCReport");
			test.assignCategory("smoke");
			initWebDriver("https://qa-mobilityportal.mercer.com/Dashboard/MyTools","chrome","","","local",test,"");
			PAT_Loginpage loginpage = new PAT_Loginpage();
		
			loginpage.login();
			loginpage.clickCustomerAndAccount();
			loginpage.clickPlatformAdmin();
			verifyElementTextContains(loginpage.platAdmin, "Platform Administration",test);
			verifyElementTextContains(loginpage.header, "Test Talent Impact", test);

			loginpage.clickAppStats();
			verifyElementTextContains(loginpage.AppStatsHeader, " Application Statistics ",test);
			verifyElementTextContains(loginpage.CLCReport, " Compensation Localizer Calculation Report ",test);
			
			loginpage.clickCLCReport();
			verifyElementTextContains(loginpage.CLCReportLabel, "View Calculation Report By:",test);
			loginpage.calIcon.click();
			loginpage.selectDateByJS();
			loginpage.clickExport();
			loginpage.CheckFile();
			assertTrue(loginpage.CheckFile(), "File is verified.",test);
			loginpage.clicksignOut();
			verifyElementTextContains(loginpage.dashboardHeader, "My Tools & Data", test);
			assertTrue(Driver.isElementExisting(driver,loginpage.myToolsTab, 10), "Element is present.",test);
		
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("verifyCLCReport()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("verifyCLCReport()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} 
		finally
		{
			reports.flush();
			driver.close();
			killBrowserExe(BROWSER_TYPE);
		}
}
}
