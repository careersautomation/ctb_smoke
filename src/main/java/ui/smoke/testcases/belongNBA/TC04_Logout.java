package ui.smoke.testcases.belongNBA;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.BelongNBA_Pages.HeaderContent;
import pages.BelongNBA_Pages.Loginpage;
import utilities.InitTests;
import verify.SoftAssertions;


public class TC04_Logout extends InitTests  {
	
	
	@Test(priority = 1, enabled = true)
	public void logout() throws Exception {
		try {
			test = reports.createTest("Logging out from application");
			test.assignCategory("smoke");
			
			HeaderContent header = new HeaderContent();
			header.logout();
		
			Loginpage login = new Loginpage();
			waitForElementToDisplay(Loginpage.LoginBelow);
			SoftAssertions.verifyElementTextContains(Loginpage.LoginBelow, "Please Login Below",test);
			
			
		
		}catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Logout failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Logout failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}finally{
			
			reports.flush();
			driver.close();
		
		}
	}
	
	
}
