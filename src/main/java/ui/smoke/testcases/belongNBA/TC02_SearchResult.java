package ui.smoke.testcases.belongNBA;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import utilities.InitTests;
import verify.SoftAssertions;
import pages.BelongNBA_Pages.HeaderContent;
import pages.BelongNBA_Pages.SearchPage;

public class TC02_SearchResult extends InitTests {
	SoftAssert softAssert = new SoftAssert();
	
	
	@Test(priority = 1, enabled = true)
	public void searchpage() throws Exception {
		try {
			test = reports.createTest("Searching for a keyword");
			test.assignCategory("smoke");
			
			
			HeaderContent header =new HeaderContent();
			header.searchAKeyword("medical");
			Thread.sleep(4000);
			SearchPage medPara =new SearchPage();
			waitForElementToDisplay(SearchPage.MedicalPara);
			SoftAssertions.verifyElementTextContains(SearchPage.MedicalPara,"therapy Mastectomy-related special bras",test);

		}
		
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

	}
}