package ui.smoke.testcases.belongNBA;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.BelongNBA_Pages.ContactsPage;
import pages.BelongNBA_Pages.FinancePage;
import pages.BelongNBA_Pages.HeaderContent;
import pages.BelongNBA_Pages.HealthPage;
import pages.BelongNBA_Pages.IncomePage;
import pages.BelongNBA_Pages.ReferencePage;
import pages.BelongNBA_Pages.WorkAndLifePage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC03_HeaderLinks  extends InitTests  {
	SoftAssert softAssert = new SoftAssert();


	@Test(priority = 1, enabled = true)
	public void headerLink() throws Exception {
		try {
			
			test = reports.createTest("Verifying Header menu");
			test.assignCategory("smoke");
			
			
			HeaderContent links = new HeaderContent();
			
			links.clickHeaderLink(HeaderContent.Contactslink);
			
			ContactsPage contactPage = new ContactsPage();
			waitForElementToDisplay(ContactsPage.Contactheading);
			SoftAssertions.verifyElementTextContains(ContactsPage.Contactheading, "Contacts",test);
			
			links.clickHeaderLink(HeaderContent.Referencelink);
			ReferencePage referencePage = new ReferencePage();
			waitForElementToDisplay(ReferencePage.Referenceheading);
			SoftAssertions.verifyElementTextContains(ReferencePage.Referenceheading, "References",test);
			
			
			links.navigateViaFlyout(HeaderContent.Healthlink);
			HealthPage healthPage = new HealthPage();
			waitForElementToDisplay(HealthPage.Healthtext);
			SoftAssertions.verifyElementTextContains(HealthPage.Healthtext,"Health",test);
			
			links.navigateViaFlyout(HeaderContent.Financiallink);
			FinancePage financePage = new FinancePage();
			waitForElementToDisplay(FinancePage.Financialtext);
			SoftAssertions.verifyElementTextContains(FinancePage.Financialtext,"Financial",test);
			
			links.navigateViaFlyout(HeaderContent.Incomelink);			
			IncomePage incomePage = new IncomePage();
			waitForElementToDisplay(IncomePage.Incometext);
			SoftAssertions.verifyElementTextContains(IncomePage.Incometext, "Income Protection",test);
			
			links.navigateViaFlyout(HeaderContent.Worklink);
			WorkAndLifePage workLifePage = new WorkAndLifePage();
			waitForElementToDisplay(WorkAndLifePage.Worktext);
			SoftAssertions.verifyElementTextContains(WorkAndLifePage.Worktext, "Work/Life",test);
		
			
			
}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

	}
}