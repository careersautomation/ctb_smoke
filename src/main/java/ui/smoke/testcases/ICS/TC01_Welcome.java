package ui.smoke.testcases.ICS;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.ICS_Pages.WelcomePage;
import ui.smoke.testcases.ICS.TC01_Welcome;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC01_Welcome extends InitTests {
	public TC01_Welcome(String appName) {
		super(appName);
		// TODO Auto-generated constructor stub
	}

	@Test(enabled = true)
	public void welcome() throws Exception {
		try {
			test = reports.createTest("Logging in to ICS");
			test.assignCategory("smoke");
			
			TC01_Welcome welcome = new TC01_Welcome("ICS");
			
			System.out.println("Logging in to application");
			initWebDriver(BASEURL, "CHROME", "","","local", test, "");
	
			System.out.println("BaseURL is: " + BASEURL);

			WelcomePage welcomepage=new WelcomePage();
			welcomepage.myAccountLink();
			waitForElementToDisplay(WelcomePage.signinHeader);
			SoftAssertions.verifyElementTextContains(WelcomePage.signinHeader, "Sign In", test);
			

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

	}@AfterSuite
	public void killDriver() {

		killBrowserExe(BROWSER_TYPE);

	}

}


