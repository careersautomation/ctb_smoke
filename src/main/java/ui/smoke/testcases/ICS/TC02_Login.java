package ui.smoke.testcases.ICS;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.killBrowserExe;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.ICS_Pages.ContactUsPage;
import pages.ICS_Pages.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

	public class TC02_Login extends InitTests {
		public TC02_Login(String appName) {
			super(appName);
			// TODO Auto-generated constructor stub
		}
		@Test(enabled = true)
		public void login() throws Exception {
			try {
				test = reports.createTest("Logging in to ICS");
				test.assignCategory("smoke");
				
				TC02_Login log = new TC02_Login("ICS");
				
			LoginPage login =new LoginPage();
			login.signIn(USERNAME, PASSWORD);
			
			ContactUsPage contactus=new ContactUsPage();
			contactus.contactUs(USERNAME, PASSWORD);
				

			} catch (Error e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
				ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			} catch (Exception e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			}

		}
		}

