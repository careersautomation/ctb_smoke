package ui.smoke.testcases.ICS;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static driverfactory.Driver.switchToWindow;
import org.testng.annotations.Test;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.ICS_Pages.HeaderPage;
import pages.ICS_Pages.HomePage;
import verify.SoftAssertions;

public class TC05_Home {
	@Test(enabled = true)
	public void home() throws Exception {
		try {
			test = reports.createTest("Logging in to ICS");
			test.assignCategory("smoke");
			
			HomePage homepage =new HomePage();
			
			switchToWindow("Compensation Tables");
			//homepage.testTabs();
			waitForElementToDisplay(homepage.welcomeimatest);
			Thread.sleep(4000);
			SoftAssertions.verifyElementTextContains(homepage.welcomeimatest, "imatest imatest",test);
			
			HeaderPage headerlinks=new HeaderPage();
			headerlinks.clickHeaderLink(HeaderPage.homelink);
			
			waitForElementToDisplay(homepage.knowledgeCenter);
			SoftAssertions.verifyElementTextContains(homepage.knowledgeCenter, "Knowledge Center",test);
			
			headerlinks.clickCompensationLink();
			
			

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

	}

}

