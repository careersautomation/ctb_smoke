package ui.smoke.testcases.ICS;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.switchToWindowWithURL;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.ICS_Pages.CompensationTablePage;
import utilities.InitTests;
import verify.SoftAssertions;
import static driverfactory.Driver.selEleByVisbleText;

public class TC06_CompensationAddTable extends InitTests{

	@Test(enabled = true)
	public void addtable() throws Exception {
		try {
			test = reports.createTest("Logging in to ICS");
			test.assignCategory("smoke");
			
			CompensationTablePage addtable =new CompensationTablePage();
			addtable.selectRadioBtn();
			
			waitForElementToDisplay(CompensationTablePage.hostLocationLabel);
			selEleByVisbleText(CompensationTablePage.hostLocation,"Athens, Greece");
			
			waitForElementToDisplay(CompensationTablePage.reportTypeLabel);
			selEleByVisbleText(CompensationTablePage.reportType,"Standard Table");
			
			addtable.selectConfirm();
			
			switchToWindowWithURL("https://www.mercer-icsnp.com/");
			addtable.selectGotoCurrentBtn();
			
			waitForElementToDisplay(CompensationTablePage.loginText);
			SoftAssertions.verifyElementTextContains(CompensationTablePage.loginText,"Login",test);
			
			
	

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}finally{
			
			reports.flush();
			driver.close();
		
		}
	}
}
