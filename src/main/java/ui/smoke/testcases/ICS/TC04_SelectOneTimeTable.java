package ui.smoke.testcases.ICS;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.killBrowserExe;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.ICS_Pages.ClientSelectionPage;
import pages.ICS_Pages.MytoolsPage;
import verify.SoftAssertions;

public class TC04_SelectOneTimeTable {
	@Test(enabled = true)
	public void selectlinks() throws Exception {
		try {
			test = reports.createTest("Logging in to ICS");
			test.assignCategory("smoke");
			
			MytoolsPage mytool =new MytoolsPage();
			mytool.selectTimeTable();
			

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

	}
}

