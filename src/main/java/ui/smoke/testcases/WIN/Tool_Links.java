package ui.smoke.testcases.WIN;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.initWebDriver;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.Login;
import pages.WINPages.Signout;
import pages.WINPages.SelectOrg;
import pages.WINPages.Tools;
import utilities.InitTests;

public class Tool_Links extends InitTests{
	SoftAssert softAssert = new SoftAssert();
	public Tool_Links(String appName) {
		super(appName);
	}
	
	@Test(enabled=false)
    public void testSearch() throws Exception {
	  try
	  {
		  Tool_Links m1=new Tool_Links("WIN");
		  test = reports.createTest("Tool_Links");
		  test.assignCategory("smoke");
		  initWebDriver(BASEURL,"CHROME", "", "", "local", test, "");
		  System.out.println("Tools Links");
		  
		  Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		  
		  SelectOrg so = new SelectOrg();
		  so.Organization();
		  //assertTrue(so.Homepage.isDisplayed(),"My Homepage",test);
		  
		  Tools tl = new Tools();
		  tl.About();
		  assertTrue(tl.Close_About_Page.isDisplayed(),"About Page is displayed",test);
		  tl.About_Page();
		 
		  tl.Administration();
		  assertTrue(tl.Administration_Title.isDisplayed(),"Administration Page is displayed",test);
		  tl.Home();
		  
		  tl.My_Profile();
		  assertTrue(tl.User_Profile_Title.isDisplayed(),"User Profile Page is displayed",test);
		  tl.Home();

		  Signout s = new Signout();
		  s.Signout();
		  	  
	} catch (Error e) {
		e.printStackTrace();
		ATUReports.add("Tool_Links()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} catch (Exception e) {
		e.printStackTrace();
		ATUReports.add("Tool_Links()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		driver.close();
	}
  }
}





