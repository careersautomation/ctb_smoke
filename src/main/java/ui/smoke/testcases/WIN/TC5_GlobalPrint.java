package ui.smoke.testcases.WIN;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import utilities.InitTests;
import verify.SoftAssertions;
import static verify.SoftAssertions.*;

import pages.WINPages.*;

public class TC5_GlobalPrint extends InitTests{
	

	public TC5_GlobalPrint(String appName) {
	super(appName);
	}
	

	SoftAssert softAssert = new SoftAssert();
	
				@Test( enabled =true)
				public void WIN_TC_5() throws Exception {
				try {
					TC5_GlobalPrint a1=new TC5_GlobalPrint("WIN");
					test = reports.createTest("TC5_GlobalPrint");
					test.assignCategory("smoke");
					//initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
					System.out.println("TC 5 Global Print");
					
					 /*Login loginPage = new Login();
					  loginPage.login(USERNAME,PASSWORD);
					selectOrgPage Org= new selectOrgPage();
					Org.Organization();*/
					
					
					navigateViaBreadCrumb navigatetohome= new navigateViaBreadCrumb();
					navigatetohome.navigateBack(navigatetohome.home_Breadcrumb);
					
					MMDPage marketData= new MMDPage();
					marketData.global();
					waitForElementToDisplay(MMDPage.global_MercerMarketData);
					verifyElementText(MMDPage.global_MercerMarketData , "Mercer Market Data for Year: All",test);
					GlobalSearchPage search= new GlobalSearchPage();
					search.GlobalJobView();
					waitForElementToDisplay(GlobalSearchPage.MMDResultPage);
					verifyElementText(GlobalSearchPage.MMDResultPage , "Mercer Market Data Results",test);
					
					Global_PrintFunctionality Print = new Global_PrintFunctionality();
					Global_Export Export = new Global_Export();
					Print.chartPrint();
					Export.GlobalReportfileOptions("globalChart");
					//Export.closePopup();
					ProgressCenter prog= new ProgressCenter();
					prog.waitReportGenerate();
					prog.progress(prog.GlobalprogressCenter_button);
					waitForElementToDisplay(ProgressCenter.Globalcomplete_status);
					verifyElementText(ProgressCenter.Globalcomplete_status, "Complete",test);
					waitForElementToDisplay(ProgressCenter.Globalreport_name);
					verifyElementText(ProgressCenter.Globalreport_name, "globalChart",test);
					prog.closeprogress();
					
					Print.GridPrint();
					Export.GlobalReportfileOptions("globalGrid");
					//Export.closePopup();
					prog.waitReportGenerate();
					prog.progress(prog.GlobalprogressCenter_button);
					waitForElementToDisplay(ProgressCenter.globalreport_Table);
					waitForElementToDisplay(ProgressCenter.globalreport_Table);
					waitForElementToDisplay(ProgressCenter.Globalcomplete_status);
					verifyElementText(ProgressCenter.Globalcomplete_status, "Complete",test);
					waitForElementToDisplay(ProgressCenter.Globalreport_name);
					verifyElementText(ProgressCenter.Globalreport_name, "globalGrid",test);
					prog.closeprogress();
					
					Export.sendGlobalexport();
					Export.GlobalReportfileOptions("Global_Export");
					prog.waitReportGenerate();
					prog.progress(prog.GlobalprogressCenter_button);
					waitForElementToDisplay(ProgressCenter.globalreport_Table);
					waitForElementToDisplay(ProgressCenter.globalreport_Table);
					delay(3000);
					waitForElementToDisplay(ProgressCenter.Globalcomplete_status);
					verifyElementText(ProgressCenter.Globalcomplete_status, "Complete",test);
					waitForElementToDisplay(ProgressCenter.Globalreport_name);
					verifyElementText(ProgressCenter.Globalreport_name, "Global_Export",test);
					prog.closeprogress();
					
					/*Sign_out sign = new Sign_out();
					sign.signout(sign.signout_1);*/
					
				} catch (Error e) {
					e.printStackTrace();
					SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
					ATUReports.add("WIN_TC_5", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
					

				} catch (Exception e) {
					e.printStackTrace();
					SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
					ATUReports.add("WIN_TC_5()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

				} 
				finally
				{
					reports.flush();
					//driver.close();

				}
			}			
}
