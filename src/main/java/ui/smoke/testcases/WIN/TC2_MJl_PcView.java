package ui.smoke.testcases.WIN;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.*;
import utilities.InitTests;
import verify.SoftAssertions;
import static verify.SoftAssertions.*;

public class TC2_MJl_PcView  extends InitTests{
	

	public TC2_MJl_PcView(String appName) {
	super(appName);
	}
	
	
	SoftAssert softAssert = new SoftAssert();
	
	@Test(priority = 1, enabled =true)
	public void WIN_TC_2() throws Exception {
	try {
		TC2_MJl_PcView a1=new TC2_MJl_PcView("WIN");
		test = reports.createTest("TC2_MJl_PcView");
		test.assignCategory("smoke");
		//initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		//driver.manage().deleteAllCookies();
		System.out.println("TC 2 MJL PC View");
		/*Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		selectOrgPage Org= new selectOrgPage();
		Org.Organization();*/
		
		navigateViaBreadCrumb navigatetohome=new navigateViaBreadCrumb();
		 navigatetohome.navigateBack(navigatetohome.home_Breadcrumb);
		
		MMDPage marketData= new MMDPage();
		marketData.jobLibrary();
		waitForElementToDisplay(MMDPage.mercerJobLibrary);
		verifyElementText(MMDPage.mercerJobLibrary , "Mercer Job Library",test);
		
		mercerJobLibraryPage MJLSearchPage = new mercerJobLibraryPage();
		MJLSearchPage.pcView();
		waitForElementToDisplay(mercerJobLibraryPage.result_Page);
		verifyElementText(mercerJobLibraryPage.result_Page , "Mercer Market Data Results: Library",test);
		
		combinePCpage combine= new combinePCpage();
		combine.pcCombine();
		Thinking hd=new Thinking();
		hd.waitForInvisibilityOfSpinner();
		//waitForElementToDisplay(combinePCpage.result_table);
		waitForElementToDisplay(mercerJobLibraryPage.result_Page);
		verifyElementText(mercerJobLibraryPage.result_Page , "Mercer Market Data Results: Library",test);
		//waitForElementToDisplay(combine.combine1_result);
		//verifyElementText(combine.combine1_result , "41 - 44 (combined)",test);
		//waitForElementToDisplay(combine.combine2_result);
		//verifyElementText(combine.combine2_result , "59 - 61 (combined)",test);
		
		/*Sign_out sign = new Sign_out();
		sign.signout(sign.signout_2);*/
		
	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_2", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_2()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		reports.flush();
		//driver.close();
		//Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe /T");
		//Runtime.getRuntime().exec("taskkill /F /IM chrome.exe /T");
		
	}
  } 
}
