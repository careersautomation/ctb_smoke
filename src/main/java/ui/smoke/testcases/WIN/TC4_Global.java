package ui.smoke.testcases.WIN;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import utilities.InitTests;
import verify.SoftAssertions;
import static verify.SoftAssertions.*;

import pages.WINPages.*;

public class TC4_Global extends InitTests{
	
	public TC4_Global(String appName) {
		super(appName);
		}
		
	SoftAssert softAssert = new SoftAssert();
	
	@Test(priority = 1, enabled =true)
	public void WIN_TC_4() throws Exception {
	try {
		TC4_Global a1=new TC4_Global("WIN");
		test = reports.createTest("TC4_Global");
		test.assignCategory("smoke");
		//initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		System.out.println("TC 4 Global");
		/* Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		selectOrgPage Org= new selectOrgPage();
		Org.Organization();*/
		
		navigateViaBreadCrumb navigatetohome=new navigateViaBreadCrumb();
		 navigatetohome.navigateBack(navigatetohome.home_Breadcrumb);
		
		/* Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		  
		  Select_Organization selectorg=new Select_Organization();
		  assertTrue(Select_Organization.signOut_link.isDisplayed(),"WIN Application has reached Select Organization page",test);
		  selectorg.select_organization();
		  assertTrue(Select_Organization.Homepage.isDisplayed(),"My Homepage",test);*/
		
		MMDPage marketData= new MMDPage();
		marketData.global();
		waitForElementToDisplay(MMDPage.global_MercerMarketData);
		verifyElementText(MMDPage.global_MercerMarketData , "Mercer Market Data for Year: All",test);
		GlobalSearchPage search= new GlobalSearchPage();
		search.GlobalJobView();
		waitForElementToDisplay(GlobalSearchPage.MMDResultPage);
		verifyElementText(GlobalSearchPage.MMDResultPage , "Mercer Market Data Results",test);
		
		Global_editView edit = new Global_editView();
		edit.editviewbutton();
		Title tt= new Title();
		waitForElementToDisplay(tt.title_tab);
		//verifyElementTextContains(tt.title_tab , "Edit View: JobCountry",test);
		edit.editviewselections();
		edit.edit_view_save();
		
		refineMarketPage refine= new refineMarketPage();
		refine.ClickonRefinement(refine.GloablrefineMarket_Tab);
		waitForElementToDisplay(tt.title_tab);
		verifyElementText(tt.title_tab , "Refine Market",test);
		refine.peerGroupRefinement();
		//waitForElementToDisplay(refine.globalresult_page);
		waitForElementToDisplay(refine.peer_Resultpage);
		verifyElementText(refine.peer_Resultpage , "peer1",test);
		
		sendToMyLibraryPage send= new sendToMyLibraryPage();
		send.sendRecord(send.GlobalsendToLibrary_Tab);
		waitForElementToDisplay(send.Global_popup);
		verifyElementText(send.Global_popup, "This data has been sent to the My Market Library and is now available for benchmarking.",test);
		send.closePopup(send.popup_close);
		
		/*Sign_out sign = new Sign_out();
		sign.signout(sign.signout_1);*/
		
	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_4", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_4()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		
		reports.flush();
		//driver.close();
		//Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe /T");
		//Runtime.getRuntime().exec("taskkill /F /IM chrome.exe /T");
		

	}
}	
}