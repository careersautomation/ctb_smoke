package ui.smoke.testcases.WIN;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import utilities.InitTests;
import verify.SoftAssertions;
import static verify.SoftAssertions.*;
import pages.WINPages.*;

public class TC6_DiagnosticReport extends InitTests {
	

	public TC6_DiagnosticReport(String appName) {
	super(appName);
	}
	
	SoftAssert softAssert = new SoftAssert();
	
	@Test( enabled =true)
	public void WIN_TC_6() throws Exception {
	try {
		TC6_DiagnosticReport a1=new TC6_DiagnosticReport("WIN");
		test = reports.createTest("TC6_DiagnosticReport");
		test.assignCategory("smoke");
		//initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		System.out.println("TC 6 Diagnostic Report");
		
		/* Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		selectOrgPage Org= new selectOrgPage();
		Org.Organization();*/
		
		
		navigateViaBreadCrumb navigatetohome= new navigateViaBreadCrumb();
		navigatetohome.navigateBack(navigatetohome.home_Breadcrumb);
		
		
		
		DiagnosticReportPage diagnostic= new DiagnosticReportPage();
		diagnostic.ClickDiagnosticReport();
		waitForElementToDisplay(DiagnosticReportPage.diagnosticPage);
		verifyElementText(DiagnosticReportPage.diagnosticPage , "Diagnostic Report",test);
		diagnostic.ReportSelections();
		diagnostic.RunReport();
		ProgressCenter prog= new ProgressCenter();
		prog.waitdiagnosticReportGeneration();
		prog.progress(prog.DiagnosticProgressCenter_button);
		waitForElementToDisplay(ProgressCenter.Globalcomplete_status);
		verifyElementText(ProgressCenter.Globalcomplete_status, "Complete",test);
		waitForElementToDisplay(ProgressCenter.Globalreport_name);
		verifyElementText(ProgressCenter.Globalreport_name, "DiagnosticReport_GlobalMVE2E3",test);
		prog.closeprogress();
		
		/*Sign_out sign = new Sign_out();
		sign.signout(sign.signout_1);*/
		
	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_6", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_6()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		reports.flush();
		//driver.close();

	}
}
}
