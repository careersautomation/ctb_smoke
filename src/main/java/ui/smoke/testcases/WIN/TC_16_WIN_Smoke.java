package ui.smoke.testcases.WIN;

import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
//import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementText;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;

import driverfactory.Driver;
import pages.WINPages.AdminPage;
import pages.WINPages.AdministrationPage;
import pages.WINPages.DiagnosticReportPage;
import pages.WINPages.GlobalSearchPage;
import pages.WINPages.LoginPage;
import pages.WINPages.MMDPage;
import pages.WINPages.Manage_My_Organization;
import pages.WINPages.My_Employees;
import pages.WINPages.My_Market_Library_search_page;
import pages.WINPages.My_Reference_Job_Library_Search_page;
import pages.WINPages.My_jobs_search_page;
import pages.WINPages.Navigate_to_Home;
import pages.WINPages.Signout;
import pages.WINPages.Title;
import pages.WINPages.mercerJobLibraryPage;
import pages.WINPages.selectOrgPage;
import utilities.InitTests;

public class TC_16_WIN_Smoke extends InitTests {
	WebDriver driver = null;
	Driver driverObj;
	ExtentTest test = null;
	WebDriver webdriver = null;

	public TC_16_WIN_Smoke(String appName) {
		super(appName);
		// TODO Auto-generated constructor stub
	}
	
	
	@Test(priority = 1, enabled = false)
	public void testLogin() throws Exception {

		try {
			TC_16_WIN_Smoke a1=new TC_16_WIN_Smoke("WIN");
			test = reports.createTest("Admin");
			test.assignCategory("smoke");
			initWebDriver(BASEURL,"PHONTOMJS", "", "", "local", test, "");
			
			Navigate_to_Home h1 = new Navigate_to_Home();

			LoginPage loginPage = new LoginPage();
			loginPage.login(USERNAME, PASSWORD);
			selectOrgPage Org = new selectOrgPage();
			Org.Organization();

			MMDPage marketData = new MMDPage();
			marketData.jobLibrary();
			waitForElementToDisplay(MMDPage.mercerJobLibrary);
			verifyElementText(MMDPage.mercerJobLibrary, "Mercer Job Library", test);
			Title tt = new Title();
			waitForElementToDisplay(tt.MMD_title);
			verifyElementText(tt.MMD_title, "Mercer Market Data for Year: ", test);

			mercerJobLibraryPage MJLSearchPage = new mercerJobLibraryPage();
			MJLSearchPage.jobView();
			waitForElementToDisplay(mercerJobLibraryPage.currentViewResult);
			verifyElementText(mercerJobLibraryPage.currentViewResult, "Job", test);
			
			h1.homepage();
			
			marketData.global();
			waitForElementToDisplay(MMDPage.global_MercerMarketData);
			verifyElementText(MMDPage.global_MercerMarketData, "Mercer Market Data for Year: ", test);
			GlobalSearchPage search = new GlobalSearchPage();
			search.GlobalJobView();

			h1.homepage();

			DiagnosticReportPage d1 = new DiagnosticReportPage();
			d1.ClickDiagnosticReport();
			waitForElementToDisplay(DiagnosticReportPage.diagnosticPage);
			verifyElementText(DiagnosticReportPage.diagnosticPage, "Diagnostic Report", test);

			h1.homepage();

			My_jobs_search_page s1 = new My_jobs_search_page();
			s1.jobssearchpage();
			assertTrue(My_jobs_search_page.Myjobssearchpage.isDisplayed(), "My Jobs Search page", test);

			h1.homepage();

			My_Employees e1 = new My_Employees();
			e1.Myemployees1();
			assertTrue(My_Employees.Myemployees.isDisplayed(), "WIN Application has reached My Employees page", test);

			h1.homepage();

			My_Market_Library_search_page mylibrary = new My_Market_Library_search_page();
			mylibrary.marketlibrarysearchpage();
			assertTrue(My_Market_Library_search_page.MyLibrary.isDisplayed(), "My Market Library search page", test);

			h1.homepage();

			My_Reference_Job_Library_Search_page referencejobs = new My_Reference_Job_Library_Search_page();
			referencejobs.reference_jobs();
			assertTrue(My_Reference_Job_Library_Search_page.MyReferenceJobLibrary.isDisplayed(),
					"WIN Application has reached Reference Library page", test);

			h1.homepage();

			Manage_My_Organization organizationpage = new Manage_My_Organization();
			organizationpage.organization();
			assertTrue(organizationpage.Organizationpage.isDisplayed(), "Organization page", test);

			h1.homepage();

			AdminPage aa = new AdminPage();
			aa.Adminbutton();
			assertTrue(AdminPage.Administration.isDisplayed(), "Administration page", test);

			AdministrationPage a2 = new AdministrationPage();
			a2.JobArchitecture();
			assertTrue(AdministrationPage.JobAchitecture_Tab.isDisplayed(),
					"WIN Application has reached Job Architecture page", test);

			h1.homepage();

			aa.Adminbutton();
			assertTrue(AdminPage.Administration.isDisplayed(), "Administration page", test);
			
			a2.ImportCenter();
			assertTrue(AdministrationPage.Import_Center_page.isDisplayed(),
					"WIN Application has reached Import Center page", test);

			Signout ss1 = new Signout();
			ss1.Signout();

		} finally {

			reports.flush();
			driver.close();

		}

	}
}

