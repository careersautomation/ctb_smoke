package ui.smoke.testcases.WIN;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.initWebDriver;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.Login;
import pages.WINPages.Signout;
import pages.WINPages.SelectOrg;
import pages.WINPages.Tools;
import pages.WINPages.User_Profile;
import utilities.InitTests;

public class Edit_User_Profile extends InitTests{
	SoftAssert softAssert = new SoftAssert();
	public Edit_User_Profile(String appName) {
		super(appName);
		}
	
	@Test(enabled=false)
    public void testSearch() throws Exception {
	  try
	  {
		  Edit_User_Profile m1=new Edit_User_Profile("WIN");
		  test = reports.createTest("Edit User Profile");
		  test.assignCategory("smoke");
		  initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		  System.out.println("Edit User Profile");
		  
		  Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		  
		  SelectOrg so = new SelectOrg();
		  so.Organization();
		  //assertTrue(so.Homepage.isDisplayed(),"My Homepage",test);
		  
		  Tools tl = new Tools();
		  tl.My_Profile();
		  assertTrue(tl.User_Profile_Title.isDisplayed(),"User Profile page is displayed",test);
		  
		  User_Profile up = new User_Profile();
		  up.Edit_User();
		 
		  up.Change_Language_Portu_Brazil();
		  assertTrue(up.Portuguese_Brazil_Button_Title.isDisplayed(), "Language Change to Portuguese Brazil Succesfully",test);
		  
		  up.Change_Language_English();
		  assertTrue(up.Save_Button.isDisplayed(),"Language Change to English Successfully", test);
		  
		  Signout s = new Signout();
		  s.Signout();
		  	  
	} catch (Error e) {
		e.printStackTrace();
		ATUReports.add("Edit_User_Profile()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} catch (Exception e) {
		e.printStackTrace();
		ATUReports.add("Edit_User_Profile()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		reports.flush();
		driver.close();

	}
  }
}





