package ui.smoke.testcases.WIN;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static driverfactory.Driver.getElementText;
import static driverfactory.Driver.waitForElementToDisplay;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.*;
import verify.SoftAssertions;
import static verify.SoftAssertions.*;

import utilities.InitTests;

public class TC8_MyEmployees extends InitTests{
		
		SoftAssert softAssert = new SoftAssert();
		public TC8_MyEmployees(String appName) {
			super(appName);
			}

		@Test()
		public void testSearchJob() throws Exception {
			try {
				TC8_MyEmployees a1=new TC8_MyEmployees("WIN");
				test = reports.createTest("TC8_MyEmployees");
				test.assignCategory("smoke");
				//initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
				System.out.println("TC 8 My Employees");
				
				/*Login loginPage = new Login();			
				loginPage.login(USERNAME,PASSWORD);				
	
				
				Select_Organization selectorg=new Select_Organization();
				//assertTrue(Select_Organization.signOut_link.isDisplayed(),"WIN Application has reached Select Organization page",test);
				selectorg.select_organization();
				assertTrue(Select_Organization.Homepage.isDisplayed(),"My Homepage",test);*/
				

				navigateViaBreadCrumb navigatetohome= new navigateViaBreadCrumb();
				navigatetohome.navigateBack(navigatetohome.home_Breadcrumb);
				
				My_Employees myempl= new My_Employees();
				myempl.Myemployees();
				assertTrue(My_Employees.Myemployees.isDisplayed(),"WIN Application has reached My Employees page",test);
				
				MyEmpdetails myemp = new MyEmpdetails();
				myemp.empdetails();
				assertTrue(myemp.empdetailslinkclose.isDisplayed(),"Employee details pop up accessed",test);
				myemp.empdetailsclose();
				myemp.jobdetails();	
				assertTrue(myemp.jobdetailsclose.isDisplayed(),"Job Details popup accessed",test);
				myemp.jobdetailsclose();
				
				/*MyEmployees_Edit_View myemployeeseditview= new MyEmployees_Edit_View();
				myemployeeseditview.editviewbutton();
				assertTrue(myemployeeseditview.editviewpage.isDisplayed(),"Edit view page",test);				
				myemployeeseditview.editviewselections();				
				myemployeeseditview.edit_view_save();*/
		
				
				/*AddEmployee empadd= new AddEmployee();
				empadd.addemp();
				verifyElementText(empadd.employeename,empadd.empname,test);
				empadd.addempclose();*/
				
				
				MyEmployees_Resultspage employeeresultspage= new MyEmployees_Resultspage();
				employeeresultspage.resultspage();
				assertTrue(myemp.empdetailslinkclose.isDisplayed(),"Employee details pop up accessed",test);
				employeeresultspage.emplinkclose();
				employeeresultspage.jobdetails();
				assertTrue(myemp.jobdetailsclose.isDisplayed(),"Job Details popup accessed",test);
				employeeresultspage.jobdetailsclose();
				employeeresultspage.save();	
				verifyEquals(employeeresultspage.resultsetname,getElementText(MyEmployees_Resultspage.title),test);
				employeeresultspage.resultset();
				verifyEquals(employeeresultspage.resultsetname,getElementText(MyEmployees_Resultspage.title),test);
				
				Results_page_Print print=new Results_page_Print();
				print.print_results();
				ProgressCenter prog= new ProgressCenter();
				prog.progress(prog.Progress_Center);
				prog.generateReport();
				waitForElementToDisplay(prog.reportTable);
				verifyElementText(ProgressCenter.complete_status, "Complete",test);
				for(int i=0; i<=2;i++){
					  try{
						  verifyElementTextContains(ProgressCenter.report_name, Results_page_Print.Reportname,test);
					     break;
					  }catch(Exception e){
						     System.out.println(e.getMessage());
					  }
				}
					  
				
				prog.closeprogress();
				
				Results_page_Export export=new Results_page_Export();
				export.export_results();
				prog.progress(prog.Progress_Center);
				prog.generateReport();
				verifyElementText(ProgressCenter.complete_status, "Complete",test);
				for(int i=0; i<=2;i++){
					  try{
						  verifyElementTextContains(ProgressCenter.report_name, Results_page_Export.Exportname,test);
					     break;
					  }catch(Exception e){
						     System.out.println(e.getMessage());
					  }
				}
				prog.closeprogress();
				
				export.export_results_xls();
				prog.progress(prog.Progress_Center);
				prog.generateReport();
				verifyElementText(ProgressCenter.complete_status, "Complete",test);
				for(int i=0; i<=2;i++){
					  try{
						  verifyElementTextContains(ProgressCenter.report_name, Results_page_Export.Exportname,test);
					     break;
					  }catch(Exception e){
						     System.out.println(e.getMessage());
					  }
				}
				
				prog.closeprogress();
				
				/*
				Sign_out sign_out=new Sign_out();
				sign_out.signout();
				*/

			}
				catch (Error e) {
				e.printStackTrace();
				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("WIN_TC_8()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				

			} catch (Exception e) {
				e.printStackTrace();
				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("WIN_TC_8()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			} 
			finally
			{
				reports.flush();
				//driver.close();

			}

		}
}
