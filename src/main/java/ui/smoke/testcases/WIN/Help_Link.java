package ui.smoke.testcases.WIN;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.initWebDriver;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.Login;
import pages.WINPages.Signout;
import pages.WINPages.SelectOrg;
import pages.WINPages.Help;
import utilities.InitTests;

public class Help_Link extends InitTests{
	SoftAssert softAssert = new SoftAssert();
	public Help_Link(String appName) {
		super(appName);
		}
	
	@Test(enabled=false)
    public void testSearch() throws Exception {
	  try
	  {
		  Help_Link m1=new Help_Link("WIN");
		  test = reports.createTest("Help Link");
		  test.assignCategory("smoke");
		  initWebDriver(BASEURL,"CHROME", "", "", "local", test, "");
		  System.out.println("Help Link");
		  
		  Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		  
		  SelectOrg so = new SelectOrg();
		  so.Organization();
		  //assertTrue(so.Homepage.isDisplayed(),"My Homepage",test);
		  
		  Help t1 = new Help();
		  t1.Help_Link();
		  Actions action= new Actions(driver);
		  action.keyDown(Keys.CONTROL).sendKeys(Keys.TAB).build().perform();
		  //String URL = driver.getCurrentUrl();
		  //Assert.assertEquals(URL, "https://qa.win.mercer.com/Help/");
		 
		  Signout s = new Signout();
		  s.Signout();
		  
		  for(int i = driver.getWindowHandles().size() -1 ; i > 0 ; i--){

		        String winHandle = driver.getWindowHandles().toArray()[i].toString();

		        driver.switchTo().window(winHandle);  

		        driver.close();
		  } 
		  	  	  
	} catch (Error e) {
		e.printStackTrace();
		ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} catch (Exception e) {
		e.printStackTrace();
		ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		reports.flush();
		driver.close();
	}
  }
}