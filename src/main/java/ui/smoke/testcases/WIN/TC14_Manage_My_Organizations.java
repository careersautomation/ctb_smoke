package ui.smoke.testcases.WIN;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getElementText;
import static driverfactory.Driver.initWebDriver;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementText;
import static verify.SoftAssertions.verifyEquals;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.*;
import utilities.InitTests;

public class TC14_Manage_My_Organizations extends InitTests {
SoftAssert softAssert = new SoftAssert();
  
    public TC14_Manage_My_Organizations(String appName) {
	super(appName);
     }
	
	@Test()
    public void testSearch() throws Exception {
	  System.out.println("in test");
	  try
	  {
		  TC14_Manage_My_Organizations a1=new TC14_Manage_My_Organizations("WIN");
		  test = reports.createTest("TC14_Manage_My_Organizations");
		  test.assignCategory("smoke");
		  //initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		  System.out.println("TC 14 Organization");
		  
		  /*Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		  
		  Select_Organization selectorg=new Select_Organization();
		  assertTrue(Select_Organization.signOut_link.isDisplayed(),"WIN Application has reached Select Organization page",test);
		  selectorg.select_organization();
		  assertTrue(Select_Organization.Homepage.isDisplayed(),"My Homepage",test);*/
		  
		  Manage_My_Organization organizationpage=new Manage_My_Organization();
		  organizationpage.organization();
		  
		  organizationpage.organizationpage();
		  
		  Organization_Page_Edit_View edit= new Organization_Page_Edit_View();
		  edit.editviewbutton();
		  assertTrue(edit.editviewpage.isDisplayed(),"Edit view page",test);
		  edit.editviewselections();
		  edit.edit_view_save();
		  
		  Manage_My_Organization_Resultspage resultspage=new Manage_My_Organization_Resultspage();
		  resultspage.Organization_results_page();
		  resultspage.detailsclose();
		  resultspage.rowedit();
		  resultspage.Resultspage();
		  resultspage.save();
		  verifyEquals(Manage_My_Organization_Resultspage.resultsetname,getElementText(Manage_My_Organization_Resultspage.title),test);
		  resultspage.resultset();
		  verifyEquals(Manage_My_Organization_Resultspage.resultsetname,getElementText(Manage_My_Organization_Resultspage.title),test);
		  
		  Results_page_Print print=new Results_page_Print();
		  print.print_results();
		  ProgressCenter prog= new ProgressCenter();
		  prog.progress(prog.Progress_Center);
		  prog.generateReport();
		  verifyElementText(ProgressCenter.complete_status, "Complete",test);
		  verifyElementText(ProgressCenter.report_name, Results_page_Print.Reportname,test);
		  prog.closeprogress();
		  
		  Results_page_Export export=new Results_page_Export();
		  export.export_results();
		  prog.progress(prog.Progress_Center);
		  prog.generateReport();
		  verifyElementText(ProgressCenter.complete_status, "Complete",test);
		  verifyElementText(ProgressCenter.report_name, Results_page_Export.Exportname,test);
		  prog.closeprogress();
		  export.export_results_xls();
		  prog.progress(prog.Progress_Center);
		  prog.generateReport();
		  verifyElementText(ProgressCenter.complete_status, "Complete",test);
		  verifyElementText(ProgressCenter.report_name, Results_page_Export.Exportname,test);
		  prog.closeprogress();
			
		  /*Sign_out sign_out=new Sign_out();
		  sign_out.signout();*/
		  

	} catch (Error e) {
		e.printStackTrace();
		ATUReports.add("WIN_TC_14()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} catch (Exception e) {
		e.printStackTrace();
		ATUReports.add("WIN_TC_14()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		reports.flush();
		//driver.close();

	}
}

}
