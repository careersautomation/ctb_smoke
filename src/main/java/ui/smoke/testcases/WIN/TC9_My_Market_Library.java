package ui.smoke.testcases.WIN;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementText;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyEquals;
import static driverfactory.Driver.getElementText;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.*;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC9_My_Market_Library extends InitTests {
SoftAssert softAssert = new SoftAssert();

	public TC9_My_Market_Library(String appName) {
	super(appName);
	}
	
	@Test()
    public void testSearch() throws Exception {
	  System.out.println("in test");
	  try
	  {
		  TC9_My_Market_Library m1=new TC9_My_Market_Library("WIN");
		  test = reports.createTest("TC9_My_Market_Library");
		  test.assignCategory("smoke");
		  //initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		  System.out.println("TC 9 My Market Library");
		  
		  /*Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		  
		  Select_Organization selectorg=new Select_Organization();
		  //assertTrue(Select_Organization.signOut_link.isDisplayed(),"WIN Application has reached Select Organization page",test);
		  selectorg.select_organization();
		  assertTrue(Select_Organization.Homepage.isDisplayed(),"My Homepage",test);*/
		  

		  navigateViaBreadCrumb navigatetohome= new navigateViaBreadCrumb();
		  navigatetohome.navigateBack(navigatetohome.home_Breadcrumb);
		  
		  My_Market_Library_search_page mylibrary= new My_Market_Library_search_page();
		  mylibrary.marketlibrarysearchpage();
		  assertTrue(My_Market_Library_search_page.MyLibrary.isDisplayed(),"My Market Library search page",test);
		  
		  My_Market_Library_details mymarketlibrary = new My_Market_Library_details();
		  mymarketlibrary.marketdetails();
		  assertTrue(My_Market_Library_details.popupclose.isDisplayed(),"Record details pop up accessed",test);
		  mymarketlibrary.recorddetailsclose();
		  mymarketlibrary.sourcedetils();
		  assertTrue(My_Market_Library_details.mmlpopupclose.isDisplayed(),"Source details pop up accessed",test);
		  mymarketlibrary.sourcedetilsclose();
		  
		 /* My_Market_Library_Edit_view editview= new My_Market_Library_Edit_view();
			editview.editviewbutton();
			//assertTrue(editview.editviewpage.isDisplayed(),"Edit view page",test);
			editview.editviewselections();		
			editview.edit_view_save();*/
		  
		  My_Market_Library_Results_page resultspage=new My_Market_Library_Results_page();
		  resultspage.libraryresultspage();
		  resultspage.libraryresultspagedetails();
		  assertTrue(My_Market_Library_details.popupclose.isDisplayed(),"Record details pop up accessed",test);
		  resultspage.recorddetailsclose();
		  resultspage.sourcedetils();
		  assertTrue(My_Market_Library_details.mmlpopupclose.isDisplayed(),"Source details pop up accessed",test);
		  resultspage.sourcedetilsclose();
		  resultspage.save();
		  verifyEquals(resultspage.resultsetname,getElementText(My_Market_Library_Results_page.title),test);
		  resultspage.resultset();
		  verifyEquals(resultspage.resultsetname,getElementText(My_Market_Library_Results_page.title),test);
		  
		  
		  Results_page_Print print=new Results_page_Print();
			print.print_results();
			ProgressCenter prog= new ProgressCenter();
			prog.progress(prog.Progress_Center);
			prog.generateReport();
			verifyElementText(ProgressCenter.complete_status, "Complete",test);
			for(int i=0; i<=2;i++){
				  try{
					  verifyElementTextContains(ProgressCenter.report_name, Results_page_Print.Reportname,test);
				     break;
				  }catch(Exception e){
					     System.out.println(e.getMessage());
				  }
			}

			prog.closeprogress();
			
			Results_page_Export export=new Results_page_Export();
			export.export_results();
			prog.progress(prog.Progress_Center);
			prog.generateReport();
			verifyElementText(ProgressCenter.report_name, Results_page_Export.Exportname,test);
			for(int i=0; i<=2;i++){
				  try{
					  verifyElementTextContains(ProgressCenter.report_name, Results_page_Export.Exportname,test);
				     break;
				  }catch(Exception e){
					     System.out.println(e.getMessage());
				  }
			}
			prog.closeprogress();
			
			export.export_results_xls();
			prog.progress(prog.Progress_Center);
			prog.generateReport();
			verifyElementText(ProgressCenter.complete_status, "Complete",test);
			for(int i=0; i<=2;i++){
				  try{
					  verifyElementTextContains(ProgressCenter.report_name, Results_page_Export.Exportname,test);
				     break;
				  }catch(Exception e){
					     System.out.println(e.getMessage());
				  }
			}
			prog.closeprogress();
			
			Sign_out sign_out=new Sign_out();
			sign_out.signout();
		  

	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_9()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_9()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		reports.flush();
		driver.close();

	}
	  
	}

}
