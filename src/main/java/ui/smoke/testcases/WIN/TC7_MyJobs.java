package ui.smoke.testcases.WIN;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.getElementText;
import static driverfactory.Driver.initWebDriver;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementText;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.*;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC7_MyJobs extends InitTests {
	SoftAssert softAssert = new SoftAssert();
	
	public TC7_MyJobs(String appName) {
		super(appName);
	}
	@Test()
    public void testSearch() throws Exception {
	  System.out.println("in test");
	  try
	  {
		  TC7_MyJobs a1=new TC7_MyJobs("WIN");
		  test = reports.createTest("TC7_MyJobs");
		  test.assignCategory("smoke");
		  //initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		  System.out.println("TC 7 My Jobs");
		  
		  /*Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		  
		  
		  Select_Organization selectorg=new Select_Organization();
		  //assertTrue(Select_Organization.signOut_link.isDisplayed(),"WIN Application has reached Select Organization page",test);
		  selectorg.select_organization();
		  
		  
		  assertTrue(Select_Organization.Homepage.isDisplayed(),"My Homepage",test);*/
		  
		  
		  navigateViaBreadCrumb navigatetohome= new navigateViaBreadCrumb();
		  navigatetohome.navigateBack(navigatetohome.home_Breadcrumb);
		  
		  
		  My_jobs_search_page searchpage=new My_jobs_search_page();
		  searchpage.jobssearchpage();
		  assertTrue(My_jobs_search_page.Myjobssearchpage.isDisplayed(),"My Jobs Search page",test);
		  
		  My_jobs myjob = new My_jobs();
		  myjob.job();
		  assertTrue(My_jobs.jobdetailsclose.isDisplayed(),"Accessed Job Details pop up",test);
		  myjob.jobclose();
		 
		  /*My_Jobs_Create_from_Reference_jobs createreferencejob=new My_Jobs_Create_from_Reference_jobs();
		  createreferencejob.create_from_referenece_jobs();
		  ProgressCenter prog= new ProgressCenter();
		  prog.progress(prog.Progress_Center);
		  verifyElementText(ProgressCenter.complete_status, "Complete",test);
		  verifyElementText(ProgressCenter.report_name,"Create Jobs Results",test);
		  prog.progress(prog.GlobalProgress_close);*/
		  
		  
		  ProgressCenter prog= new ProgressCenter();
		  My_Jobs_Edit_View myjobedit=new My_Jobs_Edit_View();
		  myjobedit.editviewbutton();
		  assertTrue(myjobedit.editviewpage.isDisplayed(),"Edit view page",test);
		  myjobedit.editviewselections();
		  myjobedit.edit_view_save();
		  
		  My_Jobs_Add_New_Job addjob= new My_Jobs_Add_New_Job();	  
		  addjob.add_job();
		  verifyElementText(My_Jobs_Add_New_Job.jobnameverify,addjob.Jobtitle,test);
		  addjob.add_job_close();
		  
		  MyJobs_Resultspage resultspage=new MyJobs_Resultspage();
		  resultspage.myjobs_results_page();
		  assertTrue(My_jobs.jobdetailsclose.isDisplayed(),"Accessed Job Details pop up",test);
		  resultspage.jobdetailsclose();
		  resultspage.benchmarkicon();
		  assertTrue(MyJobs_Resultspage.benchmarkclose.isDisplayed(),"Accessed Job Details pop up",test);
		  resultspage.benchmarkclose();
		  resultspage.save();
		  verifyEquals(MyJobs_Resultspage.resultsetname,getElementText(MyJobs_Resultspage.title),test);
		  resultspage.resultset();
		  verifyEquals(MyJobs_Resultspage.resultsetname,getElementText(MyJobs_Resultspage.title),test);
		  
		  My_Jobs_Row_Edit edit=new My_Jobs_Row_Edit();
		  edit.rowedit();
		  //assertTrue(My_Jobs_Row_Edit.confirmation_message.isDisplayed(),"Job Evaluated",test);
		  verifyElementTextContains(edit.roweditheader,"Evaluation - Row Edit",test);
		  edit.results_page();
		  
		  Results_page_Print print=new Results_page_Print();
		  print.print_results();
		  prog.progress(prog.Progress_Center);
		  prog.generateReport();
		  verifyElementText(ProgressCenter.complete_status, "Complete",test);
		  for(int i=0; i<=2;i++){
			  try{
				  verifyElementTextContains(ProgressCenter.report_name, Results_page_Print.Reportname,test);
			     break;
			  }catch(Exception e){
				     System.out.println(e.getMessage());
			  }
		}
		  prog.closeprogress();
			
		  Results_page_Export export=new Results_page_Export();
		  export.export_results();
		  prog.progress(prog.Progress_Center);
		  prog.generateReport();
		  verifyElementText(ProgressCenter.complete_status, "Complete",test);
		  for(int i=0; i<=2;i++){
			  try{
				  verifyElementTextContains(ProgressCenter.report_name, Results_page_Export.Exportname,test);
			     break;
			  }catch(Exception e){
				     System.out.println(e.getMessage());
			  }
		}
		  prog.closeprogress();
		  export.export_results_xls();
		  prog.progress(prog.Progress_Center);
		  prog.generateReport();
		  verifyElementText(ProgressCenter.complete_status, "Complete",test);
		   for(int i=0; i<=2;i++){
			  try{
				  verifyElementTextContains(ProgressCenter.report_name, Results_page_Export.Exportname,test);
			     break;
			  }catch(Exception e){
				     System.out.println(e.getMessage());
			  }
		}
		   prog.closeprogress();
		
		 /* Sign_out sign_out=new Sign_out();
		  sign_out.signout();*/
	  

	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_7()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_7()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		reports.flush();
		//driver.close();

	}
  }


}
