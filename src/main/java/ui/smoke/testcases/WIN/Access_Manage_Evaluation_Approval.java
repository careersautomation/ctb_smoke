package ui.smoke.testcases.WIN;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.initWebDriver;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.Tools;
import pages.WINPages.Login;
import pages.WINPages.Signout;
import pages.WINPages.SelectOrg;
import pages.WINPages.Access_Manage_Evaluation;
import utilities.InitTests;

public class Access_Manage_Evaluation_Approval extends InitTests{
	SoftAssert softAssert = new SoftAssert();
	public Access_Manage_Evaluation_Approval(String appName) {
		super(appName);
		}
	
	@Test(enabled=false)
    public void testSearch() throws Exception {
	  try
	  {
		  Access_Manage_Evaluation_Approval m1=new Access_Manage_Evaluation_Approval("WIN");
		  test = reports.createTest("Access Manage Evaluation Approval");
		  test.assignCategory("smoke");
		  initWebDriver(BASEURL,"CHROME", "", "", "local", test, "");
		  System.out.println("Access Manage Evaluation Approval");
		  
		  Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		  
		  SelectOrg so = new SelectOrg();
		  so.Organization();
		  //assertTrue(so.Homepage.isDisplayed(),"My Homepage",test);
		
		  Tools t = new Tools();
		  t.Administration();
		  assertTrue(t.Administration_Title.isDisplayed(),"Administration page is displayed",test);
		  
		  Access_Manage_Evaluation a = new Access_Manage_Evaluation();
		  a.Manage_Evaluation_Approvals();
		  assertTrue(t.Manage_Evaluation_Approvals_Title.isDisplayed(),"Manage Evaluation Approvals page is displayed",test);
		  
		  Signout s = new Signout();
		  s.Signout();
		  	  
	} catch (Error e) {
		e.printStackTrace();
		ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} catch (Exception e) {
		e.printStackTrace();
		ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		reports.flush();
		driver.close();
	}
  }
}