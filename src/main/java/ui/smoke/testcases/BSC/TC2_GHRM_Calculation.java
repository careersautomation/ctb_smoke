package ui.smoke.testcases.BSC;

import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.switchToWindow;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utilities.InitTests;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.BSC.BSC_AccountSelectionPage;
import pages.BSC.BSC_CalculationStartPage;
import pages.BSC.BSC_DashBoardPage;
import pages.BSC.BSC_InputsPage;
import pages.BSC.BSC_LoginPage;
import pages.BSC.BSC_PreviewPage;
import pages.BSC.BSC_ResultsPage;
import verify.SoftAssertions;
import verify.SoftAssertions.*;

public class TC2_GHRM_Calculation {
	
	
	@Parameters({"country" , "country1"})
	@Test(priority=2, enabled =true)
	public void GHRM_Calculation(String country,String country1) throws Exception {
		
		try {
			test = reports.createTest("Calulating the GHRM Calculation");
			test.assignCategory("smoke");
			
			/*
		    BSC_LoginPage Loginpage = new BSC_LoginPage();
			Loginpage.BSC_Login();
			waitForElementToDisplay(BSC_LoginPage.SigninText);
			Loginpage.Signin(, pwd);
			Thread.sleep(5000);
			*/
			
			BSC_AccountSelectionPage AccSel= new BSC_AccountSelectionPage();
			AccSel.GHRMAccount();
			Thread.sleep(4000);
			waitForElementToDisplay(BSC_AccountSelectionPage.GHRMClient);
			BSC_DashBoardPage SheetSel = new BSC_DashBoardPage();
			SheetSel.BalanceSheetSelection();
			switchToWindow("Welcome- Mercer");
			BSC_CalculationStartPage calcstart = new BSC_CalculationStartPage();
			calcstart.GHRM_StartCalculation(country,country1);
			BSC_InputsPage input=new BSC_InputsPage();
			input.GHRM_InputPageselection();
			BSC_PreviewPage Preview=new BSC_PreviewPage();
			Preview.previewContinue();
			BSC_ResultsPage Results=new BSC_ResultsPage();
			String s = Results.TotalHomeCountryCompensationValue.getText();
			assertTrue(Results.checkValues(s), "value greater than 0 validation",test);
			String s1 = Results.HostEstimatedPersonalIncomeTaxHomeValue.getText();
			assertTrue(Results.checkValues(s1), "value greater than 0 validation",test);
			String s2=Results.EstimatedHomeSocialSecurityHomeValue.getText();
			assertTrue(Results.checkValues(s2), "value greater than 0 validation",test);
			Results.ExcelDownload();
			//Results.GetExcelNames();
			Results.PDFDownload();
			Results.ReturnMobility();
			//Loginpage.BSC_logout();
			SheetSel.SwitchClient();
			
			
			
			
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("GHRM_Calculation is failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("GHRM_Calculation is failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
		}
			
		/*	finally
			{
				killBrowserExe(BROWSER_TYPE);
				reports.flush();
				driver.close();
			} */
	}
}
