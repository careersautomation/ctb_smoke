package ui.smoke.testcases.BSC;

import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static driverfactory.Driver.*;
import static utilities.DataRepository.*;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.thoughtworks.selenium.webdriven.commands.WaitForPageToLoad;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import verify.SoftAssertions;
import pages.BSC.BSC_AccountSelectionPage;
import pages.BSC.BSC_CalculationStartPage;
import pages.BSC.BSC_DashBoardPage;
import pages.BSC.BSC_InputsPage;
import pages.BSC.BSC_LoginPage;
import pages.BSC.BSC_PreviewPage;
import pages.BSC.BSC_ResultsPage;
import utilities.InitTests;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.*;


public class TC1_LoginToBSC extends InitTests{
	
	public TC1_LoginToBSC(String appName) {
		
		super(appName);
	}
	/*
	@BeforeMethod
	public void initializeBrowser() throws Exception {
		test = reports.createTest("InitializeBrowser");
		test.assignCategory("smoke");
		TestBSCCalculation calc= new TestBSCCalculation("BSC");
		initWebDriver(BASEURL, "CHROME", "latest","","local", test, "");
	} */
	
	@Test(priority = 1, enabled = true)
	public void BSC_loginprocess() throws Exception{
		try {
			System.out.println("test started");
			test = reports.createTest("Logging To Balance Sheet calculator");
			test.assignCategory("smoke");
			
			TC1_LoginToBSC calc= new TC1_LoginToBSC("BSC");
			initWebDriver(BASEURL, "CHROME", "latest","","local", test, "");
			System.out.println("BaseURL is: " + BASEURL);
			
			
			BSC_LoginPage Loginpage = new BSC_LoginPage();
			Loginpage.BSC_Login();
			waitForElementToDisplay(BSC_LoginPage.SigninText);
			verifyElementTextContains(BSC_LoginPage.SigninText, "Signin", test);
			Loginpage.Signin(USERNAME, PASSWORD);
			Thread.sleep(6000);
			//verifyElementLink(BSC_AccountSelectionPage.GHRM_Mercer, "MERCER", test);
			
			
		

	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("BSC_loginprocess is failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		//softAssert.assertAll();

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("BSC_loginprocess is failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		//softAssert.assertAll();
	}
		
		/*finally
		{
			killBrowserExe(BROWSER_TYPE);
			reports.flush();
			driver.close();
		} */
	}
	/*
	@Parameters({"country" , "country1"})
	@Test(priority=2, enabled =true)
	public void GHRM_Calculation(String country,String country1) throws Exception {
		
		try {
		    BSC_LoginPage Loginpage = new BSC_LoginPage();
			Loginpage.BSC_Login();
			waitForElementToDisplay(BSC_LoginPage.SigninText);
			Loginpage.Signin(USERNAME, PASSWORD);
			Thread.sleep(5000);
			BSC_AccountSelectionPage AccSel= new BSC_AccountSelectionPage();
			AccSel.GHRMAccount();
			Thread.sleep(4000);
			waitForElementToDisplay(BSC_AccountSelectionPage.GHRMClient);
			BSC_DashBoardPage SheetSel = new BSC_DashBoardPage();
			SheetSel.BalanceSheetSelection();
			switchToWindow("Welcome- Mercer");
			BSC_CalculationStartPage calcstart = new BSC_CalculationStartPage();
			calcstart.GHRM_StartCalculation(country,country1);
			BSC_InputsPage input=new BSC_InputsPage();
			input.GHRM_InputPageselection();
			BSC_PreviewPage Preview=new BSC_PreviewPage();
			Preview.previewContinue();
			BSC_ResultsPage Results=new BSC_ResultsPage();
			String s = Results.TotalHomeCountryCompensationValue.getText();
			assertTrue(Results.checkValues(s), "value greater than 0 validation",test);
			String s1 = Results.HostEstimatedPersonalIncomeTaxHomeValue.getText();
			assertTrue(Results.checkValues(s1), "value greater than 0 validation",test);
			String s2=Results.EstimatedHomeSocialSecurityHomeValue.getText();
			assertTrue(Results.checkValues(s2), "value greater than 0 validation",test);
			Results.ExcelDownload();
			//Results.GetExcelNames();
			Results.PDFDownload();
			Results.ReturnMobility();
			Loginpage.BSC_logout();
			
			
			
			
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("GHRM_Calculation()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("GHRM_Calculation()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
			
			finally
			{
				killBrowserExe(BROWSER_TYPE);
				reports.flush();
				driver.close();
			} 
	} 
	
	@Parameters({"country2" , "country3"})
	@Test(priority=3 , enabled=true)
	public void ICS_Calculation(String country2, String country3) throws Exception{
		
		try {
			
			BSC_LoginPage Loginpage = new BSC_LoginPage();
			Loginpage.BSC_Login();
			waitForElementToDisplay(BSC_LoginPage.SigninText);
			Loginpage.Signin(USERNAME, PASSWORD);
			Thread.sleep(4000);
			BSC_AccountSelectionPage AccSel= new BSC_AccountSelectionPage();
			AccSel.ICSAccount();
			Thread.sleep(4000);
			waitForElementToDisplay(BSC_AccountSelectionPage.Account11325);
			verifyElementHyperLink(BSC_AccountSelectionPage.Account11325,test);
			BSC_DashBoardPage SheetSel = new BSC_DashBoardPage();
			SheetSel.BalanceSheetSelection();
			switchToWindow("Welcome- Mercer");
			BSC_CalculationStartPage calcstart = new BSC_CalculationStartPage();
			calcstart.ICS_StartCalculation(country2,country3);
			BSC_InputsPage input=new BSC_InputsPage();
			input.ICS_InputPageselection();
			BSC_PreviewPage Preview=new BSC_PreviewPage();
			Preview.previewContinue();
			Thread.sleep(5000);
			BSC_ResultsPage Results=new BSC_ResultsPage();
			Thread.sleep(2000);
			String s = Results.ICS_CountryTax.getText();
			assertTrue(Results.checkValues(s), "value greater than 0 validation",test);
			Thread.sleep(3000);
			String s1 = Results.ICS_SocialSecurity.getText();
			
			assertTrue(Results.checkValues(s1), "value greater than 0 validation",test);
			Thread.sleep(3000);
			String s2=Results.ICS_TotNetCompensation.getText();
			
			assertTrue(Results.checkValues(s2), "value greater than 0 validation",test);
			Thread.sleep(5000);
			Results.ExcelDownload();
			Thread.sleep(3000);
			Results.GetExcelNames();
			
			Results.PDFDownload();
			Results.ReturnMobility();
			Loginpage.BSC_logout();
			
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("ICS_Calculation()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("ICS_Calculation()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}
			
		/*	finally
			{
				
				reports.flush();
				driver.close();
			} 
	} 
	*/
	
	 
		
	
	@AfterSuite
	public void killDriver() {
		reports.flush();
		driver.close();
		killBrowserExe(BROWSER_TYPE);
		
	}
	
	
	
}

