package ui.smoke.testcases.BSC;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.switchToWindow;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementHyperLink;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.BSC.BSC_AccountSelectionPage;
import pages.BSC.BSC_CalculationStartPage;
import pages.BSC.BSC_DashBoardPage;
import pages.BSC.BSC_InputsPage;
import pages.BSC.BSC_LoginPage;
import pages.BSC.BSC_PreviewPage;
import pages.BSC.BSC_ResultsPage;
import verify.SoftAssertions;

public class TC3_ICS_Calculation {
	@Parameters({"country2" , "country3"})
	@Test(priority=3 , enabled=true)
	public void ICS_Calculation(String country2, String country3) throws Exception{
		
		try {
			test = reports.createTest("Calulating the ICS Calculation");
			test.assignCategory("smoke");
			/*
			BSC_LoginPage Loginpage = new BSC_LoginPage();
			Loginpage.BSC_Login();
			waitForElementToDisplay(BSC_LoginPage.SigninText);
			Loginpage.Signin(USERNAME, PASSWORD);
			Thread.sleep(4000);
			*/
			
			BSC_AccountSelectionPage AccSel= new BSC_AccountSelectionPage();
			AccSel.ICSAccount();
			BSC_LoginPage Loginpage = new BSC_LoginPage();
			Thread.sleep(4000);
			
			waitForElementToDisplay(BSC_AccountSelectionPage.Account11325);
			verifyElementHyperLink(BSC_AccountSelectionPage.Account11325,test);
			BSC_DashBoardPage SheetSel = new BSC_DashBoardPage();
			SheetSel.BalanceSheetSelection();
			switchToWindow("Welcome- Mercer");
			BSC_CalculationStartPage calcstart = new BSC_CalculationStartPage();
			calcstart.ICS_StartCalculation(country2,country3);
			BSC_InputsPage input=new BSC_InputsPage();
			input.ICS_InputPageselection();
			BSC_PreviewPage Preview=new BSC_PreviewPage();
			Preview.previewContinue();
			Thread.sleep(5000);
			BSC_ResultsPage Results=new BSC_ResultsPage();
			Thread.sleep(2000);
			String s = Results.ICS_CountryTax.getText();
			
			assertTrue(Results.checkValues(s), "value greater than 0 validation",test);
			Thread.sleep(3000);
			String s1 = Results.ICS_SocialSecurity.getText();
			
			assertTrue(Results.checkValues(s1), "value greater than 0 validation",test);
			Thread.sleep(3000);
			String s2=Results.ICS_TotNetCompensation.getText();
			
			assertTrue(Results.checkValues(s2), "value greater than 0 validation",test);
			Thread.sleep(5000);
			Results.ExcelDownload();
			Thread.sleep(3000);
			Results.GetExcelNames();
			
			Results.PDFDownload();
			Results.ReturnMobility();
			Loginpage.BSC_logout();
			
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("ICS_Calculation is failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("ICS_Calculation is failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
		}
}
	
}
