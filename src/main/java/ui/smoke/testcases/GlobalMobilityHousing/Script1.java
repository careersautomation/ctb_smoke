package ui.smoke.testcases.GlobalMobilityHousing;
import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.*;

import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import com.thoughtworks.selenium.webdriven.commands.IsElementPresent;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.GlobalMobilityHousing_Pages.Script1_pages;
import utilities.InitTests;
import verify.SoftAssertions;

public class Script1    extends InitTests {
	
	public Script1  (String appName) {
		super(appName);
		
	}
	
	@Test(enabled = true)
	public void login() throws Exception{
		try {
			test = reports.createTest("Logging in to Mobility Blue application");
			test.assignCategory("smoke");
			Script1 login = new Script1  ("Mobility_");
			initWebDriver(BASEURL,"CHROME","","","local",test,"");
			
			Script1_pages loginPage = new Script1_pages(); 
			
			loginPage.Mercer_Mobility_folder_Click();	
			loginPage.login(USERNAME);
			loginPage.Shared_Reports_click();
			loginPage.Housing_Folder_Click();
			loginPage.Housing_application_landingPage_Click();
			
			loginPage.Location_Dropdown_Click();
			
			loginPage.SurveyDate_Dropdown_Click();
			loginPage.Policy_Dropdown_Click();
			loginPage.View_Button_Click();
			
			Boolean Verify= Driver.isElementExisting(driver,loginPage.Verify_Mercer_Standard_policy_page, 30);
			SoftAssertions.assertTrue(Verify, "Verify_Mercer_Standard_policy_page",test);
			
			loginPage.Apartment_checkbox_Click();
			verifyElementTextContains(loginPage.Verify_Apartment_UnCheck,"12,000",test);
			
			loginPage.MAP_tab_Click();
			verifyElementTextContains(loginPage.Verify_MAP_tab,"Manage My Pushpins",test);		
			
			loginPage.GeneralInfo_tab_Click();
			verifyElementTextContains(loginPage.Verify_GeneralInfo_tab,"General Information",test);
			
			
			loginPage.Home_tab_Click();
			verifyElementTextContains(loginPage.Verify_HOME_tab,"   View",test);
	
			
			loginPage.logout_Click();
			verifyElementTextContains(loginPage.Verify_logout,"Thank you for using MicroStrategy Web. You have been logged out.",test);
			
			
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}

}
	@AfterTest
	public void tearDown() throws InterruptedException
	{
		reports.flush();
		//driver.quit();
		Thread.sleep(5000);
	    
	}



}



