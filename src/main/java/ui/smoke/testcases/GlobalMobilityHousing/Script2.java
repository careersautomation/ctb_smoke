    package ui.smoke.testcases.GlobalMobilityHousing;
	import static driverfactory.Driver.*;
    import static utilities.MyExtentReports.reports;
	import static utilities.MyExtentReports.test;
	import static verify.SoftAssertions.*;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
    import org.testng.annotations.Test;
	import com.thoughtworks.selenium.webdriven.commands.IsElementPresent;
	import atu.testng.reports.ATUReports;
	import atu.testng.reports.logging.LogAs;
	import atu.testng.selenium.reports.CaptureScreen;
	import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
	import driverfactory.Driver;
	import pages.GlobalMobilityHousing_Pages.Script1_pages;
    import pages.GlobalMobilityHousing_Pages.Script2_pages;
    import utilities.InitTests;
	import verify.SoftAssertions;

	public class Script2  extends InitTests {
		
		public Script2  (String appName) {
			super(appName);
			
		}
		
		@Test(enabled = true)
		public void login() throws Exception{
			try {
				test = reports.createTest("Logging in to eIPE Blue application");
				test.assignCategory("smoke");
				Script2 login = new Script2  ("Mobility_");
				initWebDriver(BASEURL,"CHROME","","","local",test,"");
				Script1_pages loginPage  = new Script1_pages();
				Script2_pages Mobility  = new Script2_pages();
				loginPage.Mercer_Mobility_folder_Click();
				loginPage.login(USERNAME);
				loginPage.Shared_Reports_click();
				loginPage.Housing_Folder_Click();
				loginPage.Housing_application_landingPage_Click();
				
				
				Mobility.Location_Dropdown_Click();
				Mobility.SurveyDate_Dropdown_Click();
				Mobility.Policy_Dropdown_Click2();
				Mobility.View_Button_Click();
				
				verifyElementTextContains(Mobility.Verify_Intermediate_Page,"Calculate Housing Allowance",test);
	
				
			    Mobility.Location_Dropdown_Click2();
				loginPage.View_Button_Click();
				
				Boolean VerifyAllowancePage= Driver.isElementExisting(driver,Mobility.Verify_AdvancedAllowance_Page, 10);
				SoftAssertions.assertTrue(VerifyAllowancePage, "Verify_Allowance_Page",test);
				
				
				
				Mobility.ChangeCurrency_dropdown_Click();
				verifyElementTextContains(Mobility.Verify_Result, "1,000",test);
				
				
				Mobility.dropdown_excel_pdf_click();
				
				Mobility.dropdown_excel_pdf_click2();
				Boolean Verify_Filedownload= Mobility.CheckFile();
				SoftAssertions.assertTrue(Verify_Filedownload,"Downloaded Successfully",test);
				
				
				
				Mobility.MAP_tab_Click();
				verifyElementTextContains(Mobility.Verify_MAP_tab,"Manage My Pushpins",test);
				
				loginPage.GeneralInfo_tab_Click();
				verifyElementTextContains(loginPage.Verify_GeneralInfo_tab,"General Information",test);
				
				Mobility.SampleAccomodation_tab_Click();
				
				loginPage.Home_tab_Click();
				verifyElementTextContains(loginPage.Verify_HOME_tab,"   View",test);
				
				loginPage.logout_Click();
				verifyElementTextContains(loginPage.Verify_logout,"Thank you for using MicroStrategy Web. You have been logged out.",test);
				
							
			} catch (Error e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			} catch (Exception e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED,
						new CaptureScreen(ScreenshotOf.DESKTOP));

			}

	}

		@AfterSuite
		public void tearDown()
		{
			reports.flush();
			driver.close();
		}
	}

	

