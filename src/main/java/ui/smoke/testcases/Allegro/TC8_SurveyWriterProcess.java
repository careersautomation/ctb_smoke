package ui.smoke.testcases.Allegro;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.waitForPageLoad;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.verifyElementTextContains;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

import pages.Allegro.AllegroSurveyWriting;
import verify.SoftAssertions;

public class TC8_SurveyWriterProcess {

	@Test(enabled =true, priority=8 )
	public void SurveyWriterProcess() throws Exception {
		
		try {
			//TC3_SurveyWriting a1=new TC3_SurveyWriting("");
			test = reports.createTest("SurveyWriter_functionalities");
			test.assignCategory("smoke");
			
			AllegroSurveyWriting Survey= new AllegroSurveyWriting();
			Survey.SelectSurvey();
			waitForPageLoad(driver);
			verifyElementTextContains(Survey.NewSurvey_Button, "New Survey", test);
			Survey.SelectNewSurvey();
			waitForPageLoad(driver);
			verifyElementTextContains(Survey.SurveyName_Lable, "Survey Name", test);
			Survey.DetailsPage();
			verifyElementTextContains(Survey.WeWanttoHearFromYou_lable, "We Want to Hear From You", test);
			Survey.QuestionPage();
			verifyElementTextContains(Survey.YourAudience_lable, "YourAudience", test);
			Survey.AudiencePage();
			verifyElementTextContains(Survey.SchedulingEmails_lable, "Scheduling / Emails", test);
			Survey.EmailsPage();
			verifyElementTextContains(Survey.Resultsbasedon_lable, "Results based on", test);
			Survey.ReportsPage();
			verifyElementTextContains(Survey.SurveyDetails_lable, "Survey Details", test);
			Survey.ReviewPage();
			
			
			
			
}catch (Error e) {
	e.printStackTrace();
	SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
	ATUReports.add("EmployeeList is failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	

} catch (Exception e) {
	e.printStackTrace();
	SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
	ATUReports.add("EmployeeList is failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	
	}
	
	}

}
