package ui.smoke.testcases.Allegro;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.waitForPageLoad;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.verifyElementTextContains;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.Allegro.AllegroHeaderPage;
import pages.Allegro.AllegroSurveyWriting;
import verify.SoftAssertions;

public class TC9_LogoutFromAllegro {
	
	@Test(enabled =true, priority=9 )
	public void AllegroLogout() throws Exception {
		
		try {
			//TC3_SurveyWriting a1=new TC3_SurveyWriting("");
			test = reports.createTest("LogOut_functionalities");
			test.assignCategory("smoke");
			
			AllegroHeaderPage Header =new AllegroHeaderPage();
			Header.Logout();
			
			
			
}catch (Error e) {
	e.printStackTrace();
	SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
	ATUReports.add("Logout is failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	

} catch (Exception e) {
	e.printStackTrace();
	SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
	ATUReports.add("Logout is failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	
	}
	
	}

}
