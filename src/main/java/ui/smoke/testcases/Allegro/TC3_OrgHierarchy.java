package ui.smoke.testcases.Allegro;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.verifyElementTextContains;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.Allegro.AllegroHeaderPage;
import pages.Allegro.AllegroOrgHierarchyPage;
import verify.SoftAssertions;

public class TC3_OrgHierarchy {
	
	@Test(enabled =true, priority=3 )
	public void OrgHierarchy() throws Exception {
		
		try {
			test = reports.createTest("OrgHierarchy_functionalities");
			test.assignCategory("smoke");
			
			AllegroHeaderPage Org= new AllegroHeaderPage();
			Org.OrgHierarchyPage();
			AllegroOrgHierarchyPage OrgHie= new AllegroOrgHierarchyPage();
			OrgHie.SelectOpenHierarchy();
			OrgHie.OpenHieEmployees();
			
			
			
			
			
}catch (Error e) {
	e.printStackTrace();
	SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
	ATUReports.add("OrgHierarchy is failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	

} catch (Exception e) {
	e.printStackTrace();
	SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
	ATUReports.add("OrgHierarchy is failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	
	}
	
	}

}
