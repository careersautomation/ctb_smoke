package ui.smoke.testcases.Allegro;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.*;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import verify.SoftAssertions;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.Allegro.AllegroDashboardPage;
import pages.Allegro.AllegroHeaderPage;
import pages.Allegro.AllegroLoginPage;


import utilities.InitTests;



public class TC1_LoginToAllegro extends InitTests {
	
public TC1_LoginToAllegro(String appName) {
		
		super(appName);
	}
	/*
	@BeforeMethod
	public void initializeBrowser() throws Exception {
	test = reports.startTest("Initialize Browser");
	test.assignCategory("smoke");
	AllegroSmoke smoke= new AllegroSmoke("Allegro_");
	initWebDriver(BASEURL, BROWSER_TYPE, "", "", EXECUTION_ENV);
	}
    */

	@Test(enabled = true , priority = 1)
	public void SuperUserLogin() throws Exception {
		try {
			System.out.println("test started");
			test = reports.createTest("Logging To Allegro");
			test.assignCategory("smoke");
			
			//TC1_LoginToAllegro calc= new TC1_LoginToAllegro("Allegro");
			initWebDriver(BASEURL, "CHROME", "latest","","local", test, "");
			System.out.println("BaseURL is: " + BASEURL);
			
			TC1_LoginToAllegro a1=new TC1_LoginToAllegro("Allegro"); 
			AllegroLoginPage loginpage = new AllegroLoginPage();
			loginpage.loginAllegroNewSite();
			
			AllegroDashboardPage home = new AllegroDashboardPage();
			waitForElementToDisplay(AllegroDashboardPage.Templink);
			Thread.sleep(3000);
			verifyElementTextContains(AllegroDashboardPage.Templink, " Temporary Link To Survey Wizard ", test);
			
			AllegroHeaderPage header = new AllegroHeaderPage();
			header.ClientsTab();
			Thread.sleep(3000);
			home.SearchClients("APM");
			Thread.sleep(4000);
			verifyElementTextContains(AllegroDashboardPage.ClientPropertiesTab, " Properties",test);
			verifyElementHyperLink(AllegroHeaderPage.Admin,test);
			verifyElementHyperLink(AllegroHeaderPage.Surveys,test);
			verifyElementHyperLink(AllegroHeaderPage.Results,test);

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("LoginToAllegro is failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("LoginToAllegro is failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			
		} 

		}

	@AfterSuite
	public void killDriver() {
		reports.flush();
		driver.close();
		killBrowserExe(BROWSER_TYPE);
		
	}
}
