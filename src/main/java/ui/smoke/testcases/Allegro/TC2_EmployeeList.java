package ui.smoke.testcases.Allegro;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static driverfactory.Driver.*;
import static verify.SoftAssertions.*;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.Allegro.AllegroEmployeeListPage;
import pages.Allegro.AllegroHeaderPage;
import verify.SoftAssertions;

public class TC2_EmployeeList {

	@Test(enabled =true, priority=2 )
	public void EmployeeList() throws Exception {
		
		try {
			test = reports.createTest("EmployeeList_functionalities");
			test.assignCategory("smoke");
			
			AllegroHeaderPage Header=new AllegroHeaderPage();
			Header.EmployeeListPage();
			waitForElementToDisplay(AllegroEmployeeListPage.EmpListExternalIDText);
			//verifyElementTextContains(AllegroEmployeeListPage.EmpListExternalIDText, "External ID", test);
			
			
			
			
}catch (Error e) {
	e.printStackTrace();
	SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
	ATUReports.add("EmployeeList is failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	

} catch (Exception e) {
	e.printStackTrace();
	SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
	ATUReports.add("EmployeeList is failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	
	}
	
	}
}
