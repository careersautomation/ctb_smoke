package ui.smoke.testcases.Allegro;

import org.testng.annotations.Test;

import com.thoughtworks.selenium.webdriven.commands.WaitForPageToLoad;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.waitForPageLoad;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.verifyElementTextContains;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.Allegro.AllegroHeaderPage;
import pages.Allegro.AllegroReportChaptersPage;
import pages.Allegro.AllegroSurveyPage;
import verify.SoftAssertions;


public class TC6_ReportChapters {
	
	@Test(enabled =true, priority=6 )
	public void Reportchapters() throws Exception {
			
			try {
				test = reports.createTest("ReportChapters_functionalities");
				test.assignCategory("smoke");
				
				AllegroReportChaptersPage RptChap =new AllegroReportChaptersPage();
				waitForPageLoad();
				Thread.sleep(5000);
				RptChap.ChapterSelection(AllegroReportChaptersPage.ItemDetailChapter);
				Thread.sleep(5000);
				waitForPageLoad();
				RptChap.ChapterSelection(AllegroReportChaptersPage.Allitemchapter);
				Thread.sleep(5000);
				waitForPageLoad();
				AllegroHeaderPage Header=new AllegroHeaderPage();
				Header.ResultsTab();
				
				
				RptChap.ChapterSelection(AllegroReportChaptersPage.ActionGrid);
				Thread.sleep(5000);
				waitForPageLoad();
				RptChap.ChapterSelection(AllegroReportChaptersPage.HeatMapChapter);
				Thread.sleep(5000);
				
	}catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("ChapterSelection is failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("ChapterSelection is failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		
		}
		
		}

}
