package ui.smoke.testcases.Allegro;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.Allegro.AllegroHeaderPage;
import pages.Allegro.AllegroReportChaptersPage;
import pages.Allegro.AllegroReportLibraryDetailPage;
import pages.Allegro.AllegroResultsPage;
import verify.SoftAssertions;

public class TC7_ReportDownloads {
	
	
	@Test(enabled =true, priority=7 )
	public void ReportDownload() throws Exception {
			
			try {
				test = reports.createTest("Reportdownload_functionalities");
				test.assignCategory("smoke");
				
				AllegroResultsPage ReportLib=new AllegroResultsPage();
				ReportLib.ResultLinkSelection();
				
				AllegroReportLibraryDetailPage Rptchpter=new AllegroReportLibraryDetailPage();
				Rptchpter.ReportsDownloadSelection(AllegroReportLibraryDetailPage.FirstReportPPT);
				Thread.sleep(2000);
				Rptchpter.ReportsDownloadLanguageSelection(AllegroReportLibraryDetailPage.FirstPPTReportEnglish);
				Thread.sleep(5000);
				Rptchpter.ReportsDownloadSelection(AllegroReportLibraryDetailPage.FirstReportXLS);
				Thread.sleep(3000);
				Rptchpter.ReportsDownloadLanguageSelection(AllegroReportLibraryDetailPage.FirstXLSReportEnglish);
				Thread.sleep(3000);
				
				
				
				
				
				
	}catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("ReportDownload is failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("ReportDownload is failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		
		}
		
		}

}
