package ui.smoke.testcases.Allegro;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.*;
import static driverfactory.Driver.getScreenPath;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.*;

import org.testng.annotations.Test;

import com.thoughtworks.selenium.webdriven.commands.WaitForPageToLoad;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.Allegro.AllegroHeaderPage;
import pages.Allegro.AllegroOrgHierarchyPage;
import pages.Allegro.AllegroReportChaptersPage;
import pages.Allegro.AllegroReportLibraryDetailPage;
import pages.Allegro.AllegroResultsPage;
import pages.Allegro.AllegroSurveyPage;
import verify.SoftAssertions;

public class TC5_Results {
	
	@Test(enabled =true, priority=5 )
	public void ReportLibrary() throws Exception {
			
			try {
				test = reports.createTest("ReportLibrary_functionalities");
				test.assignCategory("smoke");
				
				AllegroHeaderPage survey= new AllegroHeaderPage();
				survey.ResultsTab();
			//	verifyElementText(AllegroResultsPage.ResultsPageTitle, "Results", test);
				
				AllegroResultsPage ReportLib=new AllegroResultsPage();
				ReportLib.ResultLinkSelection();
				Thread.sleep(3000);
				waitForPageLoad(driver);
				AllegroReportLibraryDetailPage ReportLibDetail=new AllegroReportLibraryDetailPage();
				//String ReportFirstTile = ReportLib.FirstTileTitle.getText();
			//	String ReportDetailTitle= ReportLibDetail.ReportDetailTitle.getText();
				
				//verifyEquals(ReportFirstTile,ReportDetailTitle, test);
				
				ReportLibDetail.NavigateToReportDashBoard();
				waitForPageLoad(driver);
				//verifyElementTextContains(AllegroReportChaptersPage.DashboardChapterText, "Dashboard", test);
				
				
				
				
				
				
	}catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("SurveyLibrary is failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("SurveyLibrary is failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		
		}
		
		}

	}


