package ui.smoke.testcases.Allegro;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.waitForPageLoad;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.verifyElementTextContains;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.Allegro.AllegroHeaderPage;
import pages.Allegro.AllegroOrgHierarchyPage;
import pages.Allegro.AllegroSurveyPage;
import verify.SoftAssertions;

public class TC4_SurveyPage {
	
	@Test(enabled =true, priority=4 )
public void SurveyLibrary() throws Exception {
		
		try {
			test = reports.createTest("SurveyLibrary_functionalities");
			test.assignCategory("smoke");
			
			AllegroHeaderPage survey= new AllegroHeaderPage();
			survey.SurveysTab();
			
			AllegroSurveyPage SurveyLib= new AllegroSurveyPage();
			SurveyLib.ResponseRateReportView();
			waitForPageLoad();
			verifyElementTextContains(AllegroSurveyPage.ResponseRateText, "Response Rate Report", test);
			Thread.sleep(6000);
			survey.SurveysTab();
			
			
			
}catch (Error e) {
	e.printStackTrace();
	SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
	ATUReports.add("SurveyLibrary is failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	

} catch (Exception e) {
	e.printStackTrace();
	SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
	ATUReports.add("SurveyLibrary is failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	
	}
	
	}

}
