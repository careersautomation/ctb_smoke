package ui.smoke.testcases.careerView;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.killBrowserExe;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.careerView_Pages.LoginPage;
import pages.careerView_Pages.MyProfileSideMenu;
import pages.careerView_Pages.OverviewPage;
import utilities.InitTests;
import utilities.SikuliActions;
import verify.SoftAssertions;

public class TC1_LoginToCareerView extends InitTests {
	
	public static String  dirPath = dir_path;
	
	public TC1_LoginToCareerView(String appName) {
		super(appName);
	}
	
	@Test(enabled = true, priority = 1)
	public void loginToCareerView() throws Exception {
		try {
		test = reports.createTest("Logging in to career view application");
		test.assignCategory("smoke");
		
		TC1_LoginToCareerView careerViewTest = new TC1_LoginToCareerView("careerView");

		System.out.println("Logging in to application");
//		String url,String browserType,String browserVersion,String platform,String executionEnv,ExtentTest test,String nodeUrl
		initWebDriver(BASEURL,"CHROME", "", "", "local", test, "");
		System.out.println("BaseURL is: " + BASEURL);

		LoginPage loginPage = new LoginPage();
		loginPage.loginToCareerView(USERNAME, PASSWORD);
		
		OverviewPage myProfilePopUp = new OverviewPage();
		SoftAssertions.assertTrue(isElementExisting(driver, OverviewPage.myProfile_button, 5),"User logged in to application", test);
		
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true, priority = 2)
	public void checkMyProfilePopup() throws IOException  {
		try {
		test = reports.createTest("verifying my profile pop up");
		test.assignCategory("smoke");
		
		OverviewPage myProfilePopUp = new OverviewPage();
		SoftAssertions.assertTrue(isElementExisting(driver, OverviewPage.myProfile_button, 5),"User logged in to application", test);
		
		myProfilePopUp.goTomyProfile();
		SoftAssertions.assertTrue(isElementExisting(driver, OverviewPage.myProfile_header, 5),"User navigated to my profile page in the pop up", test);
		
		MyProfileSideMenu myProfileSideMenu = new MyProfileSideMenu();
		myProfileSideMenu.closePopup();
		SoftAssertions.assertFalse(isElementExisting(driver, OverviewPage.profile_modal, 5),"User closed the pop up successfully", test);
		
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Failed while navigating to my profile pop up", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed while navigating to my profile pop up", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}

	
	
	@AfterSuite
	public void killDriver() {
		reports.flush();
		driver.close();
		killBrowserExe(BROWSER_TYPE);
		
	}
}
