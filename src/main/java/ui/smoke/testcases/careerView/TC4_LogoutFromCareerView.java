package ui.smoke.testcases.careerView;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import java.io.IOException;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.careerView_Pages.HeaderPage;
import pages.careerView_Pages.LoginPage;
import verify.SoftAssertions;

public class TC4_LogoutFromCareerView {
	@Test(enabled = false)
	public void TestForRequestAssessment() throws IOException {
		try {
		test = reports.createTest("Logging out from carrer view");
		test.assignCategory("smoke");
		
		HeaderPage headerPage = new HeaderPage();
		headerPage.logout();
		
		LoginPage loginPage = new LoginPage();
		SoftAssertions.assertTrue(isElementExisting(driver, LoginPage.username_input, 5),"User logged out successfully", test);
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to logout from career view", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed to logout from career view", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}

}
