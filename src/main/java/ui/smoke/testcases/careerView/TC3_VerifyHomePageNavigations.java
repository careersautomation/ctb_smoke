package ui.smoke.testcases.careerView;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.careerView_Pages.ComparePopUpPage;
import pages.careerView_Pages.HeaderPage;
import pages.careerView_Pages.HomePage;
import pages.careerView_Pages.LocateMePage;
import pages.careerView_Pages.NavigatePopUpPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC3_VerifyHomePageNavigations extends InitTests {
	
	@Test(enabled = false, priority = 1)
	public void verifyLocateMe() throws Exception {
		try {
		test = reports.createTest("Verifying Locate me functionality");
		test.assignCategory("smoke");
		
		HomePage homePage = new HomePage();
		homePage.navigateToLocateMe();
		
		LocateMePage locateMePage = new LocateMePage();
		SoftAssertions.assertTrue(isElementExisting(driver, LocateMePage.ownRole_hightlighted, 5),"the role of user is highlighted", test);
		
		
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("failed to locate the user role", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("failed to locate the user role", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = false, priority = 2)
	public void navigateFromHeader() throws Exception {
		try {
		test = reports.createTest("Verifying navigate to next functionality");
		test.assignCategory("smoke");
		
		LocateMePage locateMePage = new LocateMePage();
		SoftAssertions.assertTrue(isElementExisting(driver, LocateMePage.ownRole_hightlighted, 5),"User is on locate me page", test);
		
		HeaderPage headerPage = new HeaderPage();
		headerPage.navigateViaHeader(HeaderPage.navigate_button);
		
		NavigatePopUpPage navigatePopUp = new NavigatePopUpPage();
		SoftAssertions.assertTrue(isElementExisting(driver, NavigatePopUpPage.navigatePopUp_modal, 5),"Navigate pop up is visible", test);
		navigatePopUp.closePopup();
		SoftAssertions.assertTrue(isElementExisting(driver, LocateMePage.ownRole_hightlighted, 5),"User is on locate me page", test);
		
		
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("failed to locate the user role", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("failed to locate the user role", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = false, priority = 3)
	public void verifyCompareFunctionality() throws Exception {
		try {
		test = reports.createTest("Verifying compare functionality");
		test.assignCategory("smoke");
		
		LocateMePage locateMePage = new LocateMePage();
		SoftAssertions.assertTrue(isElementExisting(driver, LocateMePage.ownRole_hightlighted, 5),"User is on locate me page", test);
		
		HeaderPage headerPage = new HeaderPage();
		headerPage.navigateViaHeader(HeaderPage.compare_button);
		
		ComparePopUpPage comparePopup = new ComparePopUpPage();
		SoftAssertions.assertTrue(isElementExisting(driver, ComparePopUpPage.startcompare_button, 5),"compare pop up is visible", test);
		comparePopup.performCompare();
		SoftAssertions.assertTrue(isElementExisting(driver, ComparePopUpPage.roleCompare_modal, 5),"Roles are compared successfully", test);
		comparePopup.closeModal();
		
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("failed to compare user role", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("failed to compare user role", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = false, priority = 4)
	public void navigateToHome() throws Exception {
		try {
		test = reports.createTest("Verifying if application returns home");
		test.assignCategory("smoke");
		
		LocateMePage locateMePage = new LocateMePage();
		SoftAssertions.assertTrue(isElementExisting(driver, LocateMePage.ownRole_hightlighted, 5),"User is on locate me page", test);
		
		HeaderPage headerPage = new HeaderPage();
		headerPage.navigateViaHeader(HeaderPage.home_button);
		
		HomePage homePage = new HomePage();
		SoftAssertions.assertTrue(isElementExisting(driver, HomePage.locateMe_menu, 5),"User navigated to home page successfully", test);
		
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("failed to navigate to home page via header", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("failed to navigate to home page via header", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
}
