package ui.smoke.testcases.careerView;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.Test;

import com.thoughtworks.selenium.webdriven.commands.IsElementPresent;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.careerView_Pages.AdminAnalyticsPage;
import pages.careerView_Pages.AdminHeaderPage;
import pages.careerView_Pages.AdminImportPage;
import pages.careerView_Pages.AdminPage;
import pages.careerView_Pages.AdminTaskPage;
import pages.careerView_Pages.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC5_VerifyAllHeaderInAdmin extends InitTests{

	@Test(enabled = false, priority = 1)
	public void verifyAnalyticsTabInAdmin() throws Exception {
		try {
		test = reports.createTest("Verifying Analytics tab in admin page");
		test.assignCategory("smoke");
		
		AdminPage adminPage = new AdminPage();
		adminPage.navigateToAdmin(USERNAME, PASSWORD);
		
		SoftAssertions.assertTrue(isElementExisting(driver, AdminPage.adminPage_header, 5), "User has logged in to admin access page", test);
	
		AdminHeaderPage adminHeaderPage = new AdminHeaderPage();
		adminHeaderPage.navigateViaHeader(AdminHeaderPage.analytics_button);
		
		AdminAnalyticsPage analyticsPage = new AdminAnalyticsPage();
		SoftAssertions.assertTrue(isElementExisting(driver, AdminAnalyticsPage.rangeSelection_dropdown, 5), "User is navigated to analytics page", test);
		
		analyticsPage.selectFromRangeDropdown();
		SoftAssertions.assertTrue((AdminAnalyticsPage.dateElement).size()>=28 && (AdminAnalyticsPage.dateElement).size()<=31,"Monthly records are displayed", test);
		
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("records are not present as required", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("records are not present as required", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = false, priority = 2)
	public void verifyingImportTab() throws Exception {
		try {
		test = reports.createTest("Verifying Import tab in admin page");
		test.assignCategory("smoke");
		
		
		AdminHeaderPage adminHeaderPage = new AdminHeaderPage();
		adminHeaderPage.navigateViaHeader(AdminHeaderPage.import_button);
		
		AdminImportPage importPage = new AdminImportPage();
		SoftAssertions.assertTrue(isElementExisting(driver, AdminImportPage.importPage_header, 5), "User is navigated to import page", test);
	
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("User did not reach to Import tab", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("User did not reach to Import tab", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = false, priority = 3)
	public void verifyingAdminTaskTab() throws Exception {
		try {
		test = reports.createTest("Verifying Admin Task tab in admin page");
		test.assignCategory("smoke");
		
		
		AdminHeaderPage adminHeaderPage = new AdminHeaderPage();
		adminHeaderPage.navigateViaHeader(AdminHeaderPage.adminTask_button);
		
		AdminTaskPage adminTaskPage = new AdminTaskPage();
		SoftAssertions.assertTrue(isElementExisting(driver, AdminTaskPage.adminTask_header, 5), "User is navigated to admin task page", test);
		SoftAssertions.verifyEquals(adminTaskPage.performResetAction(USERNAME), "Assessment request cleared", test);
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Unable to reset the user settings", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Unable to reset user settings", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = false, priority = 4)
	public void logoutfromAdminSite() throws Exception {
		try {
		test = reports.createTest("Logging out from career view admin site");
		test.assignCategory("smoke");
		
		
		AdminPage adminPage = new AdminPage();
		adminPage.performLogout();
		
		LoginPage loginPage = new LoginPage();
		SoftAssertions.assertTrue(isElementExisting(driver, LoginPage.username_input, 5),"User logged out successfully from admin career view application", test);
	
		
		
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Unable to logout from admin career site", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Unable to logout from admin career site", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
}
