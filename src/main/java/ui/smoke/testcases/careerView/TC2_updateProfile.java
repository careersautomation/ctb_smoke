package ui.smoke.testcases.careerView;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import java.io.IOException;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.careerView_Pages.HeaderPage;
import pages.careerView_Pages.ManagerAssessmentPage;
import pages.careerView_Pages.MyProfileSideMenu;
import pages.careerView_Pages.OverviewPage;
import utilities.SikuliActions;
import verify.SoftAssertions;

public class TC2_updateProfile {
	
	@Test(enabled = true, priority = 1)
	public void updateProfileInfo() throws IOException {
		try {
		test = reports.createTest("Updating profile picture and basic information");
		test.assignCategory("smoke");
				
		HeaderPage headerPage = new HeaderPage();
		headerPage.selectMyProfileOption();
		
		OverviewPage myProfilePopUp = new OverviewPage();
		SoftAssertions.assertTrue(isElementExisting(driver,OverviewPage.myProfile_header, 5),"My Profile pop up is displayed", test);
		
		//check for fields "last name" and "about"
		myProfilePopUp.editInfo();
		SoftAssertions.assertTrue(isElementExisting(driver,OverviewPage.savedInfoStyle_button, 5),"Information saved successfully", test);
		
		// using sikuli to upload an image
		String oldProfilePhoto =(HeaderPage.profilePhoto).getAttribute("style");
		System.out.println("old uploaded photo id   ----   "+oldProfilePhoto);
		String imagePath = TC1_LoginToCareerView.dirPath + "/src/main/resources/WindowScreens/googleSearch.png";
		myProfilePopUp.navigateToOverviewTab();
		SoftAssertions.assertTrue(isElementExisting(driver, OverviewPage.uploadPhoto_button, 5),"Navigated to overview screen", test);
		myProfilePopUp.uploadPhoto();
		String newProfilePhoto =(HeaderPage.profilePhoto).getAttribute("style");
		
		MyProfileSideMenu myProfileSideMenu = new MyProfileSideMenu();
		myProfileSideMenu.closePopup();
		
		SoftAssertions.assertFalse(isElementExisting(driver, OverviewPage.profile_modal, 5),"User closed the pop up successfully", test);
		System.out.println("New uploaded photo id   ----   "+newProfilePhoto);
		SoftAssertions.assertFalse( newProfilePhoto.contentEquals(oldProfilePhoto), "Profile photo addedd successfully", test);
		
		
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Falied to update profile picture", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Falied to update profile picture", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = false, priority = 2)
	public void TestForRequestAssessment() throws IOException {
		try {
		test = reports.createTest("Accessing Manager assessment tab");
		test.assignCategory("smoke");
		
		
		HeaderPage headerPage = new HeaderPage();
		headerPage.selectMyProfileOption();
		
		OverviewPage myProfilePopUp = new OverviewPage();
		SoftAssertions.assertTrue(isElementExisting(driver,OverviewPage.myProfile_header, 5),"My Profile pop up is displayed", test);
		MyProfileSideMenu myprofileSidemenu = new MyProfileSideMenu();
		myprofileSidemenu.navigateToManagerAssessmentPage();
		
		ManagerAssessmentPage managerAssessmentPage = new ManagerAssessmentPage();
		SoftAssertions.assertTrue(isElementExisting(driver, ManagerAssessmentPage.edit_button, 5),"Navigated to manager assessment scren", test);
		
		managerAssessmentPage.editManagerInfo();
		SoftAssertions.verifyEquals(managerAssessmentPage.getManageName(),ManagerAssessmentPage.name, test);
		SoftAssertions.verifyEquals(managerAssessmentPage.getManageEmail(),ManagerAssessmentPage.email, test);
		
		managerAssessmentPage.requestAssesment();
		SoftAssertions.assertTrue(isElementExisting(driver, ManagerAssessmentPage.requestSent_button, 5),"Request is sent to manager's email", test);
		
		myprofileSidemenu.closePopup();
		// email verification can not automate thus not clicking on the "Request Assessment" button to trigger an email to manager.
		
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Falied to update manager info", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Falied to update manager info", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
}
