package ui.smoke.testcases.personalIncomeTax;
import java.io.IOException;
import java.util.Map;
import static driverfactory.Driver.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;


import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;



import pages.pit.*;

import utilities.DataReaderMap;
import utilities.InitTests;

import utilities.RetryAnalyzer;
import verify.SoftAssertions;

public class VerifyNetToGrossCalculation extends InitTests
{

	private ME_LoginPage mE_LoginPage=null;
	private ME_ClientAccountSelectionPage mE_ClientAccountSelectionPage=null;
	private ME_DashboardPage mE_DashboardPage=null;
	private PIT_HomePage pIT_HomePage=null;
	private PIT_InputPage pIT_InputPage=null;
	private PIT_ResultsPage pIT_ResultsPage=null;
	
	public VerifyNetToGrossCalculation(String appName) {
		super(appName);
	}
	
	@Test(priority = 1, enabled = true, groups =
		{ "smoke" }, retryAnalyzer = RetryAnalyzer.class,dataProvider="getDataFromExcel",dataProviderClass=DataReaderMap.class)
		public void testNetToGross(Map<String,String> data) throws IOException
		{
			 System.out.println("in test");
			try
			{
				VerifyNetToGrossCalculation verifyNetToGrossCalculationTest= new VerifyNetToGrossCalculation("MobilityExchange");	
				test = reports.createTest("Verifying NET to GROSS calculation for "+data.get("SpecialCase"));
				test.assignCategory("smoke");
				
				
				initWebDriver(BASEURL,"CHROME", "", "", "local", test, "");
				System.out.println("BaseURL is: " + BASEURL);
				mE_LoginPage= new ME_LoginPage();
				mE_ClientAccountSelectionPage=mE_LoginPage.doLogin();
				mE_DashboardPage=mE_ClientAccountSelectionPage.selectClientAccount(data.get("Client").trim(),data.get("Account").trim() );
				pIT_HomePage=mE_DashboardPage.accessPIT();
			    pIT_InputPage=pIT_HomePage.accessInputPage(data.get("Country"));
				SoftAssertions.verifyElementTextContains(PIT_InputPage.optionalTitleLabel,"Optional Subtitle",test);
				setInput(PIT_InputPage.salaryInput, data.get("AnnualSalary"));
				if(!(data.get("BonusType").equalsIgnoreCase("NA") &&data.get("BonusValue").equalsIgnoreCase("NA")) ) {
					selEleByVisbleText(PIT_InputPage.bonusTypeDropdown, data.get("BonusType").toString());
					setInput(PIT_InputPage.bonusInput, data.get("BonusValue"));
				}
				//select salary type 
				selEleByVisbleText(PIT_InputPage.givenSalaryTypeDropdown, data.get("GivenSalary").toString());
				// select marrital status 
				selEleByVisbleText(PIT_InputPage.marritalStatusDropdown, data.get("MarritalStatus").toString());
				
				clickElement(PIT_InputPage.additionalComp1CB);
				waitForElementToDisplay(PIT_InputPage.AdditionalCompLabel1INPUT);
				setInput(PIT_InputPage.AdditionalCompLabel1INPUT, data.get("AdditionalCompLabel"));
				setInput(PIT_InputPage.AdditionalCompValue1INPUT, data.get("AdditionalCompValue"));
				
				pIT_ResultsPage=pIT_InputPage.accessResultPage();
				SoftAssertions.verifyElementTextContains(PIT_ResultsPage.headerLabel, "Personal Tax Calculation Results",test);
				
				SoftAssertions.verifyEquals(pIT_ResultsPage.getValue("GrossIncome").trim(), data.get("ExpGrossIncome"),test);
				SoftAssertions.verifyEquals(pIT_ResultsPage.getValue("TotalPersonalIncomeTax").trim(), data.get("ExpTotalTax"),test);
				SoftAssertions.verifyEquals(pIT_ResultsPage.getValue("SocialSecurity").trim(), data.get("ExpSS"),test);
				SoftAssertions.verifyEquals(pIT_ResultsPage.getValue("FamilyAllowance").trim(), data.get("ExpFA"),test);
				
		
				
				
				
			} catch (Error e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("Failed while accessing PIT application", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			} catch (Exception e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
				ATUReports.add("Failed while  accessing PIT application", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			}	
						
		}
		
	/*	@AfterMethod
		public void ending() {
				reports.endTest(test);
				reports.flush();
				closeBrowser(BROWSER_TYPE);
			}
*/
	

	@Test(priority=2,enabled = true)
	public void resultPageExportPDF_EXCELTest() throws IOException {
		try {
			test = reports.createTest("Result page Export test");
			test.assignCategory("smoke");
			
			
PIT_ResultsPage resultPage= new PIT_ResultsPage();
System.out.println("----------->>>User is on RESULT page ");
SoftAssertions.verifyElementTextContains(PIT_ResultsPage.headerLabel, "Personal Tax Calculation Results",test);
waitForElementToDisplay(PIT_ResultsPage.exportPDFRadioBtn);
clickElement(PIT_ResultsPage.exportPDFRadioBtn);
SoftAssertions.assertTrue(PIT_ResultsPage.exportBtn.isEnabled(),"Return true if Export button is enabled",test);
clickElement(PIT_ResultsPage.exportBtn);
delay(4000);
// write code to verify pdf file download & file name verify 
String filePath= System.getProperty("user.home")+"\\Downloads";
String fileName= "Mercer Simple Personal Income Tax Results.pdf";
SoftAssertions.assertTrue(PIT_ResultsPage.isFileDownloaded(filePath, fileName), "Returns true if file is downloaded successfully", test);

			
			
			
			
			}
			catch (Error e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			} catch (Exception e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			}
		
		
	}

	@AfterMethod
	public void closeReports() {
		
		reports.flush();
		
	}


}

