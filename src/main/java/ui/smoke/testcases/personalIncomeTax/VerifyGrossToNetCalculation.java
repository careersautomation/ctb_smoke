package ui.smoke.testcases.personalIncomeTax;
import static driverfactory.Driver.initWebDriver;
import java.io.IOException;
import java.util.Map;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import static driverfactory.Driver.driver;
import org.testng.annotations.Test;
import static driverfactory.Driver.*;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;


import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import pages.pit.*;
import utilities.DataReaderMap;
import utilities.InitTests;

import utilities.RetryAnalyzer;
import verify.SoftAssertions;


public class VerifyGrossToNetCalculation extends InitTests
{
	
	private ME_LoginPage mE_LoginPage=null;
	private ME_ClientAccountSelectionPage mE_ClientAccountSelectionPage=null;
	private ME_DashboardPage mE_DashboardPage=null;
	private PIT_HomePage pIT_HomePage=null;
	private PIT_InputPage pIT_InputPage=null;
	private PIT_ResultsPage pIT_ResultsPage=null;
	
	
	public VerifyGrossToNetCalculation(String appName) {
		super(appName);
	}
	
	
	@Test(priority = 1, enabled = true, groups =
		{ "smoke" }, retryAnalyzer = RetryAnalyzer.class,dataProvider="getDataFromExcel",dataProviderClass=DataReaderMap.class)
		public void testLogin(Map<String,String> data) throws IOException
		{
			 System.out.println("in test");
			try
			{
				VerifyGrossToNetCalculation verifyGrossToNetCalculationTest= new VerifyGrossToNetCalculation("MobilityExchange");	
				test = reports.createTest("Verifying Login to PIT application for "+data.get("SpecialCase"));
				test.assignCategory("smoke");
				
				initWebDriver(BASEURL,"CHROME", "", "", "local", test, "");
				System.out.println("BaseURL is: " + BASEURL);
				mE_LoginPage= new ME_LoginPage();
				mE_ClientAccountSelectionPage=mE_LoginPage.doLogin();
				SoftAssertions.assertTrue(isElementExisting(driver, ME_ClientAccountSelectionPage.mercerLogo, 5),"User logged in to the application & able to view Client-Account selection page ", test);
				mE_DashboardPage=mE_ClientAccountSelectionPage.selectClientAccount(data.get("Client").trim(),data.get("Account").trim() );
				SoftAssertions.assertTrue(isElementExisting(driver, ME_DashboardPage.PITLink, 5),"User is on Mobility Exchange Dashboard page ", test);
				pIT_HomePage=mE_DashboardPage.accessPIT();
				SoftAssertions.verifyContains(PIT_HomePage.getAccountCode(), data.get("Account").trim(), "PIT accessed succesfully & verifed account code", test);
		
			}
				
			catch (Error e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("Failed while accessing PIT application", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			} catch (Exception e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
				ATUReports.add("Failed while  accessing PIT application", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			}	
		}
		
		

	
	@Test(priority = 3, enabled = true, retryAnalyzer = RetryAnalyzer.class,dataProvider="getDataFromExcel",dataProviderClass=DataReaderMap.class)
		public void testGrossToNet(Map<String,String> data) throws IOException
	{
		try {
		test = reports.createTest("Verifying Gross to Net calculation for "+data.get("SpecialCase"));
		test.assignCategory("smoke");
		System.out.println("Test data used ==="+data.toString());
		PIT_HomePage homePage = new PIT_HomePage();
		PIT_InputPage inputPage=homePage.accessInputPage(data.get("Country"));
		SoftAssertions.assertTrue(isElementExisting(driver, PIT_InputPage.salaryInput, 5),"User is on Input page ", test);
		
		
		setInput(PIT_InputPage.salaryInput, data.get("AnnualSalary"));
		if(!(data.get("BonusType").equalsIgnoreCase("NA") &&data.get("BonusValue").equalsIgnoreCase("NA")) ) {
			selEleByVisbleText(PIT_InputPage.bonusTypeDropdown, data.get("BonusType").toString());
			setInput(PIT_InputPage.bonusInput, data.get("BonusValue"));
		}
		
		// select marrital status 
		selEleByVisbleText(PIT_InputPage.marritalStatusDropdown, data.get("MarritalStatus").toString());
		// TA type& Value 
		selEleByVisbleText(PIT_InputPage.TaxableAllowanceTypeDropdown, data.get("TAType").toString());
		setInput(PIT_InputPage.TaxableAllowanceValueInput, data.get("TAValue").toString());
		// TDC type & vale
		selEleByVisbleText(PIT_InputPage.TDCTypeDropdown , data.get("TDCType").toString());
		setInput(PIT_InputPage.TDCValueInput, data.get("TDCValue").toString());
		
		pIT_ResultsPage=inputPage.accessResultPage();
		SoftAssertions.verifyElementTextContains(PIT_ResultsPage.headerLabel, "Personal Tax Calculation Results",test);
		SoftAssertions.verifyEquals(pIT_ResultsPage.getValue("NetIncome"), data.get("ExpNetIncome"),test);
		SoftAssertions.verifyEquals(pIT_ResultsPage.getValue("TotalPersonalIncomeTax"), data.get("ExpTotalTax"),test);
		SoftAssertions.verifyEquals(pIT_ResultsPage.getValue("SocialSecurity"), data.get("ExpSS"),test);
		SoftAssertions.verifyEquals(pIT_ResultsPage.getValue("FamilyAllowance"), data.get("ExpFA"),test);
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Failed while Calculating GROSS to NET ", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed while while Calculating GROSS to NET", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}	
	
	
	
	@Test(priority=2,enabled=true)
	
	public void verifyTaxReportHomePage() throws IOException {
		try
		{
				
			test = reports.createTest("Verifying download tax report on Home page" );
			test.assignCategory("smoke");
			PIT_HomePage homePage = new PIT_HomePage();
			SoftAssertions.assertTrue(isElementExisting(driver, PIT_HomePage.taxReportLocationDropdown, 5),"Tax report -Country / Market / City dropdown is displayed ", test);
			selEleByVisbleText(PIT_HomePage.taxReportLocationDropdown, "Brazil");
			waitForElementToDisplay(PIT_HomePage.taxYearDropdown);
			selEleByVisbleText(PIT_HomePage.taxYearDropdown, "2017");
			clickElement(PIT_HomePage.runYourReportBtn);
			delay(5000);
			// write code to verify PDF file download 
			String filePath= System.getProperty("user.home")+"\\Downloads";
			String fileName= "tax_br_17.pdf";
			SoftAssertions.assertTrue(PIT_ResultsPage.isFileDownloaded(filePath, fileName),"Returns true if file is downloaded successfully",test);
		
		}
			
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Failed while accessing PIT application", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Failed while  accessing PIT application", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	
	
	
	

	@AfterMethod
	public void closeReports() {
		
		reports.flush();
		
	}


@AfterSuite
public void killDriver() {
	
	driver.close();
	driver.quit();
	killBrowserExe(BROWSER_TYPE);
	
}
}
